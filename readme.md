# Proyecto de Next.JS con Redux

Proyecto de next para realizar una app de domicilios que sea escalable, para incluir diferentes comercios realizado con NextJS, React, Redux, CSS module, Styles Components. Primera parte estructura se hizo de manera individual, posterior el proyecto será completado colaborativamente. 

### 🚀 Logros

1. Creación del proyecto
1. Agrego de rutas
1. Agrego una ruta con scroll
1. Enlazo páginas y creo una SPA
1. Agrega nuestra propia API
1. Páginas usando nuestra propia API
1. Crea components App and Document personalizados
1. Configura path aliases
1. Crea páginas y componentes UI para el sitio.
1. Crea Store usando Redux 

### 🤖 Guía Rápida

1.  **Empieza a desarrollar.**

    Instala dependencias

    ```sh
    yarn
    ```

    Inicia el proyecto

    ```sh
    yarn dev
    ```

    El sitio estará disponible en http://localhost:3000.

    Happy hacking!