import {AiFillStar, AiFillHome} from 'react-icons/ai'
import {FaMapMarkerAlt}  from 'react-icons/fa'

const Header = () => {
    return (
        <>
            <header>
                <div className='logo'>IMG</div>
                <div className='info'>
                    <div className='title'>
                        <span>NOMBRE DEL ESTABLECIMIENTO</span>
                        <div className='rating'>
                            <AiFillStar/>
                            <span>4.5</span>
                        </div>
                    </div>
                    <div className='description'>
                        <nav>
                            <ol>
                                <li><span><AiFillHome/>Home</span></li>
                                <li><span>Restaurant</span></li>
                                <li><span>Nombre</span></li>
                            </ol>
                        </nav>
                    </div>
                    <div className='ubication'>
                        <div>
                            <FaMapMarkerAlt/>
                            <span>CALLE 15 # 17-12 LOCAL 15, Facatativá, Cundinamarca</span>
                        </div>
                        <div>
                            <span>HASTA 35% OFF</span>
                        </div>
                    </div>
                    
                    <div></div>
                </div>
            </header>
            <style jsx>{`
                header {
                    width: 100%;
                    height: 200px;
                    background: rgba(247,248, 249);
                    padding: 40px;
                    display: flex;
                    align-items: flex-start;
                    grid-area: restheader / restheader / restheader / restheader;
                    box-sizing: border-box;
                }
                .logo {
                    background-color: rgb(230, 235, 241);

                    width: 80px;
                    height: 80px;
                    margin-right: 40px;

                    display: flex;
                    align-items: center;
                    justify-content: center;
                    border-radius: 50%;
                }
                .info {
                    display: flex;
                    flex-direction: column;
                    flex: 1 1 0%;
                }
                .title {
                    justify-content: flex-start;
                    margin-bottom: 10px;

                    display: flex;
                    width: 100%;
                    align-items: center;
                }
                .title span {
                    font-size: 20px;
                    font-family: Goldplay-Bold;
                    line-height: 1.25;
                    letter-spacing: normal;
                    color: black;
                }
                .rating{
                    margin-left: 30px;

                    display: flex;
                    align-items: center;
                    justify-content: space-between;
                    background-color: rgb(255, 255, 255);
                    border: 1px solid rgb(230, 235, 241);
                    border-radius: 8px;
                    height: 32px;
                    width: 65px;
                    padding: 4px 8px;
                }
                .description{
                    display: flex;

                    flex-direction: column;

                }
                .description nav{
                    display: inline-flex;
                    align-items: center;
                    font-size: 16px;
                    font-family: Nunito-Regular;
                    line-height: 1,63;
                    letter-spacing: normal;
                }
                .description ol{
                    display: flex;
                    gap: 10px;
                    
                }
                .description li{
                    padding: 5px;
                    border-right: 1px solid black;
                }

                .description li:first-child{
                    padding-left:0 ;
                }
                
                .ubication {
                    display: flex;
                }
                .ubication div:first-child {
                    margin-right: 30px;
                }
                .ubication div:nth-child(2n) {
                    color: white;
                    background-color: #6d2b2b;
                    width: unset;
                    height: unset;
                    margin-right: 10px;
                    padding: 2px 8px;
                    flex-wrap: wrap;
                    border-radius: 4px;
                    text-align: center;
                    overflow: hidden;
                    position: relative;


                }
                @media (max-width: 768px){
                .description, .ubication div:first-child{
                    display: none;
                }
                header {
                    padding: 16px;
                    height: 110px;
                }
                .logo{
                    width: 40px;
                    height: 40px;
                    margin-right: 16px;
                }
                .title{
                    justify-content: space-between;
                    display: flex;
                    -webkit-box-pack:justify ;
                }
                }

            `}
            </style>
        </>
    )
}

export default Header
