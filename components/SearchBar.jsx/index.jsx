import styles from "./styles.module.css";
import { FaSearch } from "react-icons/fa";
// import { useEffect, useState } from "react";
// import { useHistory } from "react-router";
// import { useQuery } from "./useQuery";

export default function SearchBar() {
  // const query = useQuery();
  // const search = query.get("search");

  // const [searchText, setSearchText] = useState("");
  // const history = useHistory();

  // useEffect(() => {
  //   setSearchText(search || "");
  // }, [search]);

  // const handleSubmit = (e) => {
  //   e.preventDefault();
  //   history.push("/?search=" + searchText);
  // };
  return (
    <form className={styles.searchContainer} >
      <div className={styles.searchBox}>
        <input
          className={styles.searchInput}
          type="text"
          placeholder="Buscar..."
          //value={searchText}
          // onChange={(e) => setSearchText(e.target.value)}
        />
        <button className={styles.searchButton} type="submit">
          <FaSearch size={20} />
        </button>
      </div>
    </form>
  );
}