import React from 'react'
import {connect} from 'react-redux'
import styles from './styles.module.css'
import Spinner from '@components/Spinner'
import Fatal from '@components/Fatal'
import Products from '@components/Products'
import categories from '../../database/categories.json'


const Sections = (props) => {
    const {loading, error,} = props

    return (
      <>
        {loading ? <Spinner/> : ""}
        {error ? <Fatal message={error}/> : ""}
        <div className={styles.sectionContainer}>
          {categories.map(category=><Products key={category.category} id={category.category}/>)}
        </div>
        
        
      </>
    )
}
const mapStateToProps = (state) => {
    return state.productsReducer
     }
  
  
  
export default connect(mapStateToProps, null)(Sections)



