import React from 'react'

const Fatal = (props) => {
    return (
        <h2>
            {props.message}
        <style jsx>
            {`
            h2 {
                text-align:center;
                color: red
            }
            `}
        </style>
        </h2>
    )
}

export default Fatal
