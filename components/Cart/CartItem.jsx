import React, { useState } from 'react'


const CartItem = ({data, removeOne}) => {
    const {name, quantity, price, id, img, description} = data
    const [count, setCount] = useState(quantity)

    
    const handleReduce = ()=> {
        (count>1 ? setCount(count-1) : alert('No pueden haber menos de un producto'))
    }

    const handleIncrease = ()=> {
        setCount(count+1)
    }

   

    return (
        <div className="details cart">
            <img src={img} alt=""/>
            <div className="box">
                <div className="row">
                    <h2>{name}</h2>
                    <span>${price * count}</span>
                </div>
                                    
                <p>{description}</p>
                                    
                <div className="amount">
                    <button className="count" onClick={handleReduce}> - </button>
                    <span>{count}</span>
                    <button className="count" onClick={handleIncrease}> + </button>
                </div>
            </div>
            <div className="delete" onClick={() => removeOne(id)}>X</div>
        </div>
    )
}

export default CartItem
