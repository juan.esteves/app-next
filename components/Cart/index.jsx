import {connect} from 'react-redux'
import Link from 'next/link'
import { getTotal, increse, reduce, removeAll, removeOne } from 'store/actions'
import {FaWhatsapp} from 'react-icons/fa'
import Styles from './styles';
import CartItem from './CartItem';
import { useEffect } from 'react';


    const Cart = (props) => {
       
    const { increase,reduce,removeOne, removeAll, data, getTotal } = props;
    const {cart, total} = data;
    console.log(cart)
    useEffect(() => {
        getTotal();
    }, [])


    const wh = (cart.map(e=>JSON.stringify(e))).toString() 
   
    if(cart.length === 0){
            return <h2 style={{textAlign:"center"}}>Nothings Product</h2>
        }else{
            return (
                <>
                    {
                        cart.map((item, index) =>(
                            <CartItem key={index} data={item} increase={increase} reduce={reduce} removeOne={removeOne}/>
                        ))
                    }
                    <button onClick={removeAll}>REMOVER ALL</button>
                    <div className="total">
                    <a  href={`https://api.whatsapp.com/send?phone=573218122180&text=${wh}`}> <button>Enviar orden<FaWhatsapp/></button> </a>
                        <h3>Total:${total}.00</h3> 
                    </div>

                    <Styles/>
                </>
                )
            }
        }
    


const mapStateToProps = (state) => {
    return state.productsReducer
     }
  
  const mapDispatchToProps = (dispatch) => {
    return {
      getProducts: (payload) => dispatch(getAll(payload)),
      removeOne:  (payload) => dispatch(removeOne(payload)),
      removeAll:  (payload) => dispatch(removeAll(payload)),
      increase:  (payload) => dispatch(increse(payload)),
      reduce:  (payload) => dispatch(reduce(payload)),
      getTotal: () => dispatch(getTotal())
    }
  }
  
export default connect(mapStateToProps, mapDispatchToProps)(Cart)

