import Link from 'next/link'
import categories from "../../database/categories.json"
import MenuItem from './MenuItem'
import {BiFoodMenu} from 'react-icons/bi'

const Menu = () => {
    
    return (
        <>
            <div className="container">
                <div className='menuLabel'>
                    <BiFoodMenu className='bi'/>
                    <span>Menu</span>
                </div>
                <div className='menuOptions'>
                    {categories.map((category)=><MenuItem key={category.id} data={category}/>)}
                </div>
            </div>
            <style jsx> {`
                .container{
                    border-right: 1px solid rgb(230, 235, 241);
                    overflow-x: unset;
                    grid-template-columns: 1fr;
                    max-width: 250px;
                    grid-template-rows: auto;
                    grid-template-areas: "label" "menu";
                    padding: 0px;
                    display: grid;
                    grid-area: restmenu;
                    align-content: flex-start;
                    width: 100%;
                    
                    background-color: rgb(255, 255, 255);
                    position: relative;
                    scrollbar-width: none;
                    border-top: 1px solid rgb(230, 235, 241);
                }
                .menuLabel{
                    padding: 12px 0px 12px 40px;
                    border-bottom: 1px solid rgb(230, 235, 241);
                    margin-right: 0px;
                    grid-area: label;
                    display: flex;
                    -moz-box-align: center;
                    align-items: center;
                    -moz-box-pack: start;
                    justify-content: flex-start;
                }
                .bi{
                    margin-right: 9px;
                    fill: rgb(106, 105, 110);
                }
                .menuLabel span{
                    font-size: 16px;
                    line-height: 1.63;
                    letter-spacing: normal;
                    color: rgb(46, 44, 54);
                }
                .menuOptions {
                    flex-direction: column;
                    text-align: left;
                    align-items: flex-start;
                    overflow-y: scroll;
                    max-width: 250px;
                    grid-area: menu;
                    width: 100%;
                    display: flex;
                    -moz-box-align: center;
                    -moz-box-pack: start;
                    justify-content: flex-start;
                }



                @media(max-width: 769px){
                    .container{
                        overflow-x: scroll;
                        grid-template: "label menu" / 94px 1fr;
                        padding: 8px 16px;
                    }
                    .menuLabel{
                        margin-right: 25px;
                    }
                    .menuOptions{
                        align-items: center;
                    }
                }
                
                
                
                `}
            
            </style>
        </>    
    )
}

export default Menu
