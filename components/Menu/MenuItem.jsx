import React from 'react'
import Link from 'next/link'
import {IoIosArrowForward} from 'react-icons/io'

const MenuItem = ({data}) => {
    
    return (
        <>
            <a href={`#${data.category}`}>
                {data.category}
                <IoIosArrowForward/>
            </a>
            
            <style jsx>{`
            a{
                display: flex;
                text-align: center;    
                border-radius: unset;
                width: calc(80% - 4px);
                align-self: flex-end;
                padding: 8px 10px 8px 36px;
                -moz-box-pack: justify;
                justify-content: space-between;
                margin-right: 0px;
                -moz-box-align: center;
                align-items: center;
                cursor: pointer;
                white-space: nowrap;
            }
            @media (max-width: 769px){
                a{
                    background-color: rgb(255, 255, 255);
                    padding: 5px 7px;
                    margin-right: 8px;
                    border-radius: 8px;
                    border: 1px solid rgb(230, 235, 241);
                }
            }
                
                `}

            </style>
        </>
    )
}

export default MenuItem
