import CostDelivery from "@components/CostDelivery";
import Header from "@components/Header";
import Menu from "@components/Menu";
import NavBar from "@components/NavBar";
import Products from "@components/Products";
import Sections from "@components/Sections";


const Layout = ({ children }) => (
    <>
      <NavBar />
      
      <div className='container'>
        <Header/>
        <Menu/>
        <CostDelivery/>
        <Sections/>
      </div>
    
      <style jsx>
        {`
        .container{
          display: grid;
          height: 100%;
          scroll-behavior: smooth;
          grid-template-columns: 250px 1fr;
          grid-template-rows: 242px 51px calc(100vh - 331px);
          grid-template-areas: 
            "restheader restheader"
            "restmenu restcost"
            "restmenu restproducts";
        }
        @media (max-width: 769px) {
        .container{
          grid-template-columns: 1fr;
          grid-template-rows: auto;
          grid-template-areas: 
            "restheader"
            "restcost"
            "restmenu"
            "restsearch"
            "restproducts";
        }
        }
        `}
      </style>
      
    </>
  )

  export default Layout;