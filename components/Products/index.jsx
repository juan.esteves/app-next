import React, {useEffect } from 'react'
import Product from '@components/Product'
import {connect} from 'react-redux'
import { getAll } from 'store/actions'



const Products = (props) => {
  const {products, getProducts, id} = props


  useEffect(() => {
      getProducts()
  }, [])


  const productsCategory =  products.filter(e=>e.attributes.category === id)
    
    
    return (
      
      <>
        <div className='containerGrid'>
          <div className="productsGrid" id={id}>
            <div className='categoryGrid'>
              <div>Category</div>
              <div className='products'>
                {productsCategory.map((p)=><Product key={p.id} data={p}/>)}
              </div>
              
            </div>
          </div>
          <div className="aboutGrid" id={id}>
          </div>
          <div className="FAQsGrid" id={id}>
          </div>
        </div>

        <style jsx>{`
          .containerGrid{
            display: flex;
            grid-area: restproducts;
            overflow-y: auto;
            flex-direction: column;
            align-items: flex-start;
            scroll-behavior: smooth;
            background-color: rgb(247, 248, 249);
            border-top: 1px solid rgb(230, 235, 241);
            padding: 16px;
            height: unset;
          }
          .productsGrid{
            display: flex;
            flex-direction: column;
            width: 100%;
          }
          .categoryGrid{
            width: 100%;
            background-color: rgb(255, 255, 255);
            padding: 40px 24px;
            margin-bottom: 16px;
          }
          .categoryGrid div:first-child{
            padding: 0px ;
            margin-bottom: 30px;
          }
          .products{
            display: grid;
            grid-template-columns: repeat(2, 1fr);
            grid-template-rows: auto;
            gap: 20px 48px;
          }
          

          @media(max-width: 769px){
            .containerGrid{
              height: 100vh;
            }
            .categoryGrid{
              padding: 24px 0px;
            }
            .categoryGrid div:first-child{
              padding: 0px 16px;
            }
            .products{
              grid-template-columns: 1fr;
              grid-template-rows: auto;
            } 
          }
          
        `}
        </style>
        
        
      </>
        
    )
}

const mapStateToProps = (state) => {
    return state.productsReducer
     }
  
  const mapDispatchToProps = (dispatch) => {
    return {
      getProducts: (payload) => dispatch(getAll(payload)),
    }
  }
  
export default connect(mapStateToProps, mapDispatchToProps)(Products)

