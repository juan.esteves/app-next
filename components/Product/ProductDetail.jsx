import React from 'react'
import styles from './styles.module.css'
import {AiFillDelete, AiFillCloseCircle} from 'react-icons/ai'
import {connect} from 'react-redux'
import { addToCart } from 'store/actions'
import Link from 'next/link'



const ProductDetail = (props) => {
    const {addToCart, modal, closeModal, data} = props

    //console.log(data)

    const handleAddToCart = () => {
        addToCart({
         name: data.name,
         id:data.id,
         price:data.price
           
        })
       }
    
    
    return (
        <>
            <div className={modal ? `${styles.modal} ${styles.modalNV}`: `${styles.modal} ${styles.modalV}` }>
                <div className="container">
                    <div className="detail">
                        <header>
                            <span>{data.name}</span>
                            <div  onClick={closeModal}><AiFillCloseCircle size={30}/></div>
                        </header>
                        <section>
                           
                            <div className="conteinerSection">
                                <div className="imgSection">
                                    <div className='image'>
                                        <img/>
                                    </div>
                                    <div className='comment'>
                                        <span>Comentarios</span>
                                        <div><textarea placeholder="Añade instrucciones o comentarios..."></textarea></div>
                                        <span>*En este campo no se aceptan modificaciones que generen valor adicional en la factura. Puedes chatear con tu Rappi si quieres cambios.</span>
                                    </div>
                                </div>
                                <div className='infoSection'>
                                    <div className='infoSectionContainer'>
                                        <div className='price'><span>${data.price}.00</span></div>
                                        <p>Pizza mediana con piña y una mezcla de 4 quesos. Acompañada de Garlic Knots (8und) y Gaseosa 1.5 L sabor a elegir.</p>
                                        <div className='options'></div>
                                    </div>
                                </div>
                            </div>

                            <footer>
                                <div className="footerPrice">
                                    <span>SUBTOTAL</span>
                                    <span>$5000</span>
                                </div>
                                <div className="footerQuantity">
                                    <span className={styles.span}><AiFillDelete/></span>
                                    <span className={styles.span}>$1</span>
                                    <span className={styles.span}>+</span>
                                </div>
                                <div className="btnAdd">
                                    <button  onClick={handleAddToCart}>Agregar y seguir comprando</button>
                                </div>
                                <div className="btnAddSend" onClick={handleAddToCart}>
                                    <Link href='/cart'><button >Agregar e ir a pagar</button></Link>
                                </div>
                            </footer>

                        </section>
                    </div>
                </div>
            </div>

            <style jsx>{`

                .containerModal{
                    position: fixed;
                    display: flex;
                    width: 100%;
                    height: 100%;
                    inset: 0px;
                    background-color: rgba(46, 44, 54, 0.2);
                    z-index: 2000;
                }
                .container{
                    width: 1130px;  
                    background-color: rgb(255, 255, 255);
                    border-radius: 8px 8px 0px 0px;
                    position: relative;
                    height: auto;
                    border-bottom-left-radius: 8px;
                    border-bottom-right-radius: 8px;
                    bottom: 0px;
                    padding-bottom: 0px;
                    transition: all 1s ease-in-out 0s;
                    animation: 0.3s ease-out 0s 1 normal none running brdlrd;
                }
                .detail{
                    width: 100%;
                    height: 100%;
                    position: relative;
                }

                .detail header{
                    border-bottom: 1px solid rgb(230, 235, 241);
                    display: flex;
                    align-items: flex-start;
                    -moz-box-pack: justify;
                    justify-content: space-between;
                    padding: 30px 20px 20px;
                }

                .detail header span{
                    font-weight: normal;
                    margin: 0px;
                    text-align: left;
                    overflow: hidden;
                    text-overflow: ellipsis;
                    white-space: nowrap;
                    font-size: 24px;
                    font-family: Nunito-ExtraBold;
                    line-height: 1.42;
                    letter-spacing: -0.24px;
                    color: rgb(46, 44, 54);
                }

                .detail header div{
                    cursor: pointer;
                    background-color: transparent;
                    width: 24px;
                    height: 24px;
                    display: flex;
                    -moz-box-align: center;
                    align-items: center;
                    -moz-box-pack: center;
                    justify-content: center;
                    float: right;
                }

                section{
                    padding: 20px 20px 0px;
                    max-height: 70vh;
                    overflow-y: auto;
                    scrollbar-width: none;
                    }

                .conteinerSection{
                    display: grid;
                    grid-template: "col1 col2" / 467px 1fr;
                    padding: 10px 20px;
                    gap: 40px;
                    scrollbar-width: none;
                }


                .imgSection{
                    grid-area: col1;
                    position: relative;
                }

                .infoSection{
                    grid-area: col2;
                }
                
                

                .imgSection .image {
                    position: relative;
                    width: 100%;
                    height: 375px;
                    overflow: hidden;
                    margin-bottom: 30px;
                    background-color: rgb(230, 235, 241);
                    border-radius: 8px;
                    display: flex;
                    -moz-box-pack: center;
                    justify-content: center;
                }

                .imgSection .image img{
                    display: block;
                    overflow: hidden;
                    position: absolute;
                    inset: 0px;
                    box-sizing: border-box;
                    margin: 0px;
                    position: absolute;
                    inset: 0px;
                    box-sizing: border-box;
                    padding: 0px;
                    border: medium none;
                    margin: auto;
                    display: block;
                    width: 0px;
                    height: 0px;
                    min-width: 100%;
                    max-width: 100%;
                    min-height: 100%;
                    max-height: 100%;
                    object-fit: cover;
                }

                .imgSection .comment div{
                    margin-top: 6px;
                    margin-bottom: 8px;
                
                    margin: 0px 0px 32px;
                    padding: 12px 10px;
                    width: 100%;
                    height: 76px;
                    border-bottom: 1px solid rgb(230, 235, 241);
                    background-color: rgb(247, 248, 249);
                }

                .imgSection .comment span {
                    font-size: 12px;
                    font-family: Nunito-Regular;
                    line-height: 1.83;
                    letter-spacing: normal;
                    color: rgb(179, 185, 194);
                }

                .imgSection .comment div textarea{
                    width: 100%;
                    height: 100%;
                    border: medium none;
                    resize: none;
                    background-color: rgb(247, 248, 249);
                    font-family: Nunito-Regular;
                    font-size: 14px;
                    line-height: 1.71;
                    letter-spacing: normal;
                    color: rgb(106, 105, 110);
                    overflow: auto;
                }

                .infoSectionContainer{
                    max-height: inherit;
                    overflow: inherit;
                    width: 100%;
                    overflow-y: auto;
                    margin-bottom: 0px;
                }

                .price{
                    display: flex;
                    margin-bottom: 30px;
                    gap: 16px;
                    -moz-box-align: center;
                    align-items: center;
                }

                .price span{
                    font-size: 16px;
                    font-family: Nunito-ExtraBold;
                    line-height: 1.63;
                    letter-spacing: normal;
                    color: rgb(46, 44, 54);
                }

                .infoSectionContainer p{
                    display: -webkit-box;
                    -webkit-line-clamp: 3;
                    -moz-box-orient: vertical;
                    overflow: hidden;
                    font-size: 16px;
                    font-family: Nunito-Regular;
                    line-height: 1.63;
                    letter-spacing: normal;
                    color: rgb(106, 105, 110);
                }

                .options{
                    position: relative;
                    display: block;
                    margin-bottom: 88px;
                }


                footer{
                    position: absolute;
                    padding: 27px 20px;
                    width: 1090px;
                    display: grid;
                    grid-template-columns: 20% 28% 24% 28%;
                    -moz-box-align: center;
                    align-items: center;
                    gap: 20px;
                    border-radius: 8px;
                    bottom: 0px;
                    left: 0px;
                    background-color: rgb(255, 255, 255);
                    overflow: hidden;
                }

                .footerPrice{
                    float: left;
                    display: flex;
                    flex-direction: column;
                }

                .footerPrice span{
                    font-size: 18px;
                    font-family: Nunito-Regular;
                    line-height: 1.42;
                    letter-spacing: -0.24px;
                    color: rgb(46, 44, 54);
                }

                .footerQuantity{
                    order: 1;
                    margin-top: 0px;
                    display: inline-flex;
                }

                .btnAdd{
                    order: 2;
                }
                
                .btnAddSend{
                    order: 3;
                    margin-top: 0px;
                    float: right;
                    width: 210px;
                }


                .btnAdd button, .btnAddSend button{
                    min-width: 162px;
                    max-width: 343px;
                    width: 248px;
                    height: 48px;
                    padding: 10px;
                    font-size: 16px;
                    line-height: 1.63;
                    background-color: rgb(247, 248, 249);
                    color: rgb(179, 185, 194);
                    
                    cursor: pointer;
                    letter-spacing: normal;
                    text-align: center;
                    white-space: nowrap;
                    outline: currentcolor none medium;
                    border-radius: 8px;
                    object-fit: contain;
                    border: medium none;
                    font-family: "Nunito-ExtraBold";
                }

                
                @media (max-width: 375px){
                    .price{
                        gap: 10px;
                        grid-template: "price discount popular" / 1fr 65px 1fr;
                        display: grid;
                
                }
                @media (max-width: 769px){
                    .container{
                        width: 445px;
                        width: 100%;
                        height: 100vh;
                        position: absolute;
                        
                    }


                    .conteinerSection{
                        grid-template-columns: 1fr;
                        grid-template-rows: auto;
                        grid-template-areas: "col1" "col2";
                        padding: 10px 0px 50px;
                    }
                    .imgSection .image {
                        height: 203px;

                    }
                    .infoSectionContainer{
                    overflow: inherit;
                    }
                    .options{
                    margin-bottom: 40px;
                    }
                    footer{
                        padding: 20px 16px;
                        width: 100%;
                        position: fixed;
                    }
                    .footerQuantity, .btnAddSend{
                        margin-top: 20px;
                    }
                    
                }

                @media (max-width: 1440px){
                    .detail header{
                        padding: 10px;
                }


            `}
            </style>
        </>
    )
}


const mapStateToProps = (state) => {
    return state.shoppingReducer
     }
  
  const mapDispatchToProps = (dispatch) => {
    return {
      addToCart: (payload) => dispatch(addToCart(payload)),
      
    }
  }
  
export default connect(mapStateToProps, mapDispatchToProps)(ProductDetail)


