import React, {useState} from 'react';
import { AiOutlinePlusCircle } from 'react-icons/ai';
import ProductDetail from '@components/Product/ProductDetail';



const Product = ({data, addToCart}) => {
    const [modal, setModal] = useState(true)
    

    const toogleModal =() => setModal(!modal)

     
    return (
        <>

            <div className="containerProduct" >
            
                <div  className="imgProduct">
                    <div>
                        <img/>
                    </div>
                    
                </div>
                <div className="infoProduct">
                    <div className="infoTitle" onClick={addToCart}>{data.name}</div>
                    <button className="infoBtn" onClick={toogleModal}> <AiOutlinePlusCircle  size={40} /></button>
                    <div className="infoDescription">{data.description}</div>
                    <div className="infoPrice">${data.price}.00</div>
                
                </div>
                
            
                <ProductDetail data={data} closeModal={toogleModal} modal={modal}/>
              
            </div>
            <style jsx> {`
                .containerProduct{
                    display: grid;
                    gap: 16px;
                    padding: 16px;
                    cursor: pointer;
                    grid-template-columns: 128px 1fr;
                    width: 100% !important;
                    border: 1px solid rgb(230, 235, 241);
                    border-radius: 8px;
                }
                .imgProduct{
                    background-color: rgb(18, 49, 87);
                    border-radius: 8px;
                    display: flex;
                    -moz-box-align: center;
                    align-items: center;
                    -moz-box-pack: center;
                    justify-content: center;
                    position: relative;
                    width: 128px;
                    height: 104px;
                }
                .imgProduct div{
                    display: block;
                    overflow: hidden;
                    position: absolute;
                    top: 0;
                    left: 0;
                    bottom: 0;
                    right: 0;
                    box-sizing: border-box;
                    margin: 0;
                }

                .imgProduct img{
                    position: absolute;
                    top: 0;
                    left: 0;
                    bottom: 0;
                    right: 0;
                    box-sizing: border-box;
                    padding: 0;
                    border: none;
                    margin: auto;
                    display: block;
                    width: 0;
                    height: 0;
                    min-width: 100%;
                    max-width: 100%;
                    min-height: 100%;
                    max-height: 100%;
                    object-fit: cover;
                    border-radius: 8px;
                }
                .infoProduct{
                    position: relative;
                }
                .infoTitle{
                    display: -webkit-box;
                    -webkit-line-clamp: 2;
                    -moz-box-orient: vertical;
                    overflow: hidden;
                    padding-right: 50px;
                    font-size: 16px;
                    font-family: Nunito-Regular;
                    line-height: 1.63;
                    letter-spacing: normal;
                    color: rgb(46, 44, 54);
                }
                .infoBtn {
                    background-color: rgb(255, 255, 255);
                    border-radius: 50%;
                    border: medium none;
                    cursor: pointer;
                    display: flex;
                    -moz-box-align: center;
                    align-items: center;
                    position: absolute;
                    top: 0px;
                    right: 0px;
                    width: 40px;
                    height: 40px;
                    justify-content: center;
                }
                .infoDescription{
                    display: -webkit-box;
                    -webkit-line-clamp: 2;
                    -moz-box-orient: vertical;
                    overflow: hidden;
                    padding-right: 50px;
                    margin-bottom: 8px;
                    font-size: 14px;
                    font-family: Nunito-Regular;
                    line-height: 1.71;
                    letter-spacing: normal;
                    color: rgb(106, 105, 110);
                }
                

                @media (max-width: 769px){
                    .containerProduct{
                        width: 485px;
                    }
                }


                
                
            `}

            </style>
        </>
    )
}

  
export default (Product)

