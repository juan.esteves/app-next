import React, { useState } from 'react';
import {secciones, others, cities} from '../../database/dataBase'
import {AiFillCloseCircle} from 'react-icons/ai'




const MenuNavBar = ({menuToggle}) => {
    const [sections, setSections] = useState(false)

    const handleSections = () => {
        setSections(!sections)
    }

console.log(cities)
    return (
        <>
        <div className='modal'>
            <div className='container'>
                <header>
                    <span>CABECERA</span>
                    <span className={`close`} onClick={menuToggle} > 
                        <AiFillCloseCircle size={40} />
                    </span>    
                </header>
                <hr />
                <section className='sections'>
                    <span>SECCIONES</span>
                    {sections? secciones.map((section)=>(<span>{section}</span>)):secciones.map((section, index)=>(index<4?<span>{section}</span>:""))}
                    {sections?<h4 onClick={handleSections}>Ver menos</h4>:<h4  onClick={handleSections}>Ver más</h4>}
                </section>
                <hr />
                <section className='others'>
                    <h3>OTROS</h3>
                    {others.map((other)=><span>{other}</span>)}
                </section>
                <hr />
                <section className='others'>
                    <h3>CIUDAD</h3>
                    <span>{cities[0].city}</span>
                </section>
            </div>
            
            
                                
            

                
            </div>
            <style jsx>
                {`
                .modal{
                    width: 100%;
                    height: 100vh;
                    display: flex;
                    justify-content: center;
                    align-items: center;
                    position: fixed;
                    top:0;
                    left:0;
                    background: rgba(0, 0, 0, 0.5);
                    transition: all 500ms ease;
                    z-index: 9999;
                }

                .container {
                    height: 100vh;
                    width: 328px;
                    -moz-box-flex: 0;
                    flex-grow: 0;
                    box-shadow: rgb(230, 235, 241) -1px 0px 0px 0px;
                    background-color: white;
                    overflow-y: auto;
                    position: relative;
                    right: 550px;
                    
                    }
                    
                header {
                    position: relative;
                    height: 84px;
                    padding: 25px 16px;
                    display: flex;
                    flex-direction: row;
                    -moz-box-pack: justify;
                    justify-content: space-between;
                    -moz-box-align: center;
                    align-items: center;
                    cursor: pointer;
                }

                .sections span{
                    font-size: 12px;
                    font-family: Nunito-Regular;
                    line-height: 1.83;
                    letter-spacing: normal;
                    color: rgb(106, 105, 110);
                    padding: 10px 14px;
                    display: block;
                }
                
                  

                .close {
                    position: absolute;
                    display: flex;
                    right: 0;
                    top: 0;
                }
                
                .sections {
                    font-weight: bold;
                    position: relative;
}
                }
                    

                .sections, .others, .countries {
                    display: flex;
                    flex-direction: column;
                    padding: 20px;
                }

                span {
                   padding: 20px;
                }

                    
                    }
                `}
            </style>
        </>
    )
    
    
}

export default MenuNavBar
