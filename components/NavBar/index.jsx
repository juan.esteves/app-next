import Link from 'next/link'
import {connect} from 'react-redux'
import {AiOutlineShoppingCart} from 'react-icons/ai'
import {FaBars,FaSearch } from 'react-icons/fa'
import SearchBar from '@components/SearchBar.jsx';
import { useState } from 'react';
import MenuNavBar from './MenuNavBar'


const NavBar = ({cart}) => {
    const [toggle, setToggle] = useState(true)


    const menuToggle = () =>{
        setToggle(!toggle)
             }
        
    const reducer = (accumulator, currentValue) => accumulator + currentValue;


    return (
        
            <header>
                <div className='headerWrapper'>
                    <div className='wrapper' onClick={menuToggle}>
                        <FaBars/>
                    </div>
                    <div className={toggle ? "menuWrapper" : ""} >
                        <MenuNavBar menuToggle={menuToggle}/>
                    </div>
                    <div className="ubication">
                        <span>Dirección de entrega: Ingresar mi ubicación</span>
                    </div>
                    <div className='logoWrapper'>
                        <Link href='/' >LOGO</Link>
                    </div>
                </div>
                <div className="logo">
                    <Link href='/' >LOGO</Link>
                </div>

                <div className='rightContainer'>
                    <div className="search-bar" >
                        <SearchBar/>
                        
                    </div>
                    <span className='search'><FaSearch/></span>
                    <div className="nav_cart" >
                        <span>
                            {cart.length}
                        </span>
                        <Link href="/cart">
                            <AiOutlineShoppingCart size={20}/>
                        </Link>
                    </div>

                    <div className='login'>
                        <span><Link href='/'>Ingreso</Link></span>
                    </div>
                </div>
                
                <style jsx>{`
                    header{
                        
                        display: flex;
                        align-items: center;
                        padding: 24px 40px;
                        justify-content: space-between;
                    }
                    .headerWrapper{
                        display: flex;
                        justify-content: space-around;
                    }
                    .wrapper{
                        padding-right: 40px;
                        margin-right: 40px;
                        border-right: 1px solid black;
                    }
                    .rightContainer{
                        display: flex;
                        justify-content:end;
                        gap: 10px;
                        align-items: center;
                    }
                    .menuWrapper{
                        display:none;
                    }
                    .logoWrapper, .search{
                        visibility: hidden;
                    }
                    .nav_cart{
                        cursor: pointer;
                        position: relative;
                    }
                    .nav_cart span{
                        color:red;
                        position: absolute;
                        top:-12px;
                        right: -7px;
                        background: crimson;
                        font-size: 10px;
                        color: white;
                        padding: 3px 5px;
                        border-radius: 50%;
                        z-index: -1;
                    }
                    @media (max-width: 1024px){
                        .search-bar, .ubication, .logo{
                            display: none;
                        }
                        .logoWrapper, .search{
                        visibility: visible;
                        }
                        }


                                        
                `}</style>

            </header>

    )
}


const mapStateToProps = (state) => {
    return state.shoppingReducer
     }
  
 
  
export default connect(mapStateToProps, null )(NavBar)

