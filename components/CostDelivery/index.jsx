import React from 'react'
import {MdMonetizationOn} from 'react-icons/md'
import {BiTimeFive} from 'react-icons/bi'


const CostDelivery = () => {
    return (
        <>
            <div className='container'>
                <div>
                    <MdMonetizationOn/>
                    Costo de envío: $ 2.000
                </div>
                <div>
                    <BiTimeFive/>
                    Entrega en: 30 min
                </div>
            </div>
            <style jsx>{`
                .container {
                    grid-area: restcost;
                    border-top: 1px solid rgb(230, 235, 241);
                    display: flex;
                    -moz-box-align: center;
                    align-items: center;
                    -moz-box-pack: justify;
                    justify-content: space-between;
                }
                
                .container div{
                    font-size: 14px;
                    font-family: Nunito-Regular;
                    line-height: 1.71;
                    letter-spacing: normal;
                    color: rgb(46, 44, 54);
                }

                .container div:first-child {
                    text-align: left;
                    padding: 12px 16px;
                    display: flex;
                    -moz-box-align: center;
                    align-items: center;
                    border-right: medium none;
                    width: 33%;
                }
                .container div:nth-child(2n) {
                    text-align: right;
                    width: 50%;
                    padding: 12px 16px;
                    display: flex;
                    -moz-box-align: center;
                    align-items: center;
                    -moz-box-pack: end;
                    justify-content: flex-end;
                }

                @media(max-width: 769px) {
                    .container div:first-child {
                        width: 50%;
                        border-right: 1px solid rgb(230, 235, 241);
                }
                
                }
                
            `}

            </style>
        </>
    )   
}

export default CostDelivery
