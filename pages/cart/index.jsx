import React from 'react';
import {connect} from 'react-redux';
import Link from 'next/link';
import Cart from '@components/Cart';


const CartRoute = (props) => {
    console.log(props)
    

    return (
        <div>
            <Link href='/'>HOME</Link>
            <Cart data={props}/>
            
        </div>
    )
}


const mapStateToProps = (state) => {
    return state.shoppingReducer
  }
  
  
export default connect(mapStateToProps, null)(CartRoute)

