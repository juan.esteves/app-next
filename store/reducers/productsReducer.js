import * as types from '../types'


const initialState= {
    products: [],
    cart: [],
    loading: false,
    error:''
}


const productsReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.GET_ALL:
            return {
                ...state,
                products: action.payload,
                loading: false,
                error: '',
                total:0
            };

        case types.LOADING:
            return {...state, loading: true};

        case types.ERROR:
                    return {...state, error: action.payload, loading: false};
                    
        default:
            return state
    }
}
            
export default productsReducer;