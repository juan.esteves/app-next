import * as types from '../types'


const initialState= {
    products: [],
    cart: [],
    loading: false,
    error:'',
    total:0
}




const shoppingReducer = (state = initialState, action) => {
    switch (action.type) {
        
        case types.ADD_CART:
            console.log(action)
            let itemInCart = state.cart.find(item=>item.id === action.payload.id)
            return itemInCart 
            ?{
                ...state,
                cart: state.cart.map(item=>item.id === action.payload.id 
                    ? {...item, quantity: item.quantity+1}
                    : item)
            }
            :{
                ...state,
                cart:[...state.cart, {...action.payload, quantity: 1}]
            }
        
            
        case types.REMOVE_ONE_FROM_CART:
            if(window.confirm("Do you want to delete this product?")){
                return {
                    ...state, 
                    cart: state.cart.filter(items => items.id !== action.payload)
                    
                }
            }

        case types.REMOVE_ALL_FROM_CART:
            if(window.confirm("Do you want to delete all products?")){    
                return {
                        ...state,
                        cart:[]
                        }
            }
            
        case types.GET_TOTAL:
            const res = state.cart.reduce((prev, item) => {
                    return prev + (item.price * item.quantity);
                },0)
            return {
                ...state,
                total: res
            }
               
        
            
        default:
            return state
    }
}

export default shoppingReducer;