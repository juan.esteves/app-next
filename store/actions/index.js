import * as types from '../types'
import axios from 'axios'

    export const getAll = () =>async(dispatch) => {
        dispatch({
            type: types.LOADING
        });

        try{
            const response = await axios.get('api/app')
            //console.log(response.data.data)    
            dispatch({
                type: types.GET_ALL,
                payload: response.data.data
            })
        }
        catch(err) {
            console.log(err.message);
            dispatch({
                type: types.ERROR,
                payload: 'Algo salió mal, intente de nuevo más tarde'
            })
        }
    }

    export const addToCart = (payload) => {
        return {
            type: types.ADD_CART,
            payload
                
            
        }
    }

    export const increse = (payload) => {
        return {
            type: types.INCRESE,
            payload
                
        }    
    }

    export const reduce = (payload) => {
        return {
            type: types.REDUCE,
            payload
                
        }    
    }
    
    export const removeOne = (payload) => {
        return {
            type: types.REMOVE_ONE_FROM_CART,
            payload
                
        }    
    }

    export const removeAll = (payload) => {
        return {
            type: types.REMOVE_ALL_FROM_CART,
            payload
                
            
        }
    }

    export const getTotal = () => {
        return {
            type: types.GET_TOTAL,
            
                
            
        }
    };
   

    