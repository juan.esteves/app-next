"use strict";
(() => {
var exports = {};
exports.id = "pages/api/app";
exports.ids = ["pages/api/app"];
exports.modules = {

/***/ "./database/data.js":
/*!**************************!*\
  !*** ./database/data.js ***!
  \**************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
const data = {
  '001': {
    name: 'Pizza Hawuaiana',
    id: '001',
    price: 9500,
    image: '/images/maluma.jpg',
    attributes: {
      description: 'A relatively new cultivar, it was, the pretty boy baby, discovered in South Africa in the early 1990s by Mr. A.G. (Dries) Joubert. Maluma Babyy. It is a chance seedling of unknown parentage',
      category: 'Pizza',
      hardiness: 'Pizza de jamón y piña con salsa pomodoro y queso mozzarella. Viene en 4 porciones de 27 cm de diámetro.',
      taste: 'Catchy, is Italian'
    }
  },
  '002': {
    name: 'Pizza Pollo BBQ',
    id: '002',
    price: 9.500,
    image: '/images/fuerte.jpg',
    attributes: {
      description: 'The Fuerte avocado is the second largest commercial variety behind Hass. It is a long time California commercial variety valued for its winter season ripening and its B-Type blossom type which most growers plant adjacent to the Hass for a more consistent production cycle. This avocado tends to produce heavily in alternate years.',
      category: 'Pizza',
      hardiness: 'Pizza de jamón y piña con salsa pomodoro y queso mozzarella. Viene en 4 porciones de 27 cm de diámetro.',
      taste: 'Magnificent, is Italian'
    }
  },
  '003': {
    name: 'Pizza mexicana',
    id: '003',
    price: 125,
    image: '/images/gwen.jpg',
    attributes: {
      description: "A seedling bred from 'Hass' x 'Thille' in 1982, 'Gwen' is higher yielding and more dwarfing than 'Hass' in California. The fruit has an oval category, slightly smaller than 'Hass' (100–200 g or 3.5–7.1 oz), with a rich, nutty flavor. The skin texture is more finely pebbled than 'Hass', and is dull green when ripe. It is frost-hardy down to −1 °C (30 °F)",
      category: 'Pizza',
      hardiness: 'Pizza de jamón y piña con salsa pomodoro y queso mozzarella. Viene en 4 porciones de 27 cm de diámetro.',
      taste: 'Superb, is Italian'
    }
  },
  '004': {
    name: 'Pizza carnes',
    id: '004',
    price: 9500,
    image: '/images/bacon.jpg',
    attributes: {
      description: 'Developed by a farmer, James Bacon, in 1954, Bacon has medium-sized fruit with smooth, green skin with yellow-green, light-tasting flesh. When ripe, the skin remains green, but darkens slightly, and fruit yields to gentle pressure. It is cold-hardy down to −5 °C (23 °F)',
      category: 'Pizza',
      hardiness: 'Pizza de jamón y piña con salsa pomodoro y queso mozzarella. Viene en 4 porciones de 27 cm de diámetro.',
      taste: 'Creamy, is Italian'
    }
  },
  '005': {
    name: 'Lasagna',
    id: '005',
    price: 12000,
    image: '/images/hass.jpg',
    attributes: {
      description: "The 'Hass' is the most common cultivar of avocado. It produces fruit year-round and accounts for 80% of cultivated avocados in the world.[21][55] All 'Hass' trees are descended from a single 'mother tree' raised by a mail carrier named Rudolph Hass, of La Habra Heights, California.[20][55] Hass patented the productive tree in 1935. The 'mother tree', of uncertain subspecies, died of root rot and was cut down in September 2002.[21][55][56] 'Hass' trees have medium-sized (150–250 g or 5.3–8.8 oz), ovate fruit with a black, pebbled skin. The flesh has a nutty, rich flavor with 19% oil. A hybrid Guatemalan type can withstand temperatures to −1 °C (30 °F)",
      category: 'Pasta',
      hardiness: 'Pizza de jamón y piña con salsa pomodoro y queso mozzarella. Viene en 4 porciones de 27 cm de diámetro.',
      taste: 'Gorgeous, is Italian'
    }
  },
  '006': {
    name: 'Pasta Bolonesa',
    id: '006',
    price: 10000,
    image: '/images/lamb.jpg',
    attributes: {
      description: 'The Lamb Hass avocado is a cross between a Hass and Gwen avocado. The fruits are larger in size and later maturing than Hass. It is gaining in popularity as a commercial and backyard variety due to its exceptional flavor and easy peeling qualities. The tree has an upright, compact habit.',
      category: 'Pasta',
      hardiness: 'Pizza de jamón y piña con salsa pomodoro y queso mozzarella. Viene en 4 porciones de 27 cm de diámetro.',
      taste: 'Great, is Italian'
    }
  },
  '007': {
    name: 'Pasta salsa blanca',
    id: '007',
    price: 10.000,
    image: '/images/pinkerton.jpg',
    attributes: {
      description: "First grown on the Pinkerton Ranch in Saticoy, California, in the early 1970s, 'Pinkerton' is a seedling of 'Hass' x 'Rincon'. The large fruit has a small seed, and its green skin deepens in color as it ripens. The thick flesh has a smooth, creamy texture, pale green color, good flavor, and high oil content. It shows some cold tolerance, to −1 °C (30 °F) and bears consistently heavy crops. A hybrid Guatemalan type, it has excellent peeling characteristics",
      category: 'Pasta',
      hardiness: 'Pizza de jamón y piña con salsa pomodoro y queso mozzarella. Viene en 4 porciones de 27 cm de diámetro.',
      taste: 'Marvelous, is Italian'
    }
  },
  '008': {
    name: 'Panzerotti',
    id: '008',
    price: 9000,
    image: '/images/reed.jpg',
    attributes: {
      description: 'Developed from a chance seedling found in 1948 by James S. Reed in California, this cultivar has large, round, green fruit with a smooth texture and dark, thick, glossy skin. Smooth and delicate, the flesh has a slightly nutty flavor. The skin ripens green. A Guatemalan type, it is hardy to −1 °C (30 °F). Tree size is about 5 by 4 m (16.4 by 13.1 ft)',
      category: 'Pan',
      hardiness: 'Pizza de jamón y piña con salsa pomodoro y queso mozzarella. Viene en 4 porciones de 27 cm de diámetro.',
      taste: 'Exquisite, is Italian'
    }
  },
  '009': {
    name: 'PaneQ',
    id: '009',
    price: 12000,
    image: '/images/zutano.jpg',
    attributes: {
      description: 'The Zutano avocado is a cold hardy, consistent producing avocado variety. It resembles the Fuerte in appearance but is less flavorful but more cold hardy. The green fruits are obovate in category with waxy bumps on the skin. The flesh has a low oil but high water content which causes it to have a more fibrous texture.',
      category: 'Pann',
      hardiness: 'Pizza de jamón y piña con salsa pomodoro y queso mozzarella. Viene en 4 porciones de 27 cm de diámetro.',
      taste: 'Splendid, is Italian'
    }
  },
  '010': {
    name: 'PaneQ',
    id: '010',
    price: 12000,
    image: '/images/zutano.jpg',
    attributes: {
      description: 'The Zutano avocado is a cold hardy, consistent producing avocado variety. It resembles the Fuerte in appearance but is less flavorful but more cold hardy. The green fruits are obovate in category with waxy bumps on the skin. The flesh has a low oil but high water content which causes it to have a more fibrous texture.',
      category: 'Pann',
      hardiness: 'Pizza de jamón y piña con salsa pomodoro y queso mozzarella. Viene en 4 porciones de 27 cm de diámetro.',
      taste: 'Splendid, is Italian'
    }
  },
  '011': {
    name: 'PaneQ',
    id: '011',
    price: 12000,
    image: '/images/zutano.jpg',
    attributes: {
      description: 'The Zutano avocado is a cold hardy, consistent producing avocado variety. It resembles the Fuerte in appearance but is less flavorful but more cold hardy. The green fruits are obovate in category with waxy bumps on the skin. The flesh has a low oil but high water content which causes it to have a more fibrous texture.',
      category: 'Pann',
      hardiness: 'Pizza de jamón y piña con salsa pomodoro y queso mozzarella. Viene en 4 porciones de 27 cm de diámetro.',
      taste: 'Splendid, is Italian'
    }
  },
  '012': {
    name: 'Pizza Hawuaiana',
    id: '012',
    price: 9500,
    image: '/images/maluma.jpg',
    attributes: {
      description: 'A relatively new cultivar, it was, the pretty boy baby, discovered in South Africa in the early 1990s by Mr. A.G. (Dries) Joubert. Maluma Babyy. It is a chance seedling of unknown parentage',
      category: 'Pizza',
      hardiness: 'Pizza de jamón y piña con salsa pomodoro y queso mozzarella. Viene en 4 porciones de 27 cm de diámetro.',
      taste: 'Catchy, is Italian'
    }
  },
  '013': {
    name: 'Pizza Pollo BBQ',
    id: '013',
    price: 9.500,
    image: '/images/fuerte.jpg',
    attributes: {
      description: 'The Fuerte avocado is the second largest commercial variety behind Hass. It is a long time California commercial variety valued for its winter season ripening and its B-Type blossom type which most growers plant adjacent to the Hass for a more consistent production cycle. This avocado tends to produce heavily in alternate years.',
      category: 'Pizza',
      hardiness: 'Pizza de jamón y piña con salsa pomodoro y queso mozzarella. Viene en 4 porciones de 27 cm de diámetro.',
      taste: 'Magnificent, is Italian'
    }
  },
  '014': {
    name: 'Pizza Pollo BBQ',
    id: '014',
    price: 9.500,
    image: '/images/fuerte.jpg',
    attributes: {
      description: 'The Fuerte avocado is the second largest commercial variety behind Hass. It is a long time California commercial variety valued for its winter season ripening and its B-Type blossom type which most growers plant adjacent to the Hass for a more consistent production cycle. This avocado tends to produce heavily in alternate years.',
      category: 'Pizza',
      hardiness: 'Pizza de jamón y piña con salsa pomodoro y queso mozzarella. Viene en 4 porciones de 27 cm de diámetro.',
      taste: 'Magnificent, is Italian'
    }
  },
  '015': {
    name: 'Pizza mexicana',
    id: '015',
    price: 125,
    image: '/images/gwen.jpg',
    attributes: {
      description: "A seedling bred from 'Hass' x 'Thille' in 1982, 'Gwen' is higher yielding and more dwarfing than 'Hass' in California. The fruit has an oval category, slightly smaller than 'Hass' (100–200 g or 3.5–7.1 oz), with a rich, nutty flavor. The skin texture is more finely pebbled than 'Hass', and is dull green when ripe. It is frost-hardy down to −1 °C (30 °F)",
      category: 'Pizza',
      hardiness: 'Pizza de jamón y piña con salsa pomodoro y queso mozzarella. Viene en 4 porciones de 27 cm de diámetro.',
      taste: 'Superb, is Italian'
    }
  },
  '016': {
    name: 'Pizza carnes',
    id: '016',
    price: 9500,
    image: '/images/bacon.jpg',
    attributes: {
      description: 'Developed by a farmer, James Bacon, in 1954, Bacon has medium-sized fruit with smooth, green skin with yellow-green, light-tasting flesh. When ripe, the skin remains green, but darkens slightly, and fruit yields to gentle pressure. It is cold-hardy down to −5 °C (23 °F)',
      category: 'Pizza',
      hardiness: 'Pizza de jamón y piña con salsa pomodoro y queso mozzarella. Viene en 4 porciones de 27 cm de diámetro.',
      taste: 'Creamy, is Italian'
    }
  },
  '017': {
    name: 'Lasagna',
    id: '017',
    price: 12000,
    image: '/images/hass.jpg',
    attributes: {
      description: "The 'Hass' is the most common cultivar of avocado. It produces fruit year-round and accounts for 80% of cultivated avocados in the world.[21][55] All 'Hass' trees are descended from a single 'mother tree' raised by a mail carrier named Rudolph Hass, of La Habra Heights, California.[20][55] Hass patented the productive tree in 1935. The 'mother tree', of uncertain subspecies, died of root rot and was cut down in September 2002.[21][55][56] 'Hass' trees have medium-sized (150–250 g or 5.3–8.8 oz), ovate fruit with a black, pebbled skin. The flesh has a nutty, rich flavor with 19% oil. A hybrid Guatemalan type can withstand temperatures to −1 °C (30 °F)",
      category: 'Pasta',
      hardiness: 'Pizza de jamón y piña con salsa pomodoro y queso mozzarella. Viene en 4 porciones de 27 cm de diámetro.',
      taste: 'Gorgeous, is Italian'
    }
  },
  '018': {
    name: 'Pasta Bolonesa',
    id: '018',
    price: 10000,
    image: '/images/lamb.jpg',
    attributes: {
      description: 'The Lamb Hass avocado is a cross between a Hass and Gwen avocado. The fruits are larger in size and later maturing than Hass. It is gaining in popularity as a commercial and backyard variety due to its exceptional flavor and easy peeling qualities. The tree has an upright, compact habit.',
      category: 'Pasta',
      hardiness: 'Pizza de jamón y piña con salsa pomodoro y queso mozzarella. Viene en 4 porciones de 27 cm de diámetro.',
      taste: 'Great, is Italian'
    }
  },
  '019': {
    name: 'Pasta salsa blanca',
    id: '019',
    price: 10.000,
    image: '/images/pinkerton.jpg',
    attributes: {
      description: "First grown on the Pinkerton Ranch in Saticoy, California, in the early 1970s, 'Pinkerton' is a seedling of 'Hass' x 'Rincon'. The large fruit has a small seed, and its green skin deepens in color as it ripens. The thick flesh has a smooth, creamy texture, pale green color, good flavor, and high oil content. It shows some cold tolerance, to −1 °C (30 °F) and bears consistently heavy crops. A hybrid Guatemalan type, it has excellent peeling characteristics",
      category: 'Pasta',
      hardiness: 'Pizza de jamón y piña con salsa pomodoro y queso mozzarella. Viene en 4 porciones de 27 cm de diámetro.',
      taste: 'Marvelous, is Italian'
    }
  },
  '020': {
    name: 'Panzerotti',
    id: '020',
    price: 9000,
    image: '/images/reed.jpg',
    attributes: {
      description: 'Developed from a chance seedling found in 1948 by James S. Reed in California, this cultivar has large, round, green fruit with a smooth texture and dark, thick, glossy skin. Smooth and delicate, the flesh has a slightly nutty flavor. The skin ripens green. A Guatemalan type, it is hardy to −1 °C (30 °F). Tree size is about 5 by 4 m (16.4 by 13.1 ft)',
      category: 'Pan',
      hardiness: 'Pizza de jamón y piña con salsa pomodoro y queso mozzarella. Viene en 4 porciones de 27 cm de diámetro.',
      taste: 'Exquisite, is Italian'
    }
  },
  '021': {
    name: 'PaneQ',
    id: '021',
    price: 12000,
    image: '/images/zutano.jpg',
    attributes: {
      description: 'The Zutano avocado is a cold hardy, consistent producing avocado variety. It resembles the Fuerte in appearance but is less flavorful but more cold hardy. The green fruits are obovate in category with waxy bumps on the skin. The flesh has a low oil but high water content which causes it to have a more fibrous texture.',
      category: 'Pann',
      hardiness: 'Pizza de jamón y piña con salsa pomodoro y queso mozzarella. Viene en 4 porciones de 27 cm de diámetro.',
      taste: 'Splendid, is Italian'
    }
  },
  '022': {
    name: 'PaneQ',
    id: '022',
    price: 12000,
    image: '/images/zutano.jpg',
    attributes: {
      description: 'The Zutano avocado is a cold hardy, consistent producing avocado variety. It resembles the Fuerte in appearance but is less flavorful but more cold hardy. The green fruits are obovate in category with waxy bumps on the skin. The flesh has a low oil but high water content which causes it to have a more fibrous texture.',
      category: 'Pann',
      hardiness: 'Pizza de jamón y piña con salsa pomodoro y queso mozzarella. Viene en 4 porciones de 27 cm de diámetro.',
      taste: 'Splendid, is Italian'
    }
  },
  '023': {
    name: 'PaneQ',
    id: '023',
    price: 12000,
    image: '/images/zutano.jpg',
    attributes: {
      description: 'The Zutano avocado is a cold hardy, consistent producing avocado variety. It resembles the Fuerte in appearance but is less flavorful but more cold hardy. The green fruits are obovate in category with waxy bumps on the skin. The flesh has a low oil but high water content which causes it to have a more fibrous texture.',
      category: 'Pann',
      hardiness: 'Pizza de jamón y piña con salsa pomodoro y queso mozzarella. Viene en 4 porciones de 27 cm de diámetro.',
      taste: 'Splendid, is Italian'
    }
  }
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (data);

/***/ }),

/***/ "./database/db.js":
/*!************************!*\
  !*** ./database/db.js ***!
  \************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _data__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./data */ "./database/data.js");
// Oh you curious...
// This is not a real database,
// But let's imagine it is one :)


class Database {
  constructor() {}

  async getAll() {
    const asArray = Object.values(_data__WEBPACK_IMPORTED_MODULE_0__.default);
    await randomDelay();
    return asArray;
  }

  async getById(id) {
    if (!Object.prototype.hasOwnProperty.call(_data__WEBPACK_IMPORTED_MODULE_0__.default, id)) {
      return null;
    }

    const entry = _data__WEBPACK_IMPORTED_MODULE_0__.default[id];
    await randomDelay();
    return entry;
  }

} // Let's also add a delay to make it a bit closer to reality


const randomDelay = () => new Promise(resolve => {
  const max = 350;
  const min = 100;
  const delay = Math.floor(Math.random() * (max - min + 1)) + min;
  setTimeout(resolve, delay);
});

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Database);

/***/ }),

/***/ "./pages/api/app/index.js":
/*!********************************!*\
  !*** ./pages/api/app/index.js ***!
  \********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _database__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @database */ "./database/db.js");


const allproducts = async (req, res) => {
  try {
    const db = new _database__WEBPACK_IMPORTED_MODULE_0__.default();
    const allEntries = await db.getAll();
    const lenght = allEntries.length; // Notice: We're manually setting the response object
    // However Next.JS offers Express-like helpers :)
    // https://nextjs.org/docs/api-routes/response-helpers

    res.statusCode = 200;
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify({
      lenght,
      data: allEntries
    }));
  } catch (e) {
    console.error(e);
    res.statusCode = 500;
    res.end(JSON.stringify({
      length: 0,
      data: [],
      error: 'Something went wrong'
    }));
  }
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (allproducts);

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = (__webpack_exec__("./pages/api/app/index.js"));
module.exports = __webpack_exports__;

})();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFnZXMvYXBpL2FwcC5qcyIsIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLE1BQU1BLElBQUksR0FBRztBQUNULFNBQU87QUFDTEMsSUFBQUEsSUFBSSxFQUFFLGlCQUREO0FBRUxDLElBQUFBLEVBQUUsRUFBRSxLQUZDO0FBR0xDLElBQUFBLEtBQUssRUFBRSxJQUhGO0FBSUxDLElBQUFBLEtBQUssRUFBRSxvQkFKRjtBQUtMQyxJQUFBQSxVQUFVLEVBQUU7QUFDVkMsTUFBQUEsV0FBVyxFQUNULCtMQUZRO0FBR1ZDLE1BQUFBLFFBQVEsRUFBRSxPQUhBO0FBSVZDLE1BQUFBLFNBQVMsRUFBRSx5R0FKRDtBQUtWQyxNQUFBQSxLQUFLLEVBQUU7QUFMRztBQUxQLEdBREU7QUFjVCxTQUFPO0FBQ0xSLElBQUFBLElBQUksRUFBRSxpQkFERDtBQUVMQyxJQUFBQSxFQUFFLEVBQUUsS0FGQztBQUdMQyxJQUFBQSxLQUFLLEVBQUUsS0FIRjtBQUlMQyxJQUFBQSxLQUFLLEVBQUUsb0JBSkY7QUFLTEMsSUFBQUEsVUFBVSxFQUFFO0FBQ1ZDLE1BQUFBLFdBQVcsRUFDVCw0VUFGUTtBQUdWQyxNQUFBQSxRQUFRLEVBQUUsT0FIQTtBQUlWQyxNQUFBQSxTQUFTLEVBQUUseUdBSkQ7QUFLVkMsTUFBQUEsS0FBSyxFQUFFO0FBTEc7QUFMUCxHQWRFO0FBMkJULFNBQU87QUFDTFIsSUFBQUEsSUFBSSxFQUFFLGdCQUREO0FBRUxDLElBQUFBLEVBQUUsRUFBRSxLQUZDO0FBR0xDLElBQUFBLEtBQUssRUFBRSxHQUhGO0FBSUxDLElBQUFBLEtBQUssRUFBRSxrQkFKRjtBQUtMQyxJQUFBQSxVQUFVLEVBQUU7QUFDVkMsTUFBQUEsV0FBVyxFQUNULHFXQUZRO0FBR1ZDLE1BQUFBLFFBQVEsRUFBRSxPQUhBO0FBSVZDLE1BQUFBLFNBQVMsRUFBRSx5R0FKRDtBQUtWQyxNQUFBQSxLQUFLLEVBQUU7QUFMRztBQUxQLEdBM0JFO0FBd0NULFNBQU87QUFDTFIsSUFBQUEsSUFBSSxFQUFFLGNBREQ7QUFFTEMsSUFBQUEsRUFBRSxFQUFFLEtBRkM7QUFHTEMsSUFBQUEsS0FBSyxFQUFFLElBSEY7QUFJTEMsSUFBQUEsS0FBSyxFQUFFLG1CQUpGO0FBS0xDLElBQUFBLFVBQVUsRUFBRTtBQUNWQyxNQUFBQSxXQUFXLEVBQ1QsZ1JBRlE7QUFHVkMsTUFBQUEsUUFBUSxFQUFFLE9BSEE7QUFJVkMsTUFBQUEsU0FBUyxFQUFFLHlHQUpEO0FBS1ZDLE1BQUFBLEtBQUssRUFBRTtBQUxHO0FBTFAsR0F4Q0U7QUFxRFQsU0FBTztBQUNMUixJQUFBQSxJQUFJLEVBQUUsU0FERDtBQUVMQyxJQUFBQSxFQUFFLEVBQUUsS0FGQztBQUdMQyxJQUFBQSxLQUFLLEVBQUUsS0FIRjtBQUlMQyxJQUFBQSxLQUFLLEVBQUUsa0JBSkY7QUFLTEMsSUFBQUEsVUFBVSxFQUFFO0FBQ1ZDLE1BQUFBLFdBQVcsRUFDVCxvcEJBRlE7QUFHVkMsTUFBQUEsUUFBUSxFQUFFLE9BSEE7QUFJVkMsTUFBQUEsU0FBUyxFQUFFLHlHQUpEO0FBS1ZDLE1BQUFBLEtBQUssRUFBRTtBQUxHO0FBTFAsR0FyREU7QUFrRVQsU0FBTztBQUNMUixJQUFBQSxJQUFJLEVBQUUsZ0JBREQ7QUFFTEMsSUFBQUEsRUFBRSxFQUFFLEtBRkM7QUFHTEMsSUFBQUEsS0FBSyxFQUFFLEtBSEY7QUFJTEMsSUFBQUEsS0FBSyxFQUFFLGtCQUpGO0FBS0xDLElBQUFBLFVBQVUsRUFBRTtBQUNWQyxNQUFBQSxXQUFXLEVBQ1Qsa1NBRlE7QUFHVkMsTUFBQUEsUUFBUSxFQUFFLE9BSEE7QUFJVkMsTUFBQUEsU0FBUyxFQUFFLHlHQUpEO0FBS1ZDLE1BQUFBLEtBQUssRUFBRTtBQUxHO0FBTFAsR0FsRUU7QUErRVQsU0FBTztBQUNMUixJQUFBQSxJQUFJLEVBQUUsb0JBREQ7QUFFTEMsSUFBQUEsRUFBRSxFQUFFLEtBRkM7QUFHTEMsSUFBQUEsS0FBSyxFQUFFLE1BSEY7QUFJTEMsSUFBQUEsS0FBSyxFQUFFLHVCQUpGO0FBS0xDLElBQUFBLFVBQVUsRUFBRTtBQUNWQyxNQUFBQSxXQUFXLEVBQ1QsNmNBRlE7QUFHVkMsTUFBQUEsUUFBUSxFQUFFLE9BSEE7QUFJVkMsTUFBQUEsU0FBUyxFQUFFLHlHQUpEO0FBS1ZDLE1BQUFBLEtBQUssRUFBRTtBQUxHO0FBTFAsR0EvRUU7QUE0RlQsU0FBTztBQUNMUixJQUFBQSxJQUFJLEVBQUUsWUFERDtBQUVMQyxJQUFBQSxFQUFFLEVBQUUsS0FGQztBQUdMQyxJQUFBQSxLQUFLLEVBQUUsSUFIRjtBQUlMQyxJQUFBQSxLQUFLLEVBQUUsa0JBSkY7QUFLTEMsSUFBQUEsVUFBVSxFQUFFO0FBQ1ZDLE1BQUFBLFdBQVcsRUFDVCxrV0FGUTtBQUdWQyxNQUFBQSxRQUFRLEVBQUUsS0FIQTtBQUlWQyxNQUFBQSxTQUFTLEVBQUUseUdBSkQ7QUFLVkMsTUFBQUEsS0FBSyxFQUFFO0FBTEc7QUFMUCxHQTVGRTtBQXlHVCxTQUFPO0FBQ0xSLElBQUFBLElBQUksRUFBRSxPQUREO0FBRUxDLElBQUFBLEVBQUUsRUFBRSxLQUZDO0FBR0xDLElBQUFBLEtBQUssRUFBRSxLQUhGO0FBSUxDLElBQUFBLEtBQUssRUFBRSxvQkFKRjtBQUtMQyxJQUFBQSxVQUFVLEVBQUU7QUFDVkMsTUFBQUEsV0FBVyxFQUNULGlVQUZRO0FBR1ZDLE1BQUFBLFFBQVEsRUFBRSxNQUhBO0FBSVZDLE1BQUFBLFNBQVMsRUFBRSx5R0FKRDtBQUtWQyxNQUFBQSxLQUFLLEVBQUU7QUFMRztBQUxQLEdBekdFO0FBc0hQLFNBQU87QUFDTFIsSUFBQUEsSUFBSSxFQUFFLE9BREQ7QUFFTEMsSUFBQUEsRUFBRSxFQUFFLEtBRkM7QUFHTEMsSUFBQUEsS0FBSyxFQUFFLEtBSEY7QUFJTEMsSUFBQUEsS0FBSyxFQUFFLG9CQUpGO0FBS0xDLElBQUFBLFVBQVUsRUFBRTtBQUNWQyxNQUFBQSxXQUFXLEVBQ1QsaVVBRlE7QUFHVkMsTUFBQUEsUUFBUSxFQUFFLE1BSEE7QUFJVkMsTUFBQUEsU0FBUyxFQUFFLHlHQUpEO0FBS1ZDLE1BQUFBLEtBQUssRUFBRTtBQUxHO0FBTFAsR0F0SEE7QUFtSVAsU0FBTztBQUNMUixJQUFBQSxJQUFJLEVBQUUsT0FERDtBQUVMQyxJQUFBQSxFQUFFLEVBQUUsS0FGQztBQUdMQyxJQUFBQSxLQUFLLEVBQUUsS0FIRjtBQUlMQyxJQUFBQSxLQUFLLEVBQUUsb0JBSkY7QUFLTEMsSUFBQUEsVUFBVSxFQUFFO0FBQ1ZDLE1BQUFBLFdBQVcsRUFDVCxpVUFGUTtBQUdWQyxNQUFBQSxRQUFRLEVBQUUsTUFIQTtBQUlWQyxNQUFBQSxTQUFTLEVBQUUseUdBSkQ7QUFLVkMsTUFBQUEsS0FBSyxFQUFFO0FBTEc7QUFMUCxHQW5JQTtBQWdKUCxTQUFPO0FBQ0xSLElBQUFBLElBQUksRUFBRSxpQkFERDtBQUVMQyxJQUFBQSxFQUFFLEVBQUUsS0FGQztBQUdMQyxJQUFBQSxLQUFLLEVBQUUsSUFIRjtBQUlMQyxJQUFBQSxLQUFLLEVBQUUsb0JBSkY7QUFLTEMsSUFBQUEsVUFBVSxFQUFFO0FBQ1ZDLE1BQUFBLFdBQVcsRUFDVCwrTEFGUTtBQUdWQyxNQUFBQSxRQUFRLEVBQUUsT0FIQTtBQUlWQyxNQUFBQSxTQUFTLEVBQUUseUdBSkQ7QUFLVkMsTUFBQUEsS0FBSyxFQUFFO0FBTEc7QUFMUCxHQWhKQTtBQTZKUCxTQUFPO0FBQ0xSLElBQUFBLElBQUksRUFBRSxpQkFERDtBQUVMQyxJQUFBQSxFQUFFLEVBQUUsS0FGQztBQUdMQyxJQUFBQSxLQUFLLEVBQUUsS0FIRjtBQUlMQyxJQUFBQSxLQUFLLEVBQUUsb0JBSkY7QUFLTEMsSUFBQUEsVUFBVSxFQUFFO0FBQ1ZDLE1BQUFBLFdBQVcsRUFDVCw0VUFGUTtBQUdWQyxNQUFBQSxRQUFRLEVBQUUsT0FIQTtBQUlWQyxNQUFBQSxTQUFTLEVBQUUseUdBSkQ7QUFLVkMsTUFBQUEsS0FBSyxFQUFFO0FBTEc7QUFMUCxHQTdKQTtBQTBLUCxTQUFPO0FBQ0xSLElBQUFBLElBQUksRUFBRSxpQkFERDtBQUVMQyxJQUFBQSxFQUFFLEVBQUUsS0FGQztBQUdMQyxJQUFBQSxLQUFLLEVBQUUsS0FIRjtBQUlMQyxJQUFBQSxLQUFLLEVBQUUsb0JBSkY7QUFLTEMsSUFBQUEsVUFBVSxFQUFFO0FBQ1ZDLE1BQUFBLFdBQVcsRUFDVCw0VUFGUTtBQUdWQyxNQUFBQSxRQUFRLEVBQUUsT0FIQTtBQUlWQyxNQUFBQSxTQUFTLEVBQUUseUdBSkQ7QUFLVkMsTUFBQUEsS0FBSyxFQUFFO0FBTEc7QUFMUCxHQTFLQTtBQXVMUCxTQUFPO0FBQ0xSLElBQUFBLElBQUksRUFBRSxnQkFERDtBQUVMQyxJQUFBQSxFQUFFLEVBQUUsS0FGQztBQUdMQyxJQUFBQSxLQUFLLEVBQUUsR0FIRjtBQUlMQyxJQUFBQSxLQUFLLEVBQUUsa0JBSkY7QUFLTEMsSUFBQUEsVUFBVSxFQUFFO0FBQ1ZDLE1BQUFBLFdBQVcsRUFDVCxxV0FGUTtBQUdWQyxNQUFBQSxRQUFRLEVBQUUsT0FIQTtBQUlWQyxNQUFBQSxTQUFTLEVBQUUseUdBSkQ7QUFLVkMsTUFBQUEsS0FBSyxFQUFFO0FBTEc7QUFMUCxHQXZMQTtBQW9NUCxTQUFPO0FBQ0xSLElBQUFBLElBQUksRUFBRSxjQUREO0FBRUxDLElBQUFBLEVBQUUsRUFBRSxLQUZDO0FBR0xDLElBQUFBLEtBQUssRUFBRSxJQUhGO0FBSUxDLElBQUFBLEtBQUssRUFBRSxtQkFKRjtBQUtMQyxJQUFBQSxVQUFVLEVBQUU7QUFDVkMsTUFBQUEsV0FBVyxFQUNULGdSQUZRO0FBR1ZDLE1BQUFBLFFBQVEsRUFBRSxPQUhBO0FBSVZDLE1BQUFBLFNBQVMsRUFBRSx5R0FKRDtBQUtWQyxNQUFBQSxLQUFLLEVBQUU7QUFMRztBQUxQLEdBcE1BO0FBaU5QLFNBQU87QUFDTFIsSUFBQUEsSUFBSSxFQUFFLFNBREQ7QUFFTEMsSUFBQUEsRUFBRSxFQUFFLEtBRkM7QUFHTEMsSUFBQUEsS0FBSyxFQUFFLEtBSEY7QUFJTEMsSUFBQUEsS0FBSyxFQUFFLGtCQUpGO0FBS0xDLElBQUFBLFVBQVUsRUFBRTtBQUNWQyxNQUFBQSxXQUFXLEVBQ1Qsb3BCQUZRO0FBR1ZDLE1BQUFBLFFBQVEsRUFBRSxPQUhBO0FBSVZDLE1BQUFBLFNBQVMsRUFBRSx5R0FKRDtBQUtWQyxNQUFBQSxLQUFLLEVBQUU7QUFMRztBQUxQLEdBak5BO0FBOE5QLFNBQU87QUFDTFIsSUFBQUEsSUFBSSxFQUFFLGdCQUREO0FBRUxDLElBQUFBLEVBQUUsRUFBRSxLQUZDO0FBR0xDLElBQUFBLEtBQUssRUFBRSxLQUhGO0FBSUxDLElBQUFBLEtBQUssRUFBRSxrQkFKRjtBQUtMQyxJQUFBQSxVQUFVLEVBQUU7QUFDVkMsTUFBQUEsV0FBVyxFQUNULGtTQUZRO0FBR1ZDLE1BQUFBLFFBQVEsRUFBRSxPQUhBO0FBSVZDLE1BQUFBLFNBQVMsRUFBRSx5R0FKRDtBQUtWQyxNQUFBQSxLQUFLLEVBQUU7QUFMRztBQUxQLEdBOU5BO0FBMk9QLFNBQU87QUFDTFIsSUFBQUEsSUFBSSxFQUFFLG9CQUREO0FBRUxDLElBQUFBLEVBQUUsRUFBRSxLQUZDO0FBR0xDLElBQUFBLEtBQUssRUFBRSxNQUhGO0FBSUxDLElBQUFBLEtBQUssRUFBRSx1QkFKRjtBQUtMQyxJQUFBQSxVQUFVLEVBQUU7QUFDVkMsTUFBQUEsV0FBVyxFQUNULDZjQUZRO0FBR1ZDLE1BQUFBLFFBQVEsRUFBRSxPQUhBO0FBSVZDLE1BQUFBLFNBQVMsRUFBRSx5R0FKRDtBQUtWQyxNQUFBQSxLQUFLLEVBQUU7QUFMRztBQUxQLEdBM09BO0FBd1BQLFNBQU87QUFDTFIsSUFBQUEsSUFBSSxFQUFFLFlBREQ7QUFFTEMsSUFBQUEsRUFBRSxFQUFFLEtBRkM7QUFHTEMsSUFBQUEsS0FBSyxFQUFFLElBSEY7QUFJTEMsSUFBQUEsS0FBSyxFQUFFLGtCQUpGO0FBS0xDLElBQUFBLFVBQVUsRUFBRTtBQUNWQyxNQUFBQSxXQUFXLEVBQ1Qsa1dBRlE7QUFHVkMsTUFBQUEsUUFBUSxFQUFFLEtBSEE7QUFJVkMsTUFBQUEsU0FBUyxFQUFFLHlHQUpEO0FBS1ZDLE1BQUFBLEtBQUssRUFBRTtBQUxHO0FBTFAsR0F4UEE7QUFxUVAsU0FBTztBQUNMUixJQUFBQSxJQUFJLEVBQUUsT0FERDtBQUVMQyxJQUFBQSxFQUFFLEVBQUUsS0FGQztBQUdMQyxJQUFBQSxLQUFLLEVBQUUsS0FIRjtBQUlMQyxJQUFBQSxLQUFLLEVBQUUsb0JBSkY7QUFLTEMsSUFBQUEsVUFBVSxFQUFFO0FBQ1ZDLE1BQUFBLFdBQVcsRUFDVCxpVUFGUTtBQUdWQyxNQUFBQSxRQUFRLEVBQUUsTUFIQTtBQUlWQyxNQUFBQSxTQUFTLEVBQUUseUdBSkQ7QUFLVkMsTUFBQUEsS0FBSyxFQUFFO0FBTEc7QUFMUCxHQXJRQTtBQWtSTCxTQUFPO0FBQ0xSLElBQUFBLElBQUksRUFBRSxPQUREO0FBRUxDLElBQUFBLEVBQUUsRUFBRSxLQUZDO0FBR0xDLElBQUFBLEtBQUssRUFBRSxLQUhGO0FBSUxDLElBQUFBLEtBQUssRUFBRSxvQkFKRjtBQUtMQyxJQUFBQSxVQUFVLEVBQUU7QUFDVkMsTUFBQUEsV0FBVyxFQUNULGlVQUZRO0FBR1ZDLE1BQUFBLFFBQVEsRUFBRSxNQUhBO0FBSVZDLE1BQUFBLFNBQVMsRUFBRSx5R0FKRDtBQUtWQyxNQUFBQSxLQUFLLEVBQUU7QUFMRztBQUxQLEdBbFJGO0FBK1JMLFNBQU87QUFDTFIsSUFBQUEsSUFBSSxFQUFFLE9BREQ7QUFFTEMsSUFBQUEsRUFBRSxFQUFFLEtBRkM7QUFHTEMsSUFBQUEsS0FBSyxFQUFFLEtBSEY7QUFJTEMsSUFBQUEsS0FBSyxFQUFFLG9CQUpGO0FBS0xDLElBQUFBLFVBQVUsRUFBRTtBQUNWQyxNQUFBQSxXQUFXLEVBQ1QsaVVBRlE7QUFHVkMsTUFBQUEsUUFBUSxFQUFFLE1BSEE7QUFJVkMsTUFBQUEsU0FBUyxFQUFFLHlHQUpEO0FBS1ZDLE1BQUFBLEtBQUssRUFBRTtBQUxHO0FBTFA7QUEvUkYsQ0FBYjtBQXFURSxpRUFBZVQsSUFBZjs7Ozs7Ozs7Ozs7Ozs7O0FDclRGO0FBQ0E7QUFDQTtBQUNBOztBQUVBLE1BQU1XLFFBQU4sQ0FBZTtBQUNiQyxFQUFBQSxXQUFXLEdBQUcsQ0FBRTs7QUFFaEIsUUFBTUMsTUFBTixHQUFjO0FBQ1osVUFBTUMsT0FBTyxHQUFHQyxNQUFNLENBQUNDLE1BQVAsQ0FBY04sMENBQWQsQ0FBaEI7QUFDQSxVQUFNTyxXQUFXLEVBQWpCO0FBQ0EsV0FBT0gsT0FBUDtBQUNEOztBQUVELFFBQU1JLE9BQU4sQ0FBY2hCLEVBQWQsRUFBa0I7QUFDaEIsUUFBSSxDQUFDYSxNQUFNLENBQUNJLFNBQVAsQ0FBaUJDLGNBQWpCLENBQWdDQyxJQUFoQyxDQUFxQ1gsMENBQXJDLEVBQThDUixFQUE5QyxDQUFMLEVBQXdEO0FBQ3RELGFBQU8sSUFBUDtBQUNEOztBQUVELFVBQU1vQixLQUFLLEdBQUdaLDBDQUFPLENBQUNSLEVBQUQsQ0FBckI7QUFDQSxVQUFNZSxXQUFXLEVBQWpCO0FBQ0EsV0FBT0ssS0FBUDtBQUNEOztBQWpCWSxFQW9CZjs7O0FBQ0EsTUFBTUwsV0FBVyxHQUFHLE1BQ2xCLElBQUlNLE9BQUosQ0FBYUMsT0FBRCxJQUFhO0FBQ3ZCLFFBQU1DLEdBQUcsR0FBRyxHQUFaO0FBQ0EsUUFBTUMsR0FBRyxHQUFHLEdBQVo7QUFDQSxRQUFNQyxLQUFLLEdBQUdDLElBQUksQ0FBQ0MsS0FBTCxDQUFXRCxJQUFJLENBQUNFLE1BQUwsTUFBaUJMLEdBQUcsR0FBR0MsR0FBTixHQUFZLENBQTdCLENBQVgsSUFBOENBLEdBQTVEO0FBRUFLLEVBQUFBLFVBQVUsQ0FBQ1AsT0FBRCxFQUFVRyxLQUFWLENBQVY7QUFDRCxDQU5ELENBREY7O0FBU0EsaUVBQWVoQixRQUFmOzs7Ozs7Ozs7Ozs7Ozs7QUNuQ0E7O0FBR0EsTUFBTXNCLFdBQVcsR0FBRyxPQUFPQyxHQUFQLEVBQVlDLEdBQVosS0FBb0I7QUFDdEMsTUFBSTtBQUVGLFVBQU1DLEVBQUUsR0FBRyxJQUFJSiw4Q0FBSixFQUFYO0FBQ0EsVUFBTUssVUFBVSxHQUFHLE1BQU1ELEVBQUUsQ0FBQ3ZCLE1BQUgsRUFBekI7QUFDQSxVQUFNeUIsTUFBTSxHQUFHRCxVQUFVLENBQUNFLE1BQTFCLENBSkUsQ0FNRjtBQUNBO0FBQ0E7O0FBQ0FKLElBQUFBLEdBQUcsQ0FBQ0ssVUFBSixHQUFpQixHQUFqQjtBQUNBTCxJQUFBQSxHQUFHLENBQUNNLFNBQUosQ0FBYyxjQUFkLEVBQThCLGtCQUE5QjtBQUNBTixJQUFBQSxHQUFHLENBQUNPLEdBQUosQ0FBUUMsSUFBSSxDQUFDQyxTQUFMLENBQWU7QUFBRU4sTUFBQUEsTUFBRjtBQUFVdEMsTUFBQUEsSUFBSSxFQUFFcUM7QUFBaEIsS0FBZixDQUFSO0FBQ0QsR0FaRCxDQVlFLE9BQU9RLENBQVAsRUFBVTtBQUNWQyxJQUFBQSxPQUFPLENBQUNDLEtBQVIsQ0FBY0YsQ0FBZDtBQUNBVixJQUFBQSxHQUFHLENBQUNLLFVBQUosR0FBaUIsR0FBakI7QUFDQUwsSUFBQUEsR0FBRyxDQUFDTyxHQUFKLENBQ0VDLElBQUksQ0FBQ0MsU0FBTCxDQUFlO0FBQUVMLE1BQUFBLE1BQU0sRUFBRSxDQUFWO0FBQWF2QyxNQUFBQSxJQUFJLEVBQUUsRUFBbkI7QUFBdUIrQyxNQUFBQSxLQUFLLEVBQUU7QUFBOUIsS0FBZixDQURGO0FBR0Q7QUFDRixDQXBCRDs7QUFzQkEsaUVBQWVkLFdBQWYiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9hcHAvLi9kYXRhYmFzZS9kYXRhLmpzIiwid2VicGFjazovL2FwcC8uL2RhdGFiYXNlL2RiLmpzIiwid2VicGFjazovL2FwcC8uL3BhZ2VzL2FwaS9hcHAvaW5kZXguanMiXSwic291cmNlc0NvbnRlbnQiOlsiY29uc3QgZGF0YSA9IHtcclxuICAgICcwMDEnOiB7XHJcbiAgICAgIG5hbWU6ICdQaXp6YSBIYXd1YWlhbmEnLFxyXG4gICAgICBpZDogJzAwMScsXHJcbiAgICAgIHByaWNlOiA5NTAwLFxyXG4gICAgICBpbWFnZTogJy9pbWFnZXMvbWFsdW1hLmpwZycsXHJcbiAgICAgIGF0dHJpYnV0ZXM6IHtcclxuICAgICAgICBkZXNjcmlwdGlvbjpcclxuICAgICAgICAgICdBIHJlbGF0aXZlbHkgbmV3IGN1bHRpdmFyLCBpdCB3YXMsIHRoZSBwcmV0dHkgYm95IGJhYnksIGRpc2NvdmVyZWQgaW4gU291dGggQWZyaWNhIGluIHRoZSBlYXJseSAxOTkwcyBieSBNci4gQS5HLiAoRHJpZXMpIEpvdWJlcnQuIE1hbHVtYSBCYWJ5eS4gSXQgaXMgYSBjaGFuY2Ugc2VlZGxpbmcgb2YgdW5rbm93biBwYXJlbnRhZ2UnLFxyXG4gICAgICAgIGNhdGVnb3J5OiAnUGl6emEnLFxyXG4gICAgICAgIGhhcmRpbmVzczogJ1BpenphIGRlIGphbcOzbiB5IHBpw7FhIGNvbiBzYWxzYSBwb21vZG9ybyB5IHF1ZXNvIG1venphcmVsbGEuIFZpZW5lIGVuIDQgcG9yY2lvbmVzIGRlIDI3IGNtIGRlIGRpw6FtZXRyby4nLFxyXG4gICAgICAgIHRhc3RlOiAnQ2F0Y2h5LCBpcyBJdGFsaWFuJyxcclxuICAgICAgfSxcclxuICAgIH0sXHJcbiAgICAnMDAyJzoge1xyXG4gICAgICBuYW1lOiAnUGl6emEgUG9sbG8gQkJRJyxcclxuICAgICAgaWQ6ICcwMDInLFxyXG4gICAgICBwcmljZTogOS41MDAsXHJcbiAgICAgIGltYWdlOiAnL2ltYWdlcy9mdWVydGUuanBnJyxcclxuICAgICAgYXR0cmlidXRlczoge1xyXG4gICAgICAgIGRlc2NyaXB0aW9uOlxyXG4gICAgICAgICAgJ1RoZSBGdWVydGUgYXZvY2FkbyBpcyB0aGUgc2Vjb25kIGxhcmdlc3QgY29tbWVyY2lhbCB2YXJpZXR5IGJlaGluZCBIYXNzLiBJdCBpcyBhIGxvbmcgdGltZSBDYWxpZm9ybmlhIGNvbW1lcmNpYWwgdmFyaWV0eSB2YWx1ZWQgZm9yIGl0cyB3aW50ZXIgc2Vhc29uIHJpcGVuaW5nIGFuZCBpdHMgQi1UeXBlIGJsb3Nzb20gdHlwZSB3aGljaCBtb3N0IGdyb3dlcnMgcGxhbnQgYWRqYWNlbnQgdG8gdGhlIEhhc3MgZm9yIGEgbW9yZSBjb25zaXN0ZW50IHByb2R1Y3Rpb24gY3ljbGUuIFRoaXMgYXZvY2FkbyB0ZW5kcyB0byBwcm9kdWNlIGhlYXZpbHkgaW4gYWx0ZXJuYXRlIHllYXJzLicsXHJcbiAgICAgICAgY2F0ZWdvcnk6ICdQaXp6YScsXHJcbiAgICAgICAgaGFyZGluZXNzOiAnUGl6emEgZGUgamFtw7NuIHkgcGnDsWEgY29uIHNhbHNhIHBvbW9kb3JvIHkgcXVlc28gbW96emFyZWxsYS4gVmllbmUgZW4gNCBwb3JjaW9uZXMgZGUgMjcgY20gZGUgZGnDoW1ldHJvLicsXHJcbiAgICAgICAgdGFzdGU6ICdNYWduaWZpY2VudCwgaXMgSXRhbGlhbicsXHJcbiAgICAgIH0sXHJcbiAgICB9LFxyXG4gICAgJzAwMyc6IHtcclxuICAgICAgbmFtZTogJ1BpenphIG1leGljYW5hJyxcclxuICAgICAgaWQ6ICcwMDMnLFxyXG4gICAgICBwcmljZTogMTI1LFxyXG4gICAgICBpbWFnZTogJy9pbWFnZXMvZ3dlbi5qcGcnLFxyXG4gICAgICBhdHRyaWJ1dGVzOiB7XHJcbiAgICAgICAgZGVzY3JpcHRpb246XHJcbiAgICAgICAgICBcIkEgc2VlZGxpbmcgYnJlZCBmcm9tICdIYXNzJyB4ICdUaGlsbGUnIGluIDE5ODIsICdHd2VuJyBpcyBoaWdoZXIgeWllbGRpbmcgYW5kIG1vcmUgZHdhcmZpbmcgdGhhbiAnSGFzcycgaW4gQ2FsaWZvcm5pYS4gVGhlIGZydWl0IGhhcyBhbiBvdmFsIGNhdGVnb3J5LCBzbGlnaHRseSBzbWFsbGVyIHRoYW4gJ0hhc3MnICgxMDDigJMyMDAgZyBvciAzLjXigJM3LjEgb3opLCB3aXRoIGEgcmljaCwgbnV0dHkgZmxhdm9yLiBUaGUgc2tpbiB0ZXh0dXJlIGlzIG1vcmUgZmluZWx5IHBlYmJsZWQgdGhhbiAnSGFzcycsIGFuZCBpcyBkdWxsIGdyZWVuIHdoZW4gcmlwZS4gSXQgaXMgZnJvc3QtaGFyZHkgZG93biB0byDiiJIxIMKwQyAoMzAgwrBGKVwiLFxyXG4gICAgICAgIGNhdGVnb3J5OiAnUGl6emEnLFxyXG4gICAgICAgIGhhcmRpbmVzczogJ1BpenphIGRlIGphbcOzbiB5IHBpw7FhIGNvbiBzYWxzYSBwb21vZG9ybyB5IHF1ZXNvIG1venphcmVsbGEuIFZpZW5lIGVuIDQgcG9yY2lvbmVzIGRlIDI3IGNtIGRlIGRpw6FtZXRyby4nLFxyXG4gICAgICAgIHRhc3RlOiAnU3VwZXJiLCBpcyBJdGFsaWFuJyxcclxuICAgICAgfSxcclxuICAgIH0sXHJcbiAgICAnMDA0Jzoge1xyXG4gICAgICBuYW1lOiAnUGl6emEgY2FybmVzJyxcclxuICAgICAgaWQ6ICcwMDQnLFxyXG4gICAgICBwcmljZTogOTUwMCxcclxuICAgICAgaW1hZ2U6ICcvaW1hZ2VzL2JhY29uLmpwZycsXHJcbiAgICAgIGF0dHJpYnV0ZXM6IHtcclxuICAgICAgICBkZXNjcmlwdGlvbjpcclxuICAgICAgICAgICdEZXZlbG9wZWQgYnkgYSBmYXJtZXIsIEphbWVzIEJhY29uLCBpbiAxOTU0LCBCYWNvbiBoYXMgbWVkaXVtLXNpemVkIGZydWl0IHdpdGggc21vb3RoLCBncmVlbiBza2luIHdpdGggeWVsbG93LWdyZWVuLCBsaWdodC10YXN0aW5nIGZsZXNoLiBXaGVuIHJpcGUsIHRoZSBza2luIHJlbWFpbnMgZ3JlZW4sIGJ1dCBkYXJrZW5zIHNsaWdodGx5LCBhbmQgZnJ1aXQgeWllbGRzIHRvIGdlbnRsZSBwcmVzc3VyZS4gSXQgaXMgY29sZC1oYXJkeSBkb3duIHRvIOKIkjUgwrBDICgyMyDCsEYpJyxcclxuICAgICAgICBjYXRlZ29yeTogJ1BpenphJyxcclxuICAgICAgICBoYXJkaW5lc3M6ICdQaXp6YSBkZSBqYW3Ds24geSBwacOxYSBjb24gc2Fsc2EgcG9tb2Rvcm8geSBxdWVzbyBtb3p6YXJlbGxhLiBWaWVuZSBlbiA0IHBvcmNpb25lcyBkZSAyNyBjbSBkZSBkacOhbWV0cm8uJyxcclxuICAgICAgICB0YXN0ZTogJ0NyZWFteSwgaXMgSXRhbGlhbicsXHJcbiAgICAgIH0sXHJcbiAgICB9LFxyXG4gICAgJzAwNSc6IHtcclxuICAgICAgbmFtZTogJ0xhc2FnbmEnLFxyXG4gICAgICBpZDogJzAwNScsXHJcbiAgICAgIHByaWNlOiAxMjAwMCxcclxuICAgICAgaW1hZ2U6ICcvaW1hZ2VzL2hhc3MuanBnJyxcclxuICAgICAgYXR0cmlidXRlczoge1xyXG4gICAgICAgIGRlc2NyaXB0aW9uOlxyXG4gICAgICAgICAgXCJUaGUgJ0hhc3MnIGlzIHRoZSBtb3N0IGNvbW1vbiBjdWx0aXZhciBvZiBhdm9jYWRvLiBJdCBwcm9kdWNlcyBmcnVpdCB5ZWFyLXJvdW5kIGFuZCBhY2NvdW50cyBmb3IgODAlIG9mIGN1bHRpdmF0ZWQgYXZvY2Fkb3MgaW4gdGhlIHdvcmxkLlsyMV1bNTVdIEFsbCAnSGFzcycgdHJlZXMgYXJlIGRlc2NlbmRlZCBmcm9tIGEgc2luZ2xlICdtb3RoZXIgdHJlZScgcmFpc2VkIGJ5IGEgbWFpbCBjYXJyaWVyIG5hbWVkIFJ1ZG9scGggSGFzcywgb2YgTGEgSGFicmEgSGVpZ2h0cywgQ2FsaWZvcm5pYS5bMjBdWzU1XSBIYXNzIHBhdGVudGVkIHRoZSBwcm9kdWN0aXZlIHRyZWUgaW4gMTkzNS4gVGhlICdtb3RoZXIgdHJlZScsIG9mIHVuY2VydGFpbiBzdWJzcGVjaWVzLCBkaWVkIG9mIHJvb3Qgcm90IGFuZCB3YXMgY3V0IGRvd24gaW4gU2VwdGVtYmVyIDIwMDIuWzIxXVs1NV1bNTZdICdIYXNzJyB0cmVlcyBoYXZlIG1lZGl1bS1zaXplZCAoMTUw4oCTMjUwIGcgb3IgNS4z4oCTOC44IG96KSwgb3ZhdGUgZnJ1aXQgd2l0aCBhIGJsYWNrLCBwZWJibGVkIHNraW4uIFRoZSBmbGVzaCBoYXMgYSBudXR0eSwgcmljaCBmbGF2b3Igd2l0aCAxOSUgb2lsLiBBIGh5YnJpZCBHdWF0ZW1hbGFuIHR5cGUgY2FuIHdpdGhzdGFuZCB0ZW1wZXJhdHVyZXMgdG8g4oiSMSDCsEMgKDMwIMKwRilcIixcclxuICAgICAgICBjYXRlZ29yeTogJ1Bhc3RhJyxcclxuICAgICAgICBoYXJkaW5lc3M6ICdQaXp6YSBkZSBqYW3Ds24geSBwacOxYSBjb24gc2Fsc2EgcG9tb2Rvcm8geSBxdWVzbyBtb3p6YXJlbGxhLiBWaWVuZSBlbiA0IHBvcmNpb25lcyBkZSAyNyBjbSBkZSBkacOhbWV0cm8uJyxcclxuICAgICAgICB0YXN0ZTogJ0dvcmdlb3VzLCBpcyBJdGFsaWFuJyxcclxuICAgICAgfSxcclxuICAgIH0sXHJcbiAgICAnMDA2Jzoge1xyXG4gICAgICBuYW1lOiAnUGFzdGEgQm9sb25lc2EnLFxyXG4gICAgICBpZDogJzAwNicsXHJcbiAgICAgIHByaWNlOiAxMDAwMCxcclxuICAgICAgaW1hZ2U6ICcvaW1hZ2VzL2xhbWIuanBnJyxcclxuICAgICAgYXR0cmlidXRlczoge1xyXG4gICAgICAgIGRlc2NyaXB0aW9uOlxyXG4gICAgICAgICAgJ1RoZSBMYW1iIEhhc3MgYXZvY2FkbyBpcyBhIGNyb3NzIGJldHdlZW4gYSBIYXNzIGFuZCBHd2VuIGF2b2NhZG8uIFRoZSBmcnVpdHMgYXJlIGxhcmdlciBpbiBzaXplIGFuZCBsYXRlciBtYXR1cmluZyB0aGFuIEhhc3MuIEl0IGlzIGdhaW5pbmcgaW4gcG9wdWxhcml0eSBhcyBhIGNvbW1lcmNpYWwgYW5kIGJhY2t5YXJkIHZhcmlldHkgZHVlIHRvIGl0cyBleGNlcHRpb25hbCBmbGF2b3IgYW5kIGVhc3kgcGVlbGluZyBxdWFsaXRpZXMuIFRoZSB0cmVlIGhhcyBhbiB1cHJpZ2h0LCBjb21wYWN0IGhhYml0LicsXHJcbiAgICAgICAgY2F0ZWdvcnk6ICdQYXN0YScsXHJcbiAgICAgICAgaGFyZGluZXNzOiAnUGl6emEgZGUgamFtw7NuIHkgcGnDsWEgY29uIHNhbHNhIHBvbW9kb3JvIHkgcXVlc28gbW96emFyZWxsYS4gVmllbmUgZW4gNCBwb3JjaW9uZXMgZGUgMjcgY20gZGUgZGnDoW1ldHJvLicsXHJcbiAgICAgICAgdGFzdGU6ICdHcmVhdCwgaXMgSXRhbGlhbicsXHJcbiAgICAgIH0sXHJcbiAgICB9LFxyXG4gICAgJzAwNyc6IHtcclxuICAgICAgbmFtZTogJ1Bhc3RhIHNhbHNhIGJsYW5jYScsXHJcbiAgICAgIGlkOiAnMDA3JyxcclxuICAgICAgcHJpY2U6IDEwLjAwMCxcclxuICAgICAgaW1hZ2U6ICcvaW1hZ2VzL3BpbmtlcnRvbi5qcGcnLFxyXG4gICAgICBhdHRyaWJ1dGVzOiB7XHJcbiAgICAgICAgZGVzY3JpcHRpb246XHJcbiAgICAgICAgICBcIkZpcnN0IGdyb3duIG9uIHRoZSBQaW5rZXJ0b24gUmFuY2ggaW4gU2F0aWNveSwgQ2FsaWZvcm5pYSwgaW4gdGhlIGVhcmx5IDE5NzBzLCAnUGlua2VydG9uJyBpcyBhIHNlZWRsaW5nIG9mICdIYXNzJyB4ICdSaW5jb24nLiBUaGUgbGFyZ2UgZnJ1aXQgaGFzIGEgc21hbGwgc2VlZCwgYW5kIGl0cyBncmVlbiBza2luIGRlZXBlbnMgaW4gY29sb3IgYXMgaXQgcmlwZW5zLiBUaGUgdGhpY2sgZmxlc2ggaGFzIGEgc21vb3RoLCBjcmVhbXkgdGV4dHVyZSwgcGFsZSBncmVlbiBjb2xvciwgZ29vZCBmbGF2b3IsIGFuZCBoaWdoIG9pbCBjb250ZW50LiBJdCBzaG93cyBzb21lIGNvbGQgdG9sZXJhbmNlLCB0byDiiJIxIMKwQyAoMzAgwrBGKSBhbmQgYmVhcnMgY29uc2lzdGVudGx5IGhlYXZ5IGNyb3BzLiBBIGh5YnJpZCBHdWF0ZW1hbGFuIHR5cGUsIGl0IGhhcyBleGNlbGxlbnQgcGVlbGluZyBjaGFyYWN0ZXJpc3RpY3NcIixcclxuICAgICAgICBjYXRlZ29yeTogJ1Bhc3RhJyxcclxuICAgICAgICBoYXJkaW5lc3M6ICdQaXp6YSBkZSBqYW3Ds24geSBwacOxYSBjb24gc2Fsc2EgcG9tb2Rvcm8geSBxdWVzbyBtb3p6YXJlbGxhLiBWaWVuZSBlbiA0IHBvcmNpb25lcyBkZSAyNyBjbSBkZSBkacOhbWV0cm8uJyxcclxuICAgICAgICB0YXN0ZTogJ01hcnZlbG91cywgaXMgSXRhbGlhbicsXHJcbiAgICAgIH0sXHJcbiAgICB9LFxyXG4gICAgJzAwOCc6IHtcclxuICAgICAgbmFtZTogJ1Bhbnplcm90dGknLFxyXG4gICAgICBpZDogJzAwOCcsXHJcbiAgICAgIHByaWNlOiA5MDAwLFxyXG4gICAgICBpbWFnZTogJy9pbWFnZXMvcmVlZC5qcGcnLFxyXG4gICAgICBhdHRyaWJ1dGVzOiB7XHJcbiAgICAgICAgZGVzY3JpcHRpb246XHJcbiAgICAgICAgICAnRGV2ZWxvcGVkIGZyb20gYSBjaGFuY2Ugc2VlZGxpbmcgZm91bmQgaW4gMTk0OCBieSBKYW1lcyBTLiBSZWVkIGluIENhbGlmb3JuaWEsIHRoaXMgY3VsdGl2YXIgaGFzIGxhcmdlLCByb3VuZCwgZ3JlZW4gZnJ1aXQgd2l0aCBhIHNtb290aCB0ZXh0dXJlIGFuZCBkYXJrLCB0aGljaywgZ2xvc3N5IHNraW4uIFNtb290aCBhbmQgZGVsaWNhdGUsIHRoZSBmbGVzaCBoYXMgYSBzbGlnaHRseSBudXR0eSBmbGF2b3IuIFRoZSBza2luIHJpcGVucyBncmVlbi4gQSBHdWF0ZW1hbGFuIHR5cGUsIGl0IGlzIGhhcmR5IHRvIOKIkjEgwrBDICgzMCDCsEYpLiBUcmVlIHNpemUgaXMgYWJvdXQgNSBieSA0IG0gKDE2LjQgYnkgMTMuMSBmdCknLFxyXG4gICAgICAgIGNhdGVnb3J5OiAnUGFuJyxcclxuICAgICAgICBoYXJkaW5lc3M6ICdQaXp6YSBkZSBqYW3Ds24geSBwacOxYSBjb24gc2Fsc2EgcG9tb2Rvcm8geSBxdWVzbyBtb3p6YXJlbGxhLiBWaWVuZSBlbiA0IHBvcmNpb25lcyBkZSAyNyBjbSBkZSBkacOhbWV0cm8uJyxcclxuICAgICAgICB0YXN0ZTogJ0V4cXVpc2l0ZSwgaXMgSXRhbGlhbicsXHJcbiAgICAgIH0sXHJcbiAgICB9LFxyXG4gICAgJzAwOSc6IHtcclxuICAgICAgbmFtZTogJ1BhbmVRJyxcclxuICAgICAgaWQ6ICcwMDknLFxyXG4gICAgICBwcmljZTogMTIwMDAsXHJcbiAgICAgIGltYWdlOiAnL2ltYWdlcy96dXRhbm8uanBnJyxcclxuICAgICAgYXR0cmlidXRlczoge1xyXG4gICAgICAgIGRlc2NyaXB0aW9uOlxyXG4gICAgICAgICAgJ1RoZSBadXRhbm8gYXZvY2FkbyBpcyBhIGNvbGQgaGFyZHksIGNvbnNpc3RlbnQgcHJvZHVjaW5nIGF2b2NhZG8gdmFyaWV0eS4gSXQgcmVzZW1ibGVzIHRoZSBGdWVydGUgaW4gYXBwZWFyYW5jZSBidXQgaXMgbGVzcyBmbGF2b3JmdWwgYnV0IG1vcmUgY29sZCBoYXJkeS4gVGhlIGdyZWVuIGZydWl0cyBhcmUgb2JvdmF0ZSBpbiBjYXRlZ29yeSB3aXRoIHdheHkgYnVtcHMgb24gdGhlIHNraW4uIFRoZSBmbGVzaCBoYXMgYSBsb3cgb2lsIGJ1dCBoaWdoIHdhdGVyIGNvbnRlbnQgd2hpY2ggY2F1c2VzIGl0IHRvIGhhdmUgYSBtb3JlIGZpYnJvdXMgdGV4dHVyZS4nLFxyXG4gICAgICAgIGNhdGVnb3J5OiAnUGFubicsXHJcbiAgICAgICAgaGFyZGluZXNzOiAnUGl6emEgZGUgamFtw7NuIHkgcGnDsWEgY29uIHNhbHNhIHBvbW9kb3JvIHkgcXVlc28gbW96emFyZWxsYS4gVmllbmUgZW4gNCBwb3JjaW9uZXMgZGUgMjcgY20gZGUgZGnDoW1ldHJvLicsXHJcbiAgICAgICAgdGFzdGU6ICdTcGxlbmRpZCwgaXMgSXRhbGlhbicsXHJcbiAgICAgIH0sXHJcbiAgICB9LFxyXG4gICAgICAnMDEwJzoge1xyXG4gICAgICAgIG5hbWU6ICdQYW5lUScsXHJcbiAgICAgICAgaWQ6ICcwMTAnLFxyXG4gICAgICAgIHByaWNlOiAxMjAwMCxcclxuICAgICAgICBpbWFnZTogJy9pbWFnZXMvenV0YW5vLmpwZycsXHJcbiAgICAgICAgYXR0cmlidXRlczoge1xyXG4gICAgICAgICAgZGVzY3JpcHRpb246XHJcbiAgICAgICAgICAgICdUaGUgWnV0YW5vIGF2b2NhZG8gaXMgYSBjb2xkIGhhcmR5LCBjb25zaXN0ZW50IHByb2R1Y2luZyBhdm9jYWRvIHZhcmlldHkuIEl0IHJlc2VtYmxlcyB0aGUgRnVlcnRlIGluIGFwcGVhcmFuY2UgYnV0IGlzIGxlc3MgZmxhdm9yZnVsIGJ1dCBtb3JlIGNvbGQgaGFyZHkuIFRoZSBncmVlbiBmcnVpdHMgYXJlIG9ib3ZhdGUgaW4gY2F0ZWdvcnkgd2l0aCB3YXh5IGJ1bXBzIG9uIHRoZSBza2luLiBUaGUgZmxlc2ggaGFzIGEgbG93IG9pbCBidXQgaGlnaCB3YXRlciBjb250ZW50IHdoaWNoIGNhdXNlcyBpdCB0byBoYXZlIGEgbW9yZSBmaWJyb3VzIHRleHR1cmUuJyxcclxuICAgICAgICAgIGNhdGVnb3J5OiAnUGFubicsXHJcbiAgICAgICAgICBoYXJkaW5lc3M6ICdQaXp6YSBkZSBqYW3Ds24geSBwacOxYSBjb24gc2Fsc2EgcG9tb2Rvcm8geSBxdWVzbyBtb3p6YXJlbGxhLiBWaWVuZSBlbiA0IHBvcmNpb25lcyBkZSAyNyBjbSBkZSBkacOhbWV0cm8uJyxcclxuICAgICAgICAgIHRhc3RlOiAnU3BsZW5kaWQsIGlzIEl0YWxpYW4nLFxyXG4gICAgICAgIH0sXHJcbiAgICAgIH0sXHJcbiAgICAgICcwMTEnOiB7XHJcbiAgICAgICAgbmFtZTogJ1BhbmVRJyxcclxuICAgICAgICBpZDogJzAxMScsXHJcbiAgICAgICAgcHJpY2U6IDEyMDAwLFxyXG4gICAgICAgIGltYWdlOiAnL2ltYWdlcy96dXRhbm8uanBnJyxcclxuICAgICAgICBhdHRyaWJ1dGVzOiB7XHJcbiAgICAgICAgICBkZXNjcmlwdGlvbjpcclxuICAgICAgICAgICAgJ1RoZSBadXRhbm8gYXZvY2FkbyBpcyBhIGNvbGQgaGFyZHksIGNvbnNpc3RlbnQgcHJvZHVjaW5nIGF2b2NhZG8gdmFyaWV0eS4gSXQgcmVzZW1ibGVzIHRoZSBGdWVydGUgaW4gYXBwZWFyYW5jZSBidXQgaXMgbGVzcyBmbGF2b3JmdWwgYnV0IG1vcmUgY29sZCBoYXJkeS4gVGhlIGdyZWVuIGZydWl0cyBhcmUgb2JvdmF0ZSBpbiBjYXRlZ29yeSB3aXRoIHdheHkgYnVtcHMgb24gdGhlIHNraW4uIFRoZSBmbGVzaCBoYXMgYSBsb3cgb2lsIGJ1dCBoaWdoIHdhdGVyIGNvbnRlbnQgd2hpY2ggY2F1c2VzIGl0IHRvIGhhdmUgYSBtb3JlIGZpYnJvdXMgdGV4dHVyZS4nLFxyXG4gICAgICAgICAgY2F0ZWdvcnk6ICdQYW5uJyxcclxuICAgICAgICAgIGhhcmRpbmVzczogJ1BpenphIGRlIGphbcOzbiB5IHBpw7FhIGNvbiBzYWxzYSBwb21vZG9ybyB5IHF1ZXNvIG1venphcmVsbGEuIFZpZW5lIGVuIDQgcG9yY2lvbmVzIGRlIDI3IGNtIGRlIGRpw6FtZXRyby4nLFxyXG4gICAgICAgICAgdGFzdGU6ICdTcGxlbmRpZCwgaXMgSXRhbGlhbicsXHJcbiAgICAgICAgfSxcclxuICAgICAgfSxcclxuICAgICAgJzAxMic6IHtcclxuICAgICAgICBuYW1lOiAnUGl6emEgSGF3dWFpYW5hJyxcclxuICAgICAgICBpZDogJzAxMicsXHJcbiAgICAgICAgcHJpY2U6IDk1MDAsXHJcbiAgICAgICAgaW1hZ2U6ICcvaW1hZ2VzL21hbHVtYS5qcGcnLFxyXG4gICAgICAgIGF0dHJpYnV0ZXM6IHtcclxuICAgICAgICAgIGRlc2NyaXB0aW9uOlxyXG4gICAgICAgICAgICAnQSByZWxhdGl2ZWx5IG5ldyBjdWx0aXZhciwgaXQgd2FzLCB0aGUgcHJldHR5IGJveSBiYWJ5LCBkaXNjb3ZlcmVkIGluIFNvdXRoIEFmcmljYSBpbiB0aGUgZWFybHkgMTk5MHMgYnkgTXIuIEEuRy4gKERyaWVzKSBKb3ViZXJ0LiBNYWx1bWEgQmFieXkuIEl0IGlzIGEgY2hhbmNlIHNlZWRsaW5nIG9mIHVua25vd24gcGFyZW50YWdlJyxcclxuICAgICAgICAgIGNhdGVnb3J5OiAnUGl6emEnLFxyXG4gICAgICAgICAgaGFyZGluZXNzOiAnUGl6emEgZGUgamFtw7NuIHkgcGnDsWEgY29uIHNhbHNhIHBvbW9kb3JvIHkgcXVlc28gbW96emFyZWxsYS4gVmllbmUgZW4gNCBwb3JjaW9uZXMgZGUgMjcgY20gZGUgZGnDoW1ldHJvLicsXHJcbiAgICAgICAgICB0YXN0ZTogJ0NhdGNoeSwgaXMgSXRhbGlhbicsXHJcbiAgICAgICAgfSxcclxuICAgICAgfSxcclxuICAgICAgJzAxMyc6IHtcclxuICAgICAgICBuYW1lOiAnUGl6emEgUG9sbG8gQkJRJyxcclxuICAgICAgICBpZDogJzAxMycsXHJcbiAgICAgICAgcHJpY2U6IDkuNTAwLFxyXG4gICAgICAgIGltYWdlOiAnL2ltYWdlcy9mdWVydGUuanBnJyxcclxuICAgICAgICBhdHRyaWJ1dGVzOiB7XHJcbiAgICAgICAgICBkZXNjcmlwdGlvbjpcclxuICAgICAgICAgICAgJ1RoZSBGdWVydGUgYXZvY2FkbyBpcyB0aGUgc2Vjb25kIGxhcmdlc3QgY29tbWVyY2lhbCB2YXJpZXR5IGJlaGluZCBIYXNzLiBJdCBpcyBhIGxvbmcgdGltZSBDYWxpZm9ybmlhIGNvbW1lcmNpYWwgdmFyaWV0eSB2YWx1ZWQgZm9yIGl0cyB3aW50ZXIgc2Vhc29uIHJpcGVuaW5nIGFuZCBpdHMgQi1UeXBlIGJsb3Nzb20gdHlwZSB3aGljaCBtb3N0IGdyb3dlcnMgcGxhbnQgYWRqYWNlbnQgdG8gdGhlIEhhc3MgZm9yIGEgbW9yZSBjb25zaXN0ZW50IHByb2R1Y3Rpb24gY3ljbGUuIFRoaXMgYXZvY2FkbyB0ZW5kcyB0byBwcm9kdWNlIGhlYXZpbHkgaW4gYWx0ZXJuYXRlIHllYXJzLicsXHJcbiAgICAgICAgICBjYXRlZ29yeTogJ1BpenphJyxcclxuICAgICAgICAgIGhhcmRpbmVzczogJ1BpenphIGRlIGphbcOzbiB5IHBpw7FhIGNvbiBzYWxzYSBwb21vZG9ybyB5IHF1ZXNvIG1venphcmVsbGEuIFZpZW5lIGVuIDQgcG9yY2lvbmVzIGRlIDI3IGNtIGRlIGRpw6FtZXRyby4nLFxyXG4gICAgICAgICAgdGFzdGU6ICdNYWduaWZpY2VudCwgaXMgSXRhbGlhbicsXHJcbiAgICAgICAgfSxcclxuICAgICAgfSxcclxuICAgICAgJzAxNCc6IHtcclxuICAgICAgICBuYW1lOiAnUGl6emEgUG9sbG8gQkJRJyxcclxuICAgICAgICBpZDogJzAxNCcsXHJcbiAgICAgICAgcHJpY2U6IDkuNTAwLFxyXG4gICAgICAgIGltYWdlOiAnL2ltYWdlcy9mdWVydGUuanBnJyxcclxuICAgICAgICBhdHRyaWJ1dGVzOiB7XHJcbiAgICAgICAgICBkZXNjcmlwdGlvbjpcclxuICAgICAgICAgICAgJ1RoZSBGdWVydGUgYXZvY2FkbyBpcyB0aGUgc2Vjb25kIGxhcmdlc3QgY29tbWVyY2lhbCB2YXJpZXR5IGJlaGluZCBIYXNzLiBJdCBpcyBhIGxvbmcgdGltZSBDYWxpZm9ybmlhIGNvbW1lcmNpYWwgdmFyaWV0eSB2YWx1ZWQgZm9yIGl0cyB3aW50ZXIgc2Vhc29uIHJpcGVuaW5nIGFuZCBpdHMgQi1UeXBlIGJsb3Nzb20gdHlwZSB3aGljaCBtb3N0IGdyb3dlcnMgcGxhbnQgYWRqYWNlbnQgdG8gdGhlIEhhc3MgZm9yIGEgbW9yZSBjb25zaXN0ZW50IHByb2R1Y3Rpb24gY3ljbGUuIFRoaXMgYXZvY2FkbyB0ZW5kcyB0byBwcm9kdWNlIGhlYXZpbHkgaW4gYWx0ZXJuYXRlIHllYXJzLicsXHJcbiAgICAgICAgICBjYXRlZ29yeTogJ1BpenphJyxcclxuICAgICAgICAgIGhhcmRpbmVzczogJ1BpenphIGRlIGphbcOzbiB5IHBpw7FhIGNvbiBzYWxzYSBwb21vZG9ybyB5IHF1ZXNvIG1venphcmVsbGEuIFZpZW5lIGVuIDQgcG9yY2lvbmVzIGRlIDI3IGNtIGRlIGRpw6FtZXRyby4nLFxyXG4gICAgICAgICAgdGFzdGU6ICdNYWduaWZpY2VudCwgaXMgSXRhbGlhbicsXHJcbiAgICAgICAgfSxcclxuICAgICAgfSxcclxuICAgICAgJzAxNSc6IHtcclxuICAgICAgICBuYW1lOiAnUGl6emEgbWV4aWNhbmEnLFxyXG4gICAgICAgIGlkOiAnMDE1JyxcclxuICAgICAgICBwcmljZTogMTI1LFxyXG4gICAgICAgIGltYWdlOiAnL2ltYWdlcy9nd2VuLmpwZycsXHJcbiAgICAgICAgYXR0cmlidXRlczoge1xyXG4gICAgICAgICAgZGVzY3JpcHRpb246XHJcbiAgICAgICAgICAgIFwiQSBzZWVkbGluZyBicmVkIGZyb20gJ0hhc3MnIHggJ1RoaWxsZScgaW4gMTk4MiwgJ0d3ZW4nIGlzIGhpZ2hlciB5aWVsZGluZyBhbmQgbW9yZSBkd2FyZmluZyB0aGFuICdIYXNzJyBpbiBDYWxpZm9ybmlhLiBUaGUgZnJ1aXQgaGFzIGFuIG92YWwgY2F0ZWdvcnksIHNsaWdodGx5IHNtYWxsZXIgdGhhbiAnSGFzcycgKDEwMOKAkzIwMCBnIG9yIDMuNeKAkzcuMSBveiksIHdpdGggYSByaWNoLCBudXR0eSBmbGF2b3IuIFRoZSBza2luIHRleHR1cmUgaXMgbW9yZSBmaW5lbHkgcGViYmxlZCB0aGFuICdIYXNzJywgYW5kIGlzIGR1bGwgZ3JlZW4gd2hlbiByaXBlLiBJdCBpcyBmcm9zdC1oYXJkeSBkb3duIHRvIOKIkjEgwrBDICgzMCDCsEYpXCIsXHJcbiAgICAgICAgICBjYXRlZ29yeTogJ1BpenphJyxcclxuICAgICAgICAgIGhhcmRpbmVzczogJ1BpenphIGRlIGphbcOzbiB5IHBpw7FhIGNvbiBzYWxzYSBwb21vZG9ybyB5IHF1ZXNvIG1venphcmVsbGEuIFZpZW5lIGVuIDQgcG9yY2lvbmVzIGRlIDI3IGNtIGRlIGRpw6FtZXRyby4nLFxyXG4gICAgICAgICAgdGFzdGU6ICdTdXBlcmIsIGlzIEl0YWxpYW4nLFxyXG4gICAgICAgIH0sXHJcbiAgICAgIH0sXHJcbiAgICAgICcwMTYnOiB7XHJcbiAgICAgICAgbmFtZTogJ1BpenphIGNhcm5lcycsXHJcbiAgICAgICAgaWQ6ICcwMTYnLFxyXG4gICAgICAgIHByaWNlOiA5NTAwLFxyXG4gICAgICAgIGltYWdlOiAnL2ltYWdlcy9iYWNvbi5qcGcnLFxyXG4gICAgICAgIGF0dHJpYnV0ZXM6IHtcclxuICAgICAgICAgIGRlc2NyaXB0aW9uOlxyXG4gICAgICAgICAgICAnRGV2ZWxvcGVkIGJ5IGEgZmFybWVyLCBKYW1lcyBCYWNvbiwgaW4gMTk1NCwgQmFjb24gaGFzIG1lZGl1bS1zaXplZCBmcnVpdCB3aXRoIHNtb290aCwgZ3JlZW4gc2tpbiB3aXRoIHllbGxvdy1ncmVlbiwgbGlnaHQtdGFzdGluZyBmbGVzaC4gV2hlbiByaXBlLCB0aGUgc2tpbiByZW1haW5zIGdyZWVuLCBidXQgZGFya2VucyBzbGlnaHRseSwgYW5kIGZydWl0IHlpZWxkcyB0byBnZW50bGUgcHJlc3N1cmUuIEl0IGlzIGNvbGQtaGFyZHkgZG93biB0byDiiJI1IMKwQyAoMjMgwrBGKScsXHJcbiAgICAgICAgICBjYXRlZ29yeTogJ1BpenphJyxcclxuICAgICAgICAgIGhhcmRpbmVzczogJ1BpenphIGRlIGphbcOzbiB5IHBpw7FhIGNvbiBzYWxzYSBwb21vZG9ybyB5IHF1ZXNvIG1venphcmVsbGEuIFZpZW5lIGVuIDQgcG9yY2lvbmVzIGRlIDI3IGNtIGRlIGRpw6FtZXRyby4nLFxyXG4gICAgICAgICAgdGFzdGU6ICdDcmVhbXksIGlzIEl0YWxpYW4nLFxyXG4gICAgICAgIH0sXHJcbiAgICAgIH0sXHJcbiAgICAgICcwMTcnOiB7XHJcbiAgICAgICAgbmFtZTogJ0xhc2FnbmEnLFxyXG4gICAgICAgIGlkOiAnMDE3JyxcclxuICAgICAgICBwcmljZTogMTIwMDAsXHJcbiAgICAgICAgaW1hZ2U6ICcvaW1hZ2VzL2hhc3MuanBnJyxcclxuICAgICAgICBhdHRyaWJ1dGVzOiB7XHJcbiAgICAgICAgICBkZXNjcmlwdGlvbjpcclxuICAgICAgICAgICAgXCJUaGUgJ0hhc3MnIGlzIHRoZSBtb3N0IGNvbW1vbiBjdWx0aXZhciBvZiBhdm9jYWRvLiBJdCBwcm9kdWNlcyBmcnVpdCB5ZWFyLXJvdW5kIGFuZCBhY2NvdW50cyBmb3IgODAlIG9mIGN1bHRpdmF0ZWQgYXZvY2Fkb3MgaW4gdGhlIHdvcmxkLlsyMV1bNTVdIEFsbCAnSGFzcycgdHJlZXMgYXJlIGRlc2NlbmRlZCBmcm9tIGEgc2luZ2xlICdtb3RoZXIgdHJlZScgcmFpc2VkIGJ5IGEgbWFpbCBjYXJyaWVyIG5hbWVkIFJ1ZG9scGggSGFzcywgb2YgTGEgSGFicmEgSGVpZ2h0cywgQ2FsaWZvcm5pYS5bMjBdWzU1XSBIYXNzIHBhdGVudGVkIHRoZSBwcm9kdWN0aXZlIHRyZWUgaW4gMTkzNS4gVGhlICdtb3RoZXIgdHJlZScsIG9mIHVuY2VydGFpbiBzdWJzcGVjaWVzLCBkaWVkIG9mIHJvb3Qgcm90IGFuZCB3YXMgY3V0IGRvd24gaW4gU2VwdGVtYmVyIDIwMDIuWzIxXVs1NV1bNTZdICdIYXNzJyB0cmVlcyBoYXZlIG1lZGl1bS1zaXplZCAoMTUw4oCTMjUwIGcgb3IgNS4z4oCTOC44IG96KSwgb3ZhdGUgZnJ1aXQgd2l0aCBhIGJsYWNrLCBwZWJibGVkIHNraW4uIFRoZSBmbGVzaCBoYXMgYSBudXR0eSwgcmljaCBmbGF2b3Igd2l0aCAxOSUgb2lsLiBBIGh5YnJpZCBHdWF0ZW1hbGFuIHR5cGUgY2FuIHdpdGhzdGFuZCB0ZW1wZXJhdHVyZXMgdG8g4oiSMSDCsEMgKDMwIMKwRilcIixcclxuICAgICAgICAgIGNhdGVnb3J5OiAnUGFzdGEnLFxyXG4gICAgICAgICAgaGFyZGluZXNzOiAnUGl6emEgZGUgamFtw7NuIHkgcGnDsWEgY29uIHNhbHNhIHBvbW9kb3JvIHkgcXVlc28gbW96emFyZWxsYS4gVmllbmUgZW4gNCBwb3JjaW9uZXMgZGUgMjcgY20gZGUgZGnDoW1ldHJvLicsXHJcbiAgICAgICAgICB0YXN0ZTogJ0dvcmdlb3VzLCBpcyBJdGFsaWFuJyxcclxuICAgICAgICB9LFxyXG4gICAgICB9LFxyXG4gICAgICAnMDE4Jzoge1xyXG4gICAgICAgIG5hbWU6ICdQYXN0YSBCb2xvbmVzYScsXHJcbiAgICAgICAgaWQ6ICcwMTgnLFxyXG4gICAgICAgIHByaWNlOiAxMDAwMCxcclxuICAgICAgICBpbWFnZTogJy9pbWFnZXMvbGFtYi5qcGcnLFxyXG4gICAgICAgIGF0dHJpYnV0ZXM6IHtcclxuICAgICAgICAgIGRlc2NyaXB0aW9uOlxyXG4gICAgICAgICAgICAnVGhlIExhbWIgSGFzcyBhdm9jYWRvIGlzIGEgY3Jvc3MgYmV0d2VlbiBhIEhhc3MgYW5kIEd3ZW4gYXZvY2Fkby4gVGhlIGZydWl0cyBhcmUgbGFyZ2VyIGluIHNpemUgYW5kIGxhdGVyIG1hdHVyaW5nIHRoYW4gSGFzcy4gSXQgaXMgZ2FpbmluZyBpbiBwb3B1bGFyaXR5IGFzIGEgY29tbWVyY2lhbCBhbmQgYmFja3lhcmQgdmFyaWV0eSBkdWUgdG8gaXRzIGV4Y2VwdGlvbmFsIGZsYXZvciBhbmQgZWFzeSBwZWVsaW5nIHF1YWxpdGllcy4gVGhlIHRyZWUgaGFzIGFuIHVwcmlnaHQsIGNvbXBhY3QgaGFiaXQuJyxcclxuICAgICAgICAgIGNhdGVnb3J5OiAnUGFzdGEnLFxyXG4gICAgICAgICAgaGFyZGluZXNzOiAnUGl6emEgZGUgamFtw7NuIHkgcGnDsWEgY29uIHNhbHNhIHBvbW9kb3JvIHkgcXVlc28gbW96emFyZWxsYS4gVmllbmUgZW4gNCBwb3JjaW9uZXMgZGUgMjcgY20gZGUgZGnDoW1ldHJvLicsXHJcbiAgICAgICAgICB0YXN0ZTogJ0dyZWF0LCBpcyBJdGFsaWFuJyxcclxuICAgICAgICB9LFxyXG4gICAgICB9LFxyXG4gICAgICAnMDE5Jzoge1xyXG4gICAgICAgIG5hbWU6ICdQYXN0YSBzYWxzYSBibGFuY2EnLFxyXG4gICAgICAgIGlkOiAnMDE5JyxcclxuICAgICAgICBwcmljZTogMTAuMDAwLFxyXG4gICAgICAgIGltYWdlOiAnL2ltYWdlcy9waW5rZXJ0b24uanBnJyxcclxuICAgICAgICBhdHRyaWJ1dGVzOiB7XHJcbiAgICAgICAgICBkZXNjcmlwdGlvbjpcclxuICAgICAgICAgICAgXCJGaXJzdCBncm93biBvbiB0aGUgUGlua2VydG9uIFJhbmNoIGluIFNhdGljb3ksIENhbGlmb3JuaWEsIGluIHRoZSBlYXJseSAxOTcwcywgJ1BpbmtlcnRvbicgaXMgYSBzZWVkbGluZyBvZiAnSGFzcycgeCAnUmluY29uJy4gVGhlIGxhcmdlIGZydWl0IGhhcyBhIHNtYWxsIHNlZWQsIGFuZCBpdHMgZ3JlZW4gc2tpbiBkZWVwZW5zIGluIGNvbG9yIGFzIGl0IHJpcGVucy4gVGhlIHRoaWNrIGZsZXNoIGhhcyBhIHNtb290aCwgY3JlYW15IHRleHR1cmUsIHBhbGUgZ3JlZW4gY29sb3IsIGdvb2QgZmxhdm9yLCBhbmQgaGlnaCBvaWwgY29udGVudC4gSXQgc2hvd3Mgc29tZSBjb2xkIHRvbGVyYW5jZSwgdG8g4oiSMSDCsEMgKDMwIMKwRikgYW5kIGJlYXJzIGNvbnNpc3RlbnRseSBoZWF2eSBjcm9wcy4gQSBoeWJyaWQgR3VhdGVtYWxhbiB0eXBlLCBpdCBoYXMgZXhjZWxsZW50IHBlZWxpbmcgY2hhcmFjdGVyaXN0aWNzXCIsXHJcbiAgICAgICAgICBjYXRlZ29yeTogJ1Bhc3RhJyxcclxuICAgICAgICAgIGhhcmRpbmVzczogJ1BpenphIGRlIGphbcOzbiB5IHBpw7FhIGNvbiBzYWxzYSBwb21vZG9ybyB5IHF1ZXNvIG1venphcmVsbGEuIFZpZW5lIGVuIDQgcG9yY2lvbmVzIGRlIDI3IGNtIGRlIGRpw6FtZXRyby4nLFxyXG4gICAgICAgICAgdGFzdGU6ICdNYXJ2ZWxvdXMsIGlzIEl0YWxpYW4nLFxyXG4gICAgICAgIH0sXHJcbiAgICAgIH0sXHJcbiAgICAgICcwMjAnOiB7XHJcbiAgICAgICAgbmFtZTogJ1Bhbnplcm90dGknLFxyXG4gICAgICAgIGlkOiAnMDIwJyxcclxuICAgICAgICBwcmljZTogOTAwMCxcclxuICAgICAgICBpbWFnZTogJy9pbWFnZXMvcmVlZC5qcGcnLFxyXG4gICAgICAgIGF0dHJpYnV0ZXM6IHtcclxuICAgICAgICAgIGRlc2NyaXB0aW9uOlxyXG4gICAgICAgICAgICAnRGV2ZWxvcGVkIGZyb20gYSBjaGFuY2Ugc2VlZGxpbmcgZm91bmQgaW4gMTk0OCBieSBKYW1lcyBTLiBSZWVkIGluIENhbGlmb3JuaWEsIHRoaXMgY3VsdGl2YXIgaGFzIGxhcmdlLCByb3VuZCwgZ3JlZW4gZnJ1aXQgd2l0aCBhIHNtb290aCB0ZXh0dXJlIGFuZCBkYXJrLCB0aGljaywgZ2xvc3N5IHNraW4uIFNtb290aCBhbmQgZGVsaWNhdGUsIHRoZSBmbGVzaCBoYXMgYSBzbGlnaHRseSBudXR0eSBmbGF2b3IuIFRoZSBza2luIHJpcGVucyBncmVlbi4gQSBHdWF0ZW1hbGFuIHR5cGUsIGl0IGlzIGhhcmR5IHRvIOKIkjEgwrBDICgzMCDCsEYpLiBUcmVlIHNpemUgaXMgYWJvdXQgNSBieSA0IG0gKDE2LjQgYnkgMTMuMSBmdCknLFxyXG4gICAgICAgICAgY2F0ZWdvcnk6ICdQYW4nLFxyXG4gICAgICAgICAgaGFyZGluZXNzOiAnUGl6emEgZGUgamFtw7NuIHkgcGnDsWEgY29uIHNhbHNhIHBvbW9kb3JvIHkgcXVlc28gbW96emFyZWxsYS4gVmllbmUgZW4gNCBwb3JjaW9uZXMgZGUgMjcgY20gZGUgZGnDoW1ldHJvLicsXHJcbiAgICAgICAgICB0YXN0ZTogJ0V4cXVpc2l0ZSwgaXMgSXRhbGlhbicsXHJcbiAgICAgICAgfSxcclxuICAgICAgfSxcclxuICAgICAgJzAyMSc6IHtcclxuICAgICAgICBuYW1lOiAnUGFuZVEnLFxyXG4gICAgICAgIGlkOiAnMDIxJyxcclxuICAgICAgICBwcmljZTogMTIwMDAsXHJcbiAgICAgICAgaW1hZ2U6ICcvaW1hZ2VzL3p1dGFuby5qcGcnLFxyXG4gICAgICAgIGF0dHJpYnV0ZXM6IHtcclxuICAgICAgICAgIGRlc2NyaXB0aW9uOlxyXG4gICAgICAgICAgICAnVGhlIFp1dGFubyBhdm9jYWRvIGlzIGEgY29sZCBoYXJkeSwgY29uc2lzdGVudCBwcm9kdWNpbmcgYXZvY2FkbyB2YXJpZXR5LiBJdCByZXNlbWJsZXMgdGhlIEZ1ZXJ0ZSBpbiBhcHBlYXJhbmNlIGJ1dCBpcyBsZXNzIGZsYXZvcmZ1bCBidXQgbW9yZSBjb2xkIGhhcmR5LiBUaGUgZ3JlZW4gZnJ1aXRzIGFyZSBvYm92YXRlIGluIGNhdGVnb3J5IHdpdGggd2F4eSBidW1wcyBvbiB0aGUgc2tpbi4gVGhlIGZsZXNoIGhhcyBhIGxvdyBvaWwgYnV0IGhpZ2ggd2F0ZXIgY29udGVudCB3aGljaCBjYXVzZXMgaXQgdG8gaGF2ZSBhIG1vcmUgZmlicm91cyB0ZXh0dXJlLicsXHJcbiAgICAgICAgICBjYXRlZ29yeTogJ1Bhbm4nLFxyXG4gICAgICAgICAgaGFyZGluZXNzOiAnUGl6emEgZGUgamFtw7NuIHkgcGnDsWEgY29uIHNhbHNhIHBvbW9kb3JvIHkgcXVlc28gbW96emFyZWxsYS4gVmllbmUgZW4gNCBwb3JjaW9uZXMgZGUgMjcgY20gZGUgZGnDoW1ldHJvLicsXHJcbiAgICAgICAgICB0YXN0ZTogJ1NwbGVuZGlkLCBpcyBJdGFsaWFuJyxcclxuICAgICAgICB9LFxyXG4gICAgICB9LFxyXG4gICAgICAgICcwMjInOiB7XHJcbiAgICAgICAgICBuYW1lOiAnUGFuZVEnLFxyXG4gICAgICAgICAgaWQ6ICcwMjInLFxyXG4gICAgICAgICAgcHJpY2U6IDEyMDAwLFxyXG4gICAgICAgICAgaW1hZ2U6ICcvaW1hZ2VzL3p1dGFuby5qcGcnLFxyXG4gICAgICAgICAgYXR0cmlidXRlczoge1xyXG4gICAgICAgICAgICBkZXNjcmlwdGlvbjpcclxuICAgICAgICAgICAgICAnVGhlIFp1dGFubyBhdm9jYWRvIGlzIGEgY29sZCBoYXJkeSwgY29uc2lzdGVudCBwcm9kdWNpbmcgYXZvY2FkbyB2YXJpZXR5LiBJdCByZXNlbWJsZXMgdGhlIEZ1ZXJ0ZSBpbiBhcHBlYXJhbmNlIGJ1dCBpcyBsZXNzIGZsYXZvcmZ1bCBidXQgbW9yZSBjb2xkIGhhcmR5LiBUaGUgZ3JlZW4gZnJ1aXRzIGFyZSBvYm92YXRlIGluIGNhdGVnb3J5IHdpdGggd2F4eSBidW1wcyBvbiB0aGUgc2tpbi4gVGhlIGZsZXNoIGhhcyBhIGxvdyBvaWwgYnV0IGhpZ2ggd2F0ZXIgY29udGVudCB3aGljaCBjYXVzZXMgaXQgdG8gaGF2ZSBhIG1vcmUgZmlicm91cyB0ZXh0dXJlLicsXHJcbiAgICAgICAgICAgIGNhdGVnb3J5OiAnUGFubicsXHJcbiAgICAgICAgICAgIGhhcmRpbmVzczogJ1BpenphIGRlIGphbcOzbiB5IHBpw7FhIGNvbiBzYWxzYSBwb21vZG9ybyB5IHF1ZXNvIG1venphcmVsbGEuIFZpZW5lIGVuIDQgcG9yY2lvbmVzIGRlIDI3IGNtIGRlIGRpw6FtZXRyby4nLFxyXG4gICAgICAgICAgICB0YXN0ZTogJ1NwbGVuZGlkLCBpcyBJdGFsaWFuJyxcclxuICAgICAgICAgIH0sXHJcbiAgICAgICAgfSxcclxuICAgICAgICAnMDIzJzoge1xyXG4gICAgICAgICAgbmFtZTogJ1BhbmVRJyxcclxuICAgICAgICAgIGlkOiAnMDIzJyxcclxuICAgICAgICAgIHByaWNlOiAxMjAwMCxcclxuICAgICAgICAgIGltYWdlOiAnL2ltYWdlcy96dXRhbm8uanBnJyxcclxuICAgICAgICAgIGF0dHJpYnV0ZXM6IHtcclxuICAgICAgICAgICAgZGVzY3JpcHRpb246XHJcbiAgICAgICAgICAgICAgJ1RoZSBadXRhbm8gYXZvY2FkbyBpcyBhIGNvbGQgaGFyZHksIGNvbnNpc3RlbnQgcHJvZHVjaW5nIGF2b2NhZG8gdmFyaWV0eS4gSXQgcmVzZW1ibGVzIHRoZSBGdWVydGUgaW4gYXBwZWFyYW5jZSBidXQgaXMgbGVzcyBmbGF2b3JmdWwgYnV0IG1vcmUgY29sZCBoYXJkeS4gVGhlIGdyZWVuIGZydWl0cyBhcmUgb2JvdmF0ZSBpbiBjYXRlZ29yeSB3aXRoIHdheHkgYnVtcHMgb24gdGhlIHNraW4uIFRoZSBmbGVzaCBoYXMgYSBsb3cgb2lsIGJ1dCBoaWdoIHdhdGVyIGNvbnRlbnQgd2hpY2ggY2F1c2VzIGl0IHRvIGhhdmUgYSBtb3JlIGZpYnJvdXMgdGV4dHVyZS4nLFxyXG4gICAgICAgICAgICBjYXRlZ29yeTogJ1Bhbm4nLFxyXG4gICAgICAgICAgICBoYXJkaW5lc3M6ICdQaXp6YSBkZSBqYW3Ds24geSBwacOxYSBjb24gc2Fsc2EgcG9tb2Rvcm8geSBxdWVzbyBtb3p6YXJlbGxhLiBWaWVuZSBlbiA0IHBvcmNpb25lcyBkZSAyNyBjbSBkZSBkacOhbWV0cm8uJyxcclxuICAgICAgICAgICAgdGFzdGU6ICdTcGxlbmRpZCwgaXMgSXRhbGlhbicsXHJcbiAgICAgICAgICB9LFxyXG4gICAgICAgIFxyXG4gICAgICBcclxuICAgICAgXHJcbiAgICBcclxuICAgICAgXHJcbiAgICAgIFxyXG4gICAgICBcclxuICAgIH0sXHJcbiAgfVxyXG4gIFxyXG4gIGV4cG9ydCBkZWZhdWx0IGRhdGEiLCIvLyBPaCB5b3UgY3VyaW91cy4uLlxyXG4vLyBUaGlzIGlzIG5vdCBhIHJlYWwgZGF0YWJhc2UsXHJcbi8vIEJ1dCBsZXQncyBpbWFnaW5lIGl0IGlzIG9uZSA6KVxyXG5pbXBvcnQgYWxsRGF0YSBmcm9tICcuL2RhdGEnXHJcblxyXG5jbGFzcyBEYXRhYmFzZSB7XHJcbiAgY29uc3RydWN0b3IoKSB7fVxyXG5cclxuICBhc3luYyBnZXRBbGwoKXtcclxuICAgIGNvbnN0IGFzQXJyYXkgPSBPYmplY3QudmFsdWVzKGFsbERhdGEpXHJcbiAgICBhd2FpdCByYW5kb21EZWxheSgpXHJcbiAgICByZXR1cm4gYXNBcnJheVxyXG4gIH1cclxuXHJcbiAgYXN5bmMgZ2V0QnlJZChpZCkge1xyXG4gICAgaWYgKCFPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwoYWxsRGF0YSwgaWQpKSB7XHJcbiAgICAgIHJldHVybiBudWxsXHJcbiAgICB9XHJcblxyXG4gICAgY29uc3QgZW50cnkgPSBhbGxEYXRhW2lkXVxyXG4gICAgYXdhaXQgcmFuZG9tRGVsYXkoKVxyXG4gICAgcmV0dXJuIGVudHJ5XHJcbiAgfVxyXG59XHJcblxyXG4vLyBMZXQncyBhbHNvIGFkZCBhIGRlbGF5IHRvIG1ha2UgaXQgYSBiaXQgY2xvc2VyIHRvIHJlYWxpdHlcclxuY29uc3QgcmFuZG9tRGVsYXkgPSAoKSA9PlxyXG4gIG5ldyBQcm9taXNlKChyZXNvbHZlKSA9PiB7XHJcbiAgICBjb25zdCBtYXggPSAzNTBcclxuICAgIGNvbnN0IG1pbiA9IDEwMFxyXG4gICAgY29uc3QgZGVsYXkgPSBNYXRoLmZsb29yKE1hdGgucmFuZG9tKCkgKiAobWF4IC0gbWluICsgMSkpICsgbWluXHJcblxyXG4gICAgc2V0VGltZW91dChyZXNvbHZlLCBkZWxheSlcclxuICB9KVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgRGF0YWJhc2UiLCJpbXBvcnQgREIgZnJvbSAnQGRhdGFiYXNlJ1xyXG5cclxuXHJcbmNvbnN0IGFsbHByb2R1Y3RzID0gYXN5bmMgKHJlcSwgcmVzKSA9PiB7XHJcbiAgdHJ5IHtcclxuXHJcbiAgICBjb25zdCBkYiA9IG5ldyBEQigpXHJcbiAgICBjb25zdCBhbGxFbnRyaWVzID0gYXdhaXQgZGIuZ2V0QWxsKClcclxuICAgIGNvbnN0IGxlbmdodCA9IGFsbEVudHJpZXMubGVuZ3RoXHJcblxyXG4gICAgLy8gTm90aWNlOiBXZSdyZSBtYW51YWxseSBzZXR0aW5nIHRoZSByZXNwb25zZSBvYmplY3RcclxuICAgIC8vIEhvd2V2ZXIgTmV4dC5KUyBvZmZlcnMgRXhwcmVzcy1saWtlIGhlbHBlcnMgOilcclxuICAgIC8vIGh0dHBzOi8vbmV4dGpzLm9yZy9kb2NzL2FwaS1yb3V0ZXMvcmVzcG9uc2UtaGVscGVyc1xyXG4gICAgcmVzLnN0YXR1c0NvZGUgPSAyMDBcclxuICAgIHJlcy5zZXRIZWFkZXIoJ0NvbnRlbnQtVHlwZScsICdhcHBsaWNhdGlvbi9qc29uJylcclxuICAgIHJlcy5lbmQoSlNPTi5zdHJpbmdpZnkoeyBsZW5naHQsIGRhdGE6IGFsbEVudHJpZXMgfSkpXHJcbiAgfSBjYXRjaCAoZSkge1xyXG4gICAgY29uc29sZS5lcnJvcihlKVxyXG4gICAgcmVzLnN0YXR1c0NvZGUgPSA1MDBcclxuICAgIHJlcy5lbmQoXHJcbiAgICAgIEpTT04uc3RyaW5naWZ5KHsgbGVuZ3RoOiAwLCBkYXRhOiBbXSwgZXJyb3I6ICdTb21ldGhpbmcgd2VudCB3cm9uZycgfSlcclxuICAgIClcclxuICB9XHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IGFsbHByb2R1Y3RzIl0sIm5hbWVzIjpbImRhdGEiLCJuYW1lIiwiaWQiLCJwcmljZSIsImltYWdlIiwiYXR0cmlidXRlcyIsImRlc2NyaXB0aW9uIiwiY2F0ZWdvcnkiLCJoYXJkaW5lc3MiLCJ0YXN0ZSIsImFsbERhdGEiLCJEYXRhYmFzZSIsImNvbnN0cnVjdG9yIiwiZ2V0QWxsIiwiYXNBcnJheSIsIk9iamVjdCIsInZhbHVlcyIsInJhbmRvbURlbGF5IiwiZ2V0QnlJZCIsInByb3RvdHlwZSIsImhhc093blByb3BlcnR5IiwiY2FsbCIsImVudHJ5IiwiUHJvbWlzZSIsInJlc29sdmUiLCJtYXgiLCJtaW4iLCJkZWxheSIsIk1hdGgiLCJmbG9vciIsInJhbmRvbSIsInNldFRpbWVvdXQiLCJEQiIsImFsbHByb2R1Y3RzIiwicmVxIiwicmVzIiwiZGIiLCJhbGxFbnRyaWVzIiwibGVuZ2h0IiwibGVuZ3RoIiwic3RhdHVzQ29kZSIsInNldEhlYWRlciIsImVuZCIsIkpTT04iLCJzdHJpbmdpZnkiLCJlIiwiY29uc29sZSIsImVycm9yIl0sInNvdXJjZVJvb3QiOiIifQ==