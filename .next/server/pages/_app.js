(() => {
var exports = {};
exports.id = "pages/_app";
exports.ids = ["pages/_app"];
exports.modules = {

/***/ "./node_modules/next/app.js":
/*!**********************************!*\
  !*** ./node_modules/next/app.js ***!
  \**********************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

module.exports = __webpack_require__(/*! ./dist/pages/_app */ "./node_modules/next/dist/pages/_app.js")


/***/ }),

/***/ "./node_modules/next/dist/pages/_app.js":
/*!**********************************************!*\
  !*** ./node_modules/next/dist/pages/_app.js ***!
  \**********************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

"use strict";


Object.defineProperty(exports, "__esModule", ({
  value: true
}));
Object.defineProperty(exports, "AppInitialProps", ({
  enumerable: true,
  get: function () {
    return _utils.AppInitialProps;
  }
}));
Object.defineProperty(exports, "NextWebVitalsMetric", ({
  enumerable: true,
  get: function () {
    return _utils.NextWebVitalsMetric;
  }
}));
exports.default = void 0;

var _react = _interopRequireDefault(__webpack_require__(/*! react */ "react"));

var _utils = __webpack_require__(/*! ../shared/lib/utils */ "../shared/lib/utils");

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) {
  try {
    var info = gen[key](arg);
    var value = info.value;
  } catch (error) {
    reject(error);
    return;
  }

  if (info.done) {
    resolve(value);
  } else {
    Promise.resolve(value).then(_next, _throw);
  }
}

function _asyncToGenerator(fn) {
  return function () {
    var self = this,
        args = arguments;
    return new Promise(function (resolve, reject) {
      var gen = fn.apply(self, args);

      function _next(value) {
        asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value);
      }

      function _throw(err) {
        asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err);
      }

      _next(undefined);
    });
  };
}

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    default: obj
  };
}

function _appGetInitialProps() {
  _appGetInitialProps =
  /**
  * `App` component is used for initialize of pages. It allows for overwriting and full control of the `page` initialization.
  * This allows for keeping state between navigation, custom error handling, injecting additional data.
  */
  _asyncToGenerator(function* ({
    Component,
    ctx
  }) {
    const pageProps = yield (0, _utils).loadGetInitialProps(Component, ctx);
    return {
      pageProps
    };
  });
  return _appGetInitialProps.apply(this, arguments);
}

function appGetInitialProps(_) {
  return _appGetInitialProps.apply(this, arguments);
}

class App extends _react.default.Component {
  render() {
    const {
      Component,
      pageProps
    } = this.props;
    return /*#__PURE__*/_react.default.createElement(Component, Object.assign({}, pageProps));
  }

}

App.origGetInitialProps = appGetInitialProps;
App.getInitialProps = appGetInitialProps;
exports.default = App;

/***/ }),

/***/ "./pages/_app.js":
/*!***********************!*\
  !*** ./pages/_app.js ***!
  \***********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_app__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! next/app */ "./node_modules/next/app.js");
/* harmony import */ var next_app__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_app__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-redux */ "react-redux");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_redux__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var next_redux_wrapper__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! next-redux-wrapper */ "next-redux-wrapper");
/* harmony import */ var next_redux_wrapper__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(next_redux_wrapper__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _store_store__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../store/store */ "./store/store.js");
/* harmony import */ var _global_css__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./global.css */ "./pages/global.css");
/* harmony import */ var _global_css__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_global_css__WEBPACK_IMPORTED_MODULE_6__);

var _jsxFileName = "/mnt/d/Developer/Front-End/Next/app/pages/_app.js";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }








function MyApp({
  Component,
  pageProps
}) {
  return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_redux__WEBPACK_IMPORTED_MODULE_3__.Provider, {
    store: _store_store__WEBPACK_IMPORTED_MODULE_5__.default,
    children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(Component, _objectSpread({}, pageProps), void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 13,
      columnNumber: 7
    }, this)
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 12,
    columnNumber: 5
  }, this);
}

const makestore = () => _store_store__WEBPACK_IMPORTED_MODULE_5__.default;

const wrapper = (0,next_redux_wrapper__WEBPACK_IMPORTED_MODULE_4__.createWrapper)(makestore);
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (wrapper.withRedux(MyApp));

/***/ }),

/***/ "./store/reducers/index.js":
/*!*********************************!*\
  !*** ./store/reducers/index.js ***!
  \*********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! redux */ "redux");
/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(redux__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _shoppingReducer__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./shoppingReducer */ "./store/reducers/shoppingReducer.js");
/* harmony import */ var _productsReducer__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./productsReducer */ "./store/reducers/productsReducer.js");



/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ((0,redux__WEBPACK_IMPORTED_MODULE_0__.combineReducers)({
  shoppingReducer: _shoppingReducer__WEBPACK_IMPORTED_MODULE_1__.default,
  productsReducer: _productsReducer__WEBPACK_IMPORTED_MODULE_2__.default
}));

/***/ }),

/***/ "./store/reducers/productsReducer.js":
/*!*******************************************!*\
  !*** ./store/reducers/productsReducer.js ***!
  \*******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../types */ "./store/types.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


const initialState = {
  products: [],
  cart: [],
  loading: false,
  error: ''
};

const productsReducer = (state = initialState, action) => {
  switch (action.type) {
    case _types__WEBPACK_IMPORTED_MODULE_0__.GET_ALL:
      return _objectSpread(_objectSpread({}, state), {}, {
        products: action.payload,
        loading: false,
        error: '',
        total: 0
      });

    case _types__WEBPACK_IMPORTED_MODULE_0__.LOADING:
      return _objectSpread(_objectSpread({}, state), {}, {
        loading: true
      });

    case _types__WEBPACK_IMPORTED_MODULE_0__.ERROR:
      return _objectSpread(_objectSpread({}, state), {}, {
        error: action.payload,
        loading: false
      });

    default:
      return state;
  }
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (productsReducer);

/***/ }),

/***/ "./store/reducers/shoppingReducer.js":
/*!*******************************************!*\
  !*** ./store/reducers/shoppingReducer.js ***!
  \*******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../types */ "./store/types.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


const initialState = {
  products: [],
  cart: [],
  loading: false,
  error: '',
  total: 0
};

const shoppingReducer = (state = initialState, action) => {
  switch (action.type) {
    case _types__WEBPACK_IMPORTED_MODULE_0__.ADD_CART:
      console.log(action);
      let itemInCart = state.cart.find(item => item.id === action.payload.id);
      return itemInCart ? _objectSpread(_objectSpread({}, state), {}, {
        cart: state.cart.map(item => item.id === action.payload.id ? _objectSpread(_objectSpread({}, item), {}, {
          quantity: item.quantity + 1
        }) : item)
      }) : _objectSpread(_objectSpread({}, state), {}, {
        cart: [...state.cart, _objectSpread(_objectSpread({}, action.payload), {}, {
          quantity: 1
        })]
      });

    case _types__WEBPACK_IMPORTED_MODULE_0__.REMOVE_ONE_FROM_CART:
      if (window.confirm("Do you want to delete this product?")) {
        return _objectSpread(_objectSpread({}, state), {}, {
          cart: state.cart.filter(items => items.id !== action.payload)
        });
      }

    case _types__WEBPACK_IMPORTED_MODULE_0__.REMOVE_ALL_FROM_CART:
      if (window.confirm("Do you want to delete all products?")) {
        return _objectSpread(_objectSpread({}, state), {}, {
          cart: []
        });
      }

    case _types__WEBPACK_IMPORTED_MODULE_0__.GET_TOTAL:
      const res = state.cart.reduce((prev, item) => {
        return prev + item.price * item.quantity;
      }, 0);
      return _objectSpread(_objectSpread({}, state), {}, {
        total: res
      });

    default:
      return state;
  }
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (shoppingReducer);

/***/ }),

/***/ "./store/store.js":
/*!************************!*\
  !*** ./store/store.js ***!
  \************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! redux */ "redux");
/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(redux__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var redux_thunk__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! redux-thunk */ "redux-thunk");
/* harmony import */ var redux_thunk__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(redux_thunk__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var redux_devtools_extension__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! redux-devtools-extension */ "redux-devtools-extension");
/* harmony import */ var redux_devtools_extension__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(redux_devtools_extension__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _reducers__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./reducers */ "./store/reducers/index.js");




const initialState = {};
const middleware = [(redux_thunk__WEBPACK_IMPORTED_MODULE_1___default())];
const store = (0,redux__WEBPACK_IMPORTED_MODULE_0__.createStore)(_reducers__WEBPACK_IMPORTED_MODULE_3__.default, initialState, (0,redux_devtools_extension__WEBPACK_IMPORTED_MODULE_2__.composeWithDevTools)((0,redux__WEBPACK_IMPORTED_MODULE_0__.applyMiddleware)(...middleware)));
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (store);

/***/ }),

/***/ "./store/types.js":
/*!************************!*\
  !*** ./store/types.js ***!
  \************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ADD_CART": () => (/* binding */ ADD_CART),
/* harmony export */   "INCRESE": () => (/* binding */ INCRESE),
/* harmony export */   "REDUCE": () => (/* binding */ REDUCE),
/* harmony export */   "REMOVE_ONE_FROM_CART": () => (/* binding */ REMOVE_ONE_FROM_CART),
/* harmony export */   "REMOVE_ALL_FROM_CART": () => (/* binding */ REMOVE_ALL_FROM_CART),
/* harmony export */   "CLEAR_CART": () => (/* binding */ CLEAR_CART),
/* harmony export */   "GET_ALL": () => (/* binding */ GET_ALL),
/* harmony export */   "ERROR": () => (/* binding */ ERROR),
/* harmony export */   "LOADING": () => (/* binding */ LOADING),
/* harmony export */   "GET_TOTAL": () => (/* binding */ GET_TOTAL)
/* harmony export */ });
const ADD_CART = 'ADD_CART';
const INCRESE = 'INCRESE';
const REDUCE = 'REDUCE';
const REMOVE_ONE_FROM_CART = 'REMOVE_ONE_FROM_CART';
const REMOVE_ALL_FROM_CART = 'REMOVE_ALL_FROM_CART';
const CLEAR_CART = 'CLEAR_CART';
const GET_ALL = 'GET_ALL';
const ERROR = 'ERROR';
const LOADING = 'LOADING';
const GET_TOTAL = 'GET_TOTAL';

/***/ }),

/***/ "./pages/global.css":
/*!**************************!*\
  !*** ./pages/global.css ***!
  \**************************/
/***/ (() => {



/***/ }),

/***/ "next-redux-wrapper":
/*!*************************************!*\
  !*** external "next-redux-wrapper" ***!
  \*************************************/
/***/ ((module) => {

"use strict";
module.exports = require("next-redux-wrapper");

/***/ }),

/***/ "../shared/lib/utils":
/*!************************************************!*\
  !*** external "next/dist/shared/lib/utils.js" ***!
  \************************************************/
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/utils.js");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/***/ ((module) => {

"use strict";
module.exports = require("react");

/***/ }),

/***/ "react-redux":
/*!******************************!*\
  !*** external "react-redux" ***!
  \******************************/
/***/ ((module) => {

"use strict";
module.exports = require("react-redux");

/***/ }),

/***/ "react/jsx-dev-runtime":
/*!****************************************!*\
  !*** external "react/jsx-dev-runtime" ***!
  \****************************************/
/***/ ((module) => {

"use strict";
module.exports = require("react/jsx-dev-runtime");

/***/ }),

/***/ "redux":
/*!************************!*\
  !*** external "redux" ***!
  \************************/
/***/ ((module) => {

"use strict";
module.exports = require("redux");

/***/ }),

/***/ "redux-devtools-extension":
/*!*******************************************!*\
  !*** external "redux-devtools-extension" ***!
  \*******************************************/
/***/ ((module) => {

"use strict";
module.exports = require("redux-devtools-extension");

/***/ }),

/***/ "redux-thunk":
/*!******************************!*\
  !*** external "redux-thunk" ***!
  \******************************/
/***/ ((module) => {

"use strict";
module.exports = require("redux-thunk");

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = (__webpack_exec__("./pages/_app.js"));
module.exports = __webpack_exports__;

})();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFnZXMvX2FwcC5qcyIsIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7QUFBQSx1R0FBNkM7Ozs7Ozs7Ozs7OztBQ0FoQzs7QUFDYkEsOENBQTZDO0FBQ3pDRyxFQUFBQSxLQUFLLEVBQUU7QUFEa0MsQ0FBN0M7QUFHQUgsbURBQWtEO0FBQzlDSSxFQUFBQSxVQUFVLEVBQUUsSUFEa0M7QUFFOUNDLEVBQUFBLEdBQUcsRUFBRSxZQUFXO0FBQ1osV0FBT0MsTUFBTSxDQUFDQyxlQUFkO0FBQ0g7QUFKNkMsQ0FBbEQ7QUFNQVAsdURBQXNEO0FBQ2xESSxFQUFBQSxVQUFVLEVBQUUsSUFEc0M7QUFFbERDLEVBQUFBLEdBQUcsRUFBRSxZQUFXO0FBQ1osV0FBT0MsTUFBTSxDQUFDRSxtQkFBZDtBQUNIO0FBSmlELENBQXREO0FBTUFOLGVBQUEsR0FBa0IsS0FBSyxDQUF2Qjs7QUFDQSxJQUFJUSxNQUFNLEdBQUdDLHNCQUFzQixDQUFDQyxtQkFBTyxDQUFDLG9CQUFELENBQVIsQ0FBbkM7O0FBQ0EsSUFBSU4sTUFBTSxHQUFHTSxtQkFBTyxDQUFDLGdEQUFELENBQXBCOztBQUNBLFNBQVNDLGtCQUFULENBQTRCQyxHQUE1QixFQUFpQ0MsT0FBakMsRUFBMENDLE1BQTFDLEVBQWtEQyxLQUFsRCxFQUF5REMsTUFBekQsRUFBaUVDLEdBQWpFLEVBQXNFQyxHQUF0RSxFQUEyRTtBQUN2RSxNQUFJO0FBQ0EsUUFBSUMsSUFBSSxHQUFHUCxHQUFHLENBQUNLLEdBQUQsQ0FBSCxDQUFTQyxHQUFULENBQVg7QUFDQSxRQUFJakIsS0FBSyxHQUFHa0IsSUFBSSxDQUFDbEIsS0FBakI7QUFDSCxHQUhELENBR0UsT0FBT21CLEtBQVAsRUFBYztBQUNaTixJQUFBQSxNQUFNLENBQUNNLEtBQUQsQ0FBTjtBQUNBO0FBQ0g7O0FBQ0QsTUFBSUQsSUFBSSxDQUFDRSxJQUFULEVBQWU7QUFDWFIsSUFBQUEsT0FBTyxDQUFDWixLQUFELENBQVA7QUFDSCxHQUZELE1BRU87QUFDSHFCLElBQUFBLE9BQU8sQ0FBQ1QsT0FBUixDQUFnQlosS0FBaEIsRUFBdUJzQixJQUF2QixDQUE0QlIsS0FBNUIsRUFBbUNDLE1BQW5DO0FBQ0g7QUFDSjs7QUFDRCxTQUFTUSxpQkFBVCxDQUEyQkMsRUFBM0IsRUFBK0I7QUFDM0IsU0FBTyxZQUFXO0FBQ2QsUUFBSUMsSUFBSSxHQUFHLElBQVg7QUFBQSxRQUFpQkMsSUFBSSxHQUFHQyxTQUF4QjtBQUNBLFdBQU8sSUFBSU4sT0FBSixDQUFZLFVBQVNULE9BQVQsRUFBa0JDLE1BQWxCLEVBQTBCO0FBQ3pDLFVBQUlGLEdBQUcsR0FBR2EsRUFBRSxDQUFDSSxLQUFILENBQVNILElBQVQsRUFBZUMsSUFBZixDQUFWOztBQUNBLGVBQVNaLEtBQVQsQ0FBZWQsS0FBZixFQUFzQjtBQUNsQlUsUUFBQUEsa0JBQWtCLENBQUNDLEdBQUQsRUFBTUMsT0FBTixFQUFlQyxNQUFmLEVBQXVCQyxLQUF2QixFQUE4QkMsTUFBOUIsRUFBc0MsTUFBdEMsRUFBOENmLEtBQTlDLENBQWxCO0FBQ0g7O0FBQ0QsZUFBU2UsTUFBVCxDQUFnQmMsR0FBaEIsRUFBcUI7QUFDakJuQixRQUFBQSxrQkFBa0IsQ0FBQ0MsR0FBRCxFQUFNQyxPQUFOLEVBQWVDLE1BQWYsRUFBdUJDLEtBQXZCLEVBQThCQyxNQUE5QixFQUFzQyxPQUF0QyxFQUErQ2MsR0FBL0MsQ0FBbEI7QUFDSDs7QUFDRGYsTUFBQUEsS0FBSyxDQUFDZ0IsU0FBRCxDQUFMO0FBQ0gsS0FUTSxDQUFQO0FBVUgsR0FaRDtBQWFIOztBQUNELFNBQVN0QixzQkFBVCxDQUFnQ3VCLEdBQWhDLEVBQXFDO0FBQ2pDLFNBQU9BLEdBQUcsSUFBSUEsR0FBRyxDQUFDQyxVQUFYLEdBQXdCRCxHQUF4QixHQUE4QjtBQUNqQ3pCLElBQUFBLE9BQU8sRUFBRXlCO0FBRHdCLEdBQXJDO0FBR0g7O0FBQ0QsU0FBU0UsbUJBQVQsR0FBK0I7QUFDM0JBLEVBQUFBLG1CQUFtQjtBQUFHO0FBQzFCO0FBQ0E7QUFDQTtBQUFJVixFQUFBQSxpQkFBaUIsQ0FBQyxXQUFVO0FBQUVXLElBQUFBLFNBQUY7QUFBY0MsSUFBQUE7QUFBZCxHQUFWLEVBQWdDO0FBQzlDLFVBQU1DLFNBQVMsR0FBRyxNQUFNLENBQUMsR0FBR2pDLE1BQUosRUFBWWtDLG1CQUFaLENBQWdDSCxTQUFoQyxFQUEyQ0MsR0FBM0MsQ0FBeEI7QUFDQSxXQUFPO0FBQ0hDLE1BQUFBO0FBREcsS0FBUDtBQUdILEdBTGdCLENBSGpCO0FBU0EsU0FBT0gsbUJBQW1CLENBQUNMLEtBQXBCLENBQTBCLElBQTFCLEVBQWdDRCxTQUFoQyxDQUFQO0FBQ0g7O0FBQ0QsU0FBU1csa0JBQVQsQ0FBNEJDLENBQTVCLEVBQStCO0FBQzNCLFNBQU9OLG1CQUFtQixDQUFDTCxLQUFwQixDQUEwQixJQUExQixFQUFnQ0QsU0FBaEMsQ0FBUDtBQUNIOztBQUNELE1BQU1hLEdBQU4sU0FBa0JqQyxNQUFNLENBQUNELE9BQVAsQ0FBZTRCLFNBQWpDLENBQTJDO0FBQ3ZDTyxFQUFBQSxNQUFNLEdBQUc7QUFDTCxVQUFNO0FBQUVQLE1BQUFBLFNBQUY7QUFBY0UsTUFBQUE7QUFBZCxRQUE2QixLQUFLTSxLQUF4QztBQUNBLFdBQU8sYUFBY25DLE1BQU0sQ0FBQ0QsT0FBUCxDQUFlcUMsYUFBZixDQUE2QlQsU0FBN0IsRUFBd0NyQyxNQUFNLENBQUMrQyxNQUFQLENBQWMsRUFBZCxFQUMxRFIsU0FEMEQsQ0FBeEMsQ0FBckI7QUFFSDs7QUFMc0M7O0FBTzNDSSxHQUFHLENBQUNLLG1CQUFKLEdBQTBCUCxrQkFBMUI7QUFDQUUsR0FBRyxDQUFDTSxlQUFKLEdBQXNCUixrQkFBdEI7QUFDQXZDLGVBQUEsR0FBa0J5QyxHQUFsQjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzdFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7O0FBR0EsU0FBU1csS0FBVCxDQUFlO0FBQUVqQixFQUFBQSxTQUFGO0FBQWFFLEVBQUFBO0FBQWIsQ0FBZixFQUF5QztBQUNyQyxzQkFDQSw4REFBQyxpREFBRDtBQUFVLFNBQUssRUFBRWMsaURBQWpCO0FBQUEsMkJBQ0UsOERBQUMsU0FBRCxvQkFBZWQsU0FBZjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxVQURBO0FBS0g7O0FBRUQsTUFBTWdCLFNBQVMsR0FBRyxNQUFLRixpREFBdkI7O0FBQ0EsTUFBTUcsT0FBTyxHQUFHSixpRUFBYSxDQUFDRyxTQUFELENBQTdCO0FBRUEsaUVBQWVDLE9BQU8sQ0FBQ0MsU0FBUixDQUFrQkgsS0FBbEIsQ0FBZjs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3BCQTtBQUNBO0FBQ0E7QUFFQSxpRUFBZUksc0RBQWUsQ0FBQztBQUMzQkMsRUFBQUEsZUFEMkI7QUFFM0JDLEVBQUFBLGVBQWVBLHVEQUFBQTtBQUZZLENBQUQsQ0FBOUI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNKQTtBQUdBLE1BQU1FLFlBQVksR0FBRTtBQUNoQkMsRUFBQUEsUUFBUSxFQUFFLEVBRE07QUFFaEJDLEVBQUFBLElBQUksRUFBRSxFQUZVO0FBR2hCQyxFQUFBQSxPQUFPLEVBQUUsS0FITztBQUloQjNDLEVBQUFBLEtBQUssRUFBQztBQUpVLENBQXBCOztBQVFBLE1BQU1zQyxlQUFlLEdBQUcsQ0FBQ00sS0FBSyxHQUFHSixZQUFULEVBQXVCSyxNQUF2QixLQUFrQztBQUN0RCxVQUFRQSxNQUFNLENBQUNDLElBQWY7QUFDSSxTQUFLUCwyQ0FBTDtBQUNJLDZDQUNPSyxLQURQO0FBRUlILFFBQUFBLFFBQVEsRUFBRUksTUFBTSxDQUFDRyxPQUZyQjtBQUdJTCxRQUFBQSxPQUFPLEVBQUUsS0FIYjtBQUlJM0MsUUFBQUEsS0FBSyxFQUFFLEVBSlg7QUFLSWlELFFBQUFBLEtBQUssRUFBQztBQUxWOztBQVFKLFNBQUtWLDJDQUFMO0FBQ0ksNkNBQVdLLEtBQVg7QUFBa0JELFFBQUFBLE9BQU8sRUFBRTtBQUEzQjs7QUFFSixTQUFLSix5Q0FBTDtBQUNZLDZDQUFXSyxLQUFYO0FBQWtCNUMsUUFBQUEsS0FBSyxFQUFFNkMsTUFBTSxDQUFDRyxPQUFoQztBQUF5Q0wsUUFBQUEsT0FBTyxFQUFFO0FBQWxEOztBQUVaO0FBQ0ksYUFBT0MsS0FBUDtBQWpCUjtBQW1CSCxDQXBCRDs7QUFzQkEsaUVBQWVOLGVBQWY7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNqQ0E7QUFHQSxNQUFNRSxZQUFZLEdBQUU7QUFDaEJDLEVBQUFBLFFBQVEsRUFBRSxFQURNO0FBRWhCQyxFQUFBQSxJQUFJLEVBQUUsRUFGVTtBQUdoQkMsRUFBQUEsT0FBTyxFQUFFLEtBSE87QUFJaEIzQyxFQUFBQSxLQUFLLEVBQUMsRUFKVTtBQUtoQmlELEVBQUFBLEtBQUssRUFBQztBQUxVLENBQXBCOztBQVdBLE1BQU1aLGVBQWUsR0FBRyxDQUFDTyxLQUFLLEdBQUdKLFlBQVQsRUFBdUJLLE1BQXZCLEtBQWtDO0FBQ3RELFVBQVFBLE1BQU0sQ0FBQ0MsSUFBZjtBQUVJLFNBQUtQLDRDQUFMO0FBQ0ljLE1BQUFBLE9BQU8sQ0FBQ0MsR0FBUixDQUFZVCxNQUFaO0FBQ0EsVUFBSVUsVUFBVSxHQUFHWCxLQUFLLENBQUNGLElBQU4sQ0FBV2MsSUFBWCxDQUFnQkMsSUFBSSxJQUFFQSxJQUFJLENBQUNDLEVBQUwsS0FBWWIsTUFBTSxDQUFDRyxPQUFQLENBQWVVLEVBQWpELENBQWpCO0FBQ0EsYUFBT0gsVUFBVSxtQ0FFVlgsS0FGVTtBQUdiRixRQUFBQSxJQUFJLEVBQUVFLEtBQUssQ0FBQ0YsSUFBTixDQUFXaUIsR0FBWCxDQUFlRixJQUFJLElBQUVBLElBQUksQ0FBQ0MsRUFBTCxLQUFZYixNQUFNLENBQUNHLE9BQVAsQ0FBZVUsRUFBM0IsbUNBQ2pCRCxJQURpQjtBQUNYRyxVQUFBQSxRQUFRLEVBQUVILElBQUksQ0FBQ0csUUFBTCxHQUFjO0FBRGIsYUFFckJILElBRkE7QUFITywyQ0FRVmIsS0FSVTtBQVNiRixRQUFBQSxJQUFJLEVBQUMsQ0FBQyxHQUFHRSxLQUFLLENBQUNGLElBQVYsa0NBQW9CRyxNQUFNLENBQUNHLE9BQTNCO0FBQW9DWSxVQUFBQSxRQUFRLEVBQUU7QUFBOUM7QUFUUSxRQUFqQjs7QUFhSixTQUFLckIsd0RBQUw7QUFDSSxVQUFHdUIsTUFBTSxDQUFDQyxPQUFQLENBQWUscUNBQWYsQ0FBSCxFQUF5RDtBQUNyRCwrQ0FDT25CLEtBRFA7QUFFSUYsVUFBQUEsSUFBSSxFQUFFRSxLQUFLLENBQUNGLElBQU4sQ0FBV3NCLE1BQVgsQ0FBa0JDLEtBQUssSUFBSUEsS0FBSyxDQUFDUCxFQUFOLEtBQWFiLE1BQU0sQ0FBQ0csT0FBL0M7QUFGVjtBQUtIOztBQUVMLFNBQUtULHdEQUFMO0FBQ0ksVUFBR3VCLE1BQU0sQ0FBQ0MsT0FBUCxDQUFlLHFDQUFmLENBQUgsRUFBeUQ7QUFDckQsK0NBQ1duQixLQURYO0FBRVFGLFVBQUFBLElBQUksRUFBQztBQUZiO0FBSUg7O0FBRUwsU0FBS0gsNkNBQUw7QUFDSSxZQUFNNkIsR0FBRyxHQUFHeEIsS0FBSyxDQUFDRixJQUFOLENBQVcyQixNQUFYLENBQWtCLENBQUNDLElBQUQsRUFBT2IsSUFBUCxLQUFnQjtBQUN0QyxlQUFPYSxJQUFJLEdBQUliLElBQUksQ0FBQ2MsS0FBTCxHQUFhZCxJQUFJLENBQUNHLFFBQWpDO0FBQ0gsT0FGTyxFQUVOLENBRk0sQ0FBWjtBQUdBLDZDQUNPaEIsS0FEUDtBQUVJSyxRQUFBQSxLQUFLLEVBQUVtQjtBQUZYOztBQU9KO0FBQ0ksYUFBT3hCLEtBQVA7QUEvQ1I7QUFpREgsQ0FsREQ7O0FBb0RBLGlFQUFlUCxlQUFmOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDbEVBO0FBQ0E7QUFDQTtBQUVBO0FBRUEsTUFBTUcsWUFBWSxHQUFDLEVBQW5CO0FBQ0EsTUFBTXFDLFVBQVUsR0FBRyxDQUFDSCxvREFBRCxDQUFuQjtBQUdBLE1BQU0zQyxLQUFLLEdBQUcwQyxrREFBVyxDQUFDRyw4Q0FBRCxFQUFjcEMsWUFBZCxFQUE0Qm1DLDZFQUFtQixDQUFDSCxzREFBZSxDQUFDLEdBQUdLLFVBQUosQ0FBaEIsQ0FBL0MsQ0FBekI7QUFFQSxpRUFBZTlDLEtBQWY7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ1pPLE1BQU1xQixRQUFRLEdBQUcsVUFBakI7QUFDQSxNQUFNMEIsT0FBTyxHQUFHLFNBQWhCO0FBQ0EsTUFBTUMsTUFBTSxHQUFHLFFBQWY7QUFDQSxNQUFNbEIsb0JBQW9CLEdBQUcsc0JBQTdCO0FBQ0EsTUFBTUssb0JBQW9CLEdBQUcsc0JBQTdCO0FBQ0EsTUFBTWMsVUFBVSxHQUFHLFlBQW5CO0FBQ0EsTUFBTWpDLE9BQU8sR0FBRyxTQUFoQjtBQUNBLE1BQU1JLEtBQUssR0FBRyxPQUFkO0FBQ0EsTUFBTUQsT0FBTyxHQUFHLFNBQWhCO0FBQ0EsTUFBTWlCLFNBQVMsR0FBQyxXQUFoQjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDVFA7Ozs7Ozs7Ozs7O0FDQUE7Ozs7Ozs7Ozs7O0FDQUE7Ozs7Ozs7Ozs7O0FDQUE7Ozs7Ozs7Ozs7O0FDQUE7Ozs7Ozs7Ozs7O0FDQUE7Ozs7Ozs7Ozs7O0FDQUE7Ozs7Ozs7Ozs7O0FDQUEiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9hcHAvLi9ub2RlX21vZHVsZXMvbmV4dC9hcHAuanMiLCJ3ZWJwYWNrOi8vYXBwLy4vbm9kZV9tb2R1bGVzL25leHQvZGlzdC9wYWdlcy9fYXBwLmpzIiwid2VicGFjazovL2FwcC8uL3BhZ2VzL19hcHAuanMiLCJ3ZWJwYWNrOi8vYXBwLy4vc3RvcmUvcmVkdWNlcnMvaW5kZXguanMiLCJ3ZWJwYWNrOi8vYXBwLy4vc3RvcmUvcmVkdWNlcnMvcHJvZHVjdHNSZWR1Y2VyLmpzIiwid2VicGFjazovL2FwcC8uL3N0b3JlL3JlZHVjZXJzL3Nob3BwaW5nUmVkdWNlci5qcyIsIndlYnBhY2s6Ly9hcHAvLi9zdG9yZS9zdG9yZS5qcyIsIndlYnBhY2s6Ly9hcHAvLi9zdG9yZS90eXBlcy5qcyIsIndlYnBhY2s6Ly9hcHAvZXh0ZXJuYWwgXCJuZXh0LXJlZHV4LXdyYXBwZXJcIiIsIndlYnBhY2s6Ly9hcHAvZXh0ZXJuYWwgXCJuZXh0L2Rpc3Qvc2hhcmVkL2xpYi91dGlscy5qc1wiIiwid2VicGFjazovL2FwcC9leHRlcm5hbCBcInJlYWN0XCIiLCJ3ZWJwYWNrOi8vYXBwL2V4dGVybmFsIFwicmVhY3QtcmVkdXhcIiIsIndlYnBhY2s6Ly9hcHAvZXh0ZXJuYWwgXCJyZWFjdC9qc3gtZGV2LXJ1bnRpbWVcIiIsIndlYnBhY2s6Ly9hcHAvZXh0ZXJuYWwgXCJyZWR1eFwiIiwid2VicGFjazovL2FwcC9leHRlcm5hbCBcInJlZHV4LWRldnRvb2xzLWV4dGVuc2lvblwiIiwid2VicGFjazovL2FwcC9leHRlcm5hbCBcInJlZHV4LXRodW5rXCIiXSwic291cmNlc0NvbnRlbnQiOlsibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKCcuL2Rpc3QvcGFnZXMvX2FwcCcpXG4iLCJcInVzZSBzdHJpY3RcIjtcbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICAgIHZhbHVlOiB0cnVlXG59KTtcbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIkFwcEluaXRpYWxQcm9wc1wiLCB7XG4gICAgZW51bWVyYWJsZTogdHJ1ZSxcbiAgICBnZXQ6IGZ1bmN0aW9uKCkge1xuICAgICAgICByZXR1cm4gX3V0aWxzLkFwcEluaXRpYWxQcm9wcztcbiAgICB9XG59KTtcbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIk5leHRXZWJWaXRhbHNNZXRyaWNcIiwge1xuICAgIGVudW1lcmFibGU6IHRydWUsXG4gICAgZ2V0OiBmdW5jdGlvbigpIHtcbiAgICAgICAgcmV0dXJuIF91dGlscy5OZXh0V2ViVml0YWxzTWV0cmljO1xuICAgIH1cbn0pO1xuZXhwb3J0cy5kZWZhdWx0ID0gdm9pZCAwO1xudmFyIF9yZWFjdCA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQocmVxdWlyZShcInJlYWN0XCIpKTtcbnZhciBfdXRpbHMgPSByZXF1aXJlKFwiLi4vc2hhcmVkL2xpYi91dGlsc1wiKTtcbmZ1bmN0aW9uIGFzeW5jR2VuZXJhdG9yU3RlcChnZW4sIHJlc29sdmUsIHJlamVjdCwgX25leHQsIF90aHJvdywga2V5LCBhcmcpIHtcbiAgICB0cnkge1xuICAgICAgICB2YXIgaW5mbyA9IGdlbltrZXldKGFyZyk7XG4gICAgICAgIHZhciB2YWx1ZSA9IGluZm8udmFsdWU7XG4gICAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICAgICAgcmVqZWN0KGVycm9yKTtcbiAgICAgICAgcmV0dXJuO1xuICAgIH1cbiAgICBpZiAoaW5mby5kb25lKSB7XG4gICAgICAgIHJlc29sdmUodmFsdWUpO1xuICAgIH0gZWxzZSB7XG4gICAgICAgIFByb21pc2UucmVzb2x2ZSh2YWx1ZSkudGhlbihfbmV4dCwgX3Rocm93KTtcbiAgICB9XG59XG5mdW5jdGlvbiBfYXN5bmNUb0dlbmVyYXRvcihmbikge1xuICAgIHJldHVybiBmdW5jdGlvbigpIHtcbiAgICAgICAgdmFyIHNlbGYgPSB0aGlzLCBhcmdzID0gYXJndW1lbnRzO1xuICAgICAgICByZXR1cm4gbmV3IFByb21pc2UoZnVuY3Rpb24ocmVzb2x2ZSwgcmVqZWN0KSB7XG4gICAgICAgICAgICB2YXIgZ2VuID0gZm4uYXBwbHkoc2VsZiwgYXJncyk7XG4gICAgICAgICAgICBmdW5jdGlvbiBfbmV4dCh2YWx1ZSkge1xuICAgICAgICAgICAgICAgIGFzeW5jR2VuZXJhdG9yU3RlcChnZW4sIHJlc29sdmUsIHJlamVjdCwgX25leHQsIF90aHJvdywgXCJuZXh0XCIsIHZhbHVlKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGZ1bmN0aW9uIF90aHJvdyhlcnIpIHtcbiAgICAgICAgICAgICAgICBhc3luY0dlbmVyYXRvclN0ZXAoZ2VuLCByZXNvbHZlLCByZWplY3QsIF9uZXh0LCBfdGhyb3csIFwidGhyb3dcIiwgZXJyKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF9uZXh0KHVuZGVmaW5lZCk7XG4gICAgICAgIH0pO1xuICAgIH07XG59XG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikge1xuICAgIHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7XG4gICAgICAgIGRlZmF1bHQ6IG9ialxuICAgIH07XG59XG5mdW5jdGlvbiBfYXBwR2V0SW5pdGlhbFByb3BzKCkge1xuICAgIF9hcHBHZXRJbml0aWFsUHJvcHMgPSAvKipcbiAqIGBBcHBgIGNvbXBvbmVudCBpcyB1c2VkIGZvciBpbml0aWFsaXplIG9mIHBhZ2VzLiBJdCBhbGxvd3MgZm9yIG92ZXJ3cml0aW5nIGFuZCBmdWxsIGNvbnRyb2wgb2YgdGhlIGBwYWdlYCBpbml0aWFsaXphdGlvbi5cbiAqIFRoaXMgYWxsb3dzIGZvciBrZWVwaW5nIHN0YXRlIGJldHdlZW4gbmF2aWdhdGlvbiwgY3VzdG9tIGVycm9yIGhhbmRsaW5nLCBpbmplY3RpbmcgYWRkaXRpb25hbCBkYXRhLlxuICovIF9hc3luY1RvR2VuZXJhdG9yKGZ1bmN0aW9uKih7IENvbXBvbmVudCAsIGN0eCAgfSkge1xuICAgICAgICBjb25zdCBwYWdlUHJvcHMgPSB5aWVsZCAoMCwgX3V0aWxzKS5sb2FkR2V0SW5pdGlhbFByb3BzKENvbXBvbmVudCwgY3R4KTtcbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgIHBhZ2VQcm9wc1xuICAgICAgICB9O1xuICAgIH0pO1xuICAgIHJldHVybiBfYXBwR2V0SW5pdGlhbFByb3BzLmFwcGx5KHRoaXMsIGFyZ3VtZW50cyk7XG59XG5mdW5jdGlvbiBhcHBHZXRJbml0aWFsUHJvcHMoXykge1xuICAgIHJldHVybiBfYXBwR2V0SW5pdGlhbFByb3BzLmFwcGx5KHRoaXMsIGFyZ3VtZW50cyk7XG59XG5jbGFzcyBBcHAgZXh0ZW5kcyBfcmVhY3QuZGVmYXVsdC5Db21wb25lbnQge1xuICAgIHJlbmRlcigpIHtcbiAgICAgICAgY29uc3QgeyBDb21wb25lbnQgLCBwYWdlUHJvcHMgIH0gPSB0aGlzLnByb3BzO1xuICAgICAgICByZXR1cm4oLyojX19QVVJFX18qLyBfcmVhY3QuZGVmYXVsdC5jcmVhdGVFbGVtZW50KENvbXBvbmVudCwgT2JqZWN0LmFzc2lnbih7XG4gICAgICAgIH0sIHBhZ2VQcm9wcykpKTtcbiAgICB9XG59XG5BcHAub3JpZ0dldEluaXRpYWxQcm9wcyA9IGFwcEdldEluaXRpYWxQcm9wcztcbkFwcC5nZXRJbml0aWFsUHJvcHMgPSBhcHBHZXRJbml0aWFsUHJvcHM7XG5leHBvcnRzLmRlZmF1bHQgPSBBcHA7XG5cbi8vIyBzb3VyY2VNYXBwaW5nVVJMPV9hcHAuanMubWFwIiwiaW1wb3J0IEFwcCBmcm9tICduZXh0L2FwcCc7XHJcbmltcG9ydCBSZWFjdCBmcm9tICAncmVhY3QnO1xyXG5pbXBvcnQge1Byb3ZpZGVyfSBmcm9tICdyZWFjdC1yZWR1eCc7XHJcbmltcG9ydCB7IGNyZWF0ZVdyYXBwZXIgfSBmcm9tICduZXh0LXJlZHV4LXdyYXBwZXInO1xyXG5pbXBvcnQgc3RvcmUgZnJvbSAnLi4vc3RvcmUvc3RvcmUnO1xyXG5cclxuaW1wb3J0ICcuL2dsb2JhbC5jc3MnO1xyXG5cclxuXHJcbmZ1bmN0aW9uIE15QXBwKHsgQ29tcG9uZW50LCBwYWdlUHJvcHMgfSkge1xyXG4gICAgcmV0dXJuIChcclxuICAgIDxQcm92aWRlciBzdG9yZT17c3RvcmV9PlxyXG4gICAgICA8Q29tcG9uZW50IHsuLi5wYWdlUHJvcHN9IC8+XHJcbiAgICA8L1Byb3ZpZGVyPlxyXG4gICAgKSAgXHJcbn1cclxuICBcclxuY29uc3QgbWFrZXN0b3JlID0gKCkgPT5zdG9yZTtcclxuY29uc3Qgd3JhcHBlciA9IGNyZWF0ZVdyYXBwZXIobWFrZXN0b3JlKTtcclxuICBcclxuZXhwb3J0IGRlZmF1bHQgd3JhcHBlci53aXRoUmVkdXgoTXlBcHApXHJcbiAgIiwiaW1wb3J0IHsgY29tYmluZVJlZHVjZXJzfSBmcm9tICdyZWR1eCdcclxuaW1wb3J0IHNob3BwaW5nUmVkdWNlciBmcm9tICcuL3Nob3BwaW5nUmVkdWNlcic7XHJcbmltcG9ydCBwcm9kdWN0c1JlZHVjZXIgZnJvbSAnLi9wcm9kdWN0c1JlZHVjZXInO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY29tYmluZVJlZHVjZXJzKHtcclxuICAgIHNob3BwaW5nUmVkdWNlcixcclxuICAgIHByb2R1Y3RzUmVkdWNlclxyXG5cclxufSk7XHJcblxyXG5cclxuIiwiaW1wb3J0ICogYXMgdHlwZXMgZnJvbSAnLi4vdHlwZXMnXHJcblxyXG5cclxuY29uc3QgaW5pdGlhbFN0YXRlPSB7XHJcbiAgICBwcm9kdWN0czogW10sXHJcbiAgICBjYXJ0OiBbXSxcclxuICAgIGxvYWRpbmc6IGZhbHNlLFxyXG4gICAgZXJyb3I6JydcclxufVxyXG5cclxuXHJcbmNvbnN0IHByb2R1Y3RzUmVkdWNlciA9IChzdGF0ZSA9IGluaXRpYWxTdGF0ZSwgYWN0aW9uKSA9PiB7XHJcbiAgICBzd2l0Y2ggKGFjdGlvbi50eXBlKSB7XHJcbiAgICAgICAgY2FzZSB0eXBlcy5HRVRfQUxMOlxyXG4gICAgICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICAgICAgLi4uc3RhdGUsXHJcbiAgICAgICAgICAgICAgICBwcm9kdWN0czogYWN0aW9uLnBheWxvYWQsXHJcbiAgICAgICAgICAgICAgICBsb2FkaW5nOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgIGVycm9yOiAnJyxcclxuICAgICAgICAgICAgICAgIHRvdGFsOjBcclxuICAgICAgICAgICAgfTtcclxuXHJcbiAgICAgICAgY2FzZSB0eXBlcy5MT0FESU5HOlxyXG4gICAgICAgICAgICByZXR1cm4gey4uLnN0YXRlLCBsb2FkaW5nOiB0cnVlfTtcclxuXHJcbiAgICAgICAgY2FzZSB0eXBlcy5FUlJPUjpcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gey4uLnN0YXRlLCBlcnJvcjogYWN0aW9uLnBheWxvYWQsIGxvYWRpbmc6IGZhbHNlfTtcclxuICAgICAgICAgICAgICAgICAgICBcclxuICAgICAgICBkZWZhdWx0OlxyXG4gICAgICAgICAgICByZXR1cm4gc3RhdGVcclxuICAgIH1cclxufVxyXG4gICAgICAgICAgICBcclxuZXhwb3J0IGRlZmF1bHQgcHJvZHVjdHNSZWR1Y2VyOyIsImltcG9ydCAqIGFzIHR5cGVzIGZyb20gJy4uL3R5cGVzJ1xyXG5cclxuXHJcbmNvbnN0IGluaXRpYWxTdGF0ZT0ge1xyXG4gICAgcHJvZHVjdHM6IFtdLFxyXG4gICAgY2FydDogW10sXHJcbiAgICBsb2FkaW5nOiBmYWxzZSxcclxuICAgIGVycm9yOicnLFxyXG4gICAgdG90YWw6MFxyXG59XHJcblxyXG5cclxuXHJcblxyXG5jb25zdCBzaG9wcGluZ1JlZHVjZXIgPSAoc3RhdGUgPSBpbml0aWFsU3RhdGUsIGFjdGlvbikgPT4ge1xyXG4gICAgc3dpdGNoIChhY3Rpb24udHlwZSkge1xyXG4gICAgICAgIFxyXG4gICAgICAgIGNhc2UgdHlwZXMuQUREX0NBUlQ6XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKGFjdGlvbilcclxuICAgICAgICAgICAgbGV0IGl0ZW1JbkNhcnQgPSBzdGF0ZS5jYXJ0LmZpbmQoaXRlbT0+aXRlbS5pZCA9PT0gYWN0aW9uLnBheWxvYWQuaWQpXHJcbiAgICAgICAgICAgIHJldHVybiBpdGVtSW5DYXJ0IFxyXG4gICAgICAgICAgICA/e1xyXG4gICAgICAgICAgICAgICAgLi4uc3RhdGUsXHJcbiAgICAgICAgICAgICAgICBjYXJ0OiBzdGF0ZS5jYXJ0Lm1hcChpdGVtPT5pdGVtLmlkID09PSBhY3Rpb24ucGF5bG9hZC5pZCBcclxuICAgICAgICAgICAgICAgICAgICA/IHsuLi5pdGVtLCBxdWFudGl0eTogaXRlbS5xdWFudGl0eSsxfVxyXG4gICAgICAgICAgICAgICAgICAgIDogaXRlbSlcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICA6e1xyXG4gICAgICAgICAgICAgICAgLi4uc3RhdGUsXHJcbiAgICAgICAgICAgICAgICBjYXJ0OlsuLi5zdGF0ZS5jYXJ0LCB7Li4uYWN0aW9uLnBheWxvYWQsIHF1YW50aXR5OiAxfV1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIFxyXG4gICAgICAgICAgICBcclxuICAgICAgICBjYXNlIHR5cGVzLlJFTU9WRV9PTkVfRlJPTV9DQVJUOlxyXG4gICAgICAgICAgICBpZih3aW5kb3cuY29uZmlybShcIkRvIHlvdSB3YW50IHRvIGRlbGV0ZSB0aGlzIHByb2R1Y3Q/XCIpKXtcclxuICAgICAgICAgICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgICAgICAgICAgLi4uc3RhdGUsIFxyXG4gICAgICAgICAgICAgICAgICAgIGNhcnQ6IHN0YXRlLmNhcnQuZmlsdGVyKGl0ZW1zID0+IGl0ZW1zLmlkICE9PSBhY3Rpb24ucGF5bG9hZClcclxuICAgICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICBjYXNlIHR5cGVzLlJFTU9WRV9BTExfRlJPTV9DQVJUOlxyXG4gICAgICAgICAgICBpZih3aW5kb3cuY29uZmlybShcIkRvIHlvdSB3YW50IHRvIGRlbGV0ZSBhbGwgcHJvZHVjdHM/XCIpKXsgICAgXHJcbiAgICAgICAgICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAuLi5zdGF0ZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgY2FydDpbXVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgXHJcbiAgICAgICAgY2FzZSB0eXBlcy5HRVRfVE9UQUw6XHJcbiAgICAgICAgICAgIGNvbnN0IHJlcyA9IHN0YXRlLmNhcnQucmVkdWNlKChwcmV2LCBpdGVtKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHByZXYgKyAoaXRlbS5wcmljZSAqIGl0ZW0ucXVhbnRpdHkpO1xyXG4gICAgICAgICAgICAgICAgfSwwKVxyXG4gICAgICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICAgICAgLi4uc3RhdGUsXHJcbiAgICAgICAgICAgICAgICB0b3RhbDogcmVzXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgXHJcbiAgICAgICAgXHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgIGRlZmF1bHQ6XHJcbiAgICAgICAgICAgIHJldHVybiBzdGF0ZVxyXG4gICAgfVxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBzaG9wcGluZ1JlZHVjZXI7IiwiaW1wb3J0IHsgYXBwbHlNaWRkbGV3YXJlLCBjcmVhdGVTdG9yZSB9IGZyb20gXCJyZWR1eFwiXHJcbmltcG9ydCB0aHVuayBmcm9tIFwicmVkdXgtdGh1bmtcIlxyXG5pbXBvcnQgeyBjb21wb3NlV2l0aERldlRvb2xzIH0gZnJvbSBcInJlZHV4LWRldnRvb2xzLWV4dGVuc2lvblwiXHJcblxyXG5pbXBvcnQgcm9vdFJlZHVjZXIgZnJvbSAnLi9yZWR1Y2Vycyc7XHJcblxyXG5jb25zdCBpbml0aWFsU3RhdGU9e307XHJcbmNvbnN0IG1pZGRsZXdhcmUgPSBbdGh1bmtdO1xyXG5cclxuXHJcbmNvbnN0IHN0b3JlID0gY3JlYXRlU3RvcmUocm9vdFJlZHVjZXIsIGluaXRpYWxTdGF0ZSwgY29tcG9zZVdpdGhEZXZUb29scyhhcHBseU1pZGRsZXdhcmUoLi4ubWlkZGxld2FyZSkpKTtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IHN0b3JlOyIsImV4cG9ydCBjb25zdCBBRERfQ0FSVCA9ICdBRERfQ0FSVCdcclxuZXhwb3J0IGNvbnN0IElOQ1JFU0UgPSAnSU5DUkVTRSdcclxuZXhwb3J0IGNvbnN0IFJFRFVDRSA9ICdSRURVQ0UnXHJcbmV4cG9ydCBjb25zdCBSRU1PVkVfT05FX0ZST01fQ0FSVCA9ICdSRU1PVkVfT05FX0ZST01fQ0FSVCdcclxuZXhwb3J0IGNvbnN0IFJFTU9WRV9BTExfRlJPTV9DQVJUID0gJ1JFTU9WRV9BTExfRlJPTV9DQVJUJ1xyXG5leHBvcnQgY29uc3QgQ0xFQVJfQ0FSVCA9ICdDTEVBUl9DQVJUJ1xyXG5leHBvcnQgY29uc3QgR0VUX0FMTCA9ICdHRVRfQUxMJ1xyXG5leHBvcnQgY29uc3QgRVJST1IgPSAnRVJST1InXHJcbmV4cG9ydCBjb25zdCBMT0FESU5HID0gJ0xPQURJTkcnXHJcbmV4cG9ydCBjb25zdCBHRVRfVE9UQUw9J0dFVF9UT1RBTCdcclxuXHJcbiIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIm5leHQtcmVkdXgtd3JhcHBlclwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJuZXh0L2Rpc3Qvc2hhcmVkL2xpYi91dGlscy5qc1wiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJyZWFjdFwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJyZWFjdC1yZWR1eFwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJyZWFjdC9qc3gtZGV2LXJ1bnRpbWVcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwicmVkdXhcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwicmVkdXgtZGV2dG9vbHMtZXh0ZW5zaW9uXCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcInJlZHV4LXRodW5rXCIpOyJdLCJuYW1lcyI6WyJPYmplY3QiLCJkZWZpbmVQcm9wZXJ0eSIsImV4cG9ydHMiLCJ2YWx1ZSIsImVudW1lcmFibGUiLCJnZXQiLCJfdXRpbHMiLCJBcHBJbml0aWFsUHJvcHMiLCJOZXh0V2ViVml0YWxzTWV0cmljIiwiZGVmYXVsdCIsIl9yZWFjdCIsIl9pbnRlcm9wUmVxdWlyZURlZmF1bHQiLCJyZXF1aXJlIiwiYXN5bmNHZW5lcmF0b3JTdGVwIiwiZ2VuIiwicmVzb2x2ZSIsInJlamVjdCIsIl9uZXh0IiwiX3Rocm93Iiwia2V5IiwiYXJnIiwiaW5mbyIsImVycm9yIiwiZG9uZSIsIlByb21pc2UiLCJ0aGVuIiwiX2FzeW5jVG9HZW5lcmF0b3IiLCJmbiIsInNlbGYiLCJhcmdzIiwiYXJndW1lbnRzIiwiYXBwbHkiLCJlcnIiLCJ1bmRlZmluZWQiLCJvYmoiLCJfX2VzTW9kdWxlIiwiX2FwcEdldEluaXRpYWxQcm9wcyIsIkNvbXBvbmVudCIsImN0eCIsInBhZ2VQcm9wcyIsImxvYWRHZXRJbml0aWFsUHJvcHMiLCJhcHBHZXRJbml0aWFsUHJvcHMiLCJfIiwiQXBwIiwicmVuZGVyIiwicHJvcHMiLCJjcmVhdGVFbGVtZW50IiwiYXNzaWduIiwib3JpZ0dldEluaXRpYWxQcm9wcyIsImdldEluaXRpYWxQcm9wcyIsIlJlYWN0IiwiUHJvdmlkZXIiLCJjcmVhdGVXcmFwcGVyIiwic3RvcmUiLCJNeUFwcCIsIm1ha2VzdG9yZSIsIndyYXBwZXIiLCJ3aXRoUmVkdXgiLCJjb21iaW5lUmVkdWNlcnMiLCJzaG9wcGluZ1JlZHVjZXIiLCJwcm9kdWN0c1JlZHVjZXIiLCJ0eXBlcyIsImluaXRpYWxTdGF0ZSIsInByb2R1Y3RzIiwiY2FydCIsImxvYWRpbmciLCJzdGF0ZSIsImFjdGlvbiIsInR5cGUiLCJHRVRfQUxMIiwicGF5bG9hZCIsInRvdGFsIiwiTE9BRElORyIsIkVSUk9SIiwiQUREX0NBUlQiLCJjb25zb2xlIiwibG9nIiwiaXRlbUluQ2FydCIsImZpbmQiLCJpdGVtIiwiaWQiLCJtYXAiLCJxdWFudGl0eSIsIlJFTU9WRV9PTkVfRlJPTV9DQVJUIiwid2luZG93IiwiY29uZmlybSIsImZpbHRlciIsIml0ZW1zIiwiUkVNT1ZFX0FMTF9GUk9NX0NBUlQiLCJHRVRfVE9UQUwiLCJyZXMiLCJyZWR1Y2UiLCJwcmV2IiwicHJpY2UiLCJhcHBseU1pZGRsZXdhcmUiLCJjcmVhdGVTdG9yZSIsInRodW5rIiwiY29tcG9zZVdpdGhEZXZUb29scyIsInJvb3RSZWR1Y2VyIiwibWlkZGxld2FyZSIsIklOQ1JFU0UiLCJSRURVQ0UiLCJDTEVBUl9DQVJUIl0sInNvdXJjZVJvb3QiOiIifQ==