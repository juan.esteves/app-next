"use strict";
self["webpackHotUpdate_N_E"]("pages/cart",{

/***/ "./components/Cart/index.jsx":
/*!***********************************!*\
  !*** ./components/Cart/index.jsx ***!
  \***********************************/
/***/ (function(module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/link */ "./node_modules/next/link.js");
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_link__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var store_actions__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! store/actions */ "./store/actions/index.js");
/* harmony import */ var react_icons_fa__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react-icons/fa */ "./node_modules/react-icons/fa/index.esm.js");
/* harmony import */ var _styles__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./styles */ "./components/Cart/styles.jsx");
/* harmony import */ var _CartItem__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./CartItem */ "./components/Cart/CartItem.jsx");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_6__);
/* module decorator */ module = __webpack_require__.hmd(module);



var _jsxFileName = "/mnt/d/Developer/Front-End/Next/app/components/Cart/index.jsx",
    _this = undefined,
    _s = $RefreshSig$();









var Cart = function Cart(props) {
  _s();

  var increase = props.increase,
      reduce = props.reduce,
      removeOne = props.removeOne,
      removeAll = props.removeAll,
      data = props.data,
      getTotal = props.getTotal;
  var cart = data.cart,
      total = data.total;
  (0,react__WEBPACK_IMPORTED_MODULE_6__.useEffect)(function () {
    getTotal;
  }, []);
  var wh = cart.map(function (e) {
    return JSON.stringify(e);
  }).toString();

  if (cart.length === 0) {
    return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("h2", {
      style: {
        textAlign: "center"
      },
      children: "Nothings Product"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 23,
      columnNumber: 20
    }, _this);
  } else {
    return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
      children: [cart.map(function (item, index) {
        return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_CartItem__WEBPACK_IMPORTED_MODULE_5__.default, {
          data: item,
          increase: increase,
          reduce: reduce,
          removeOne: removeOne
        }, index, false, {
          fileName: _jsxFileName,
          lineNumber: 29,
          columnNumber: 29
        }, _this);
      }), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("button", {
        onClick: removeAll,
        children: "REMOVER ALL"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 32,
        columnNumber: 21
      }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
        className: "total",
        children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("a", {
          href: "https://api.whatsapp.com/send?phone=573218122180&text=".concat(wh),
          children: [" ", /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("button", {
            children: ["Enviar orden", /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_icons_fa__WEBPACK_IMPORTED_MODULE_7__.FaWhatsapp, {}, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 34,
              columnNumber: 115
            }, _this)]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 34,
            columnNumber: 95
          }, _this), " "]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 34,
          columnNumber: 21
        }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("h3", {
          children: ["Total:$", total, ".00"]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 35,
          columnNumber: 25
        }, _this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 33,
        columnNumber: 21
      }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_styles__WEBPACK_IMPORTED_MODULE_4__.default, {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 38,
        columnNumber: 21
      }, _this)]
    }, void 0, true);
  }
};

_s(Cart, "OD7bBpZva5O2jO+Puf00hKivP7c=");

_c = Cart;

var mapStateToProps = function mapStateToProps(state) {
  return state.productsReducer;
};

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    getProducts: function getProducts(payload) {
      return dispatch(getAll(payload));
    },
    removeOne: function removeOne(payload) {
      return dispatch((0,store_actions__WEBPACK_IMPORTED_MODULE_3__.removeOne)(payload));
    },
    removeAll: function removeAll(payload) {
      return dispatch((0,store_actions__WEBPACK_IMPORTED_MODULE_3__.removeAll)(payload));
    },
    increase: function increase(payload) {
      return dispatch((0,store_actions__WEBPACK_IMPORTED_MODULE_3__.increse)(payload));
    },
    reduce: function reduce(payload) {
      return dispatch((0,store_actions__WEBPACK_IMPORTED_MODULE_3__.reduce)(payload));
    },
    getTotal: function getTotal() {
      return dispatch((0,store_actions__WEBPACK_IMPORTED_MODULE_3__.getTotal)());
    }
  };
};

/* harmony default export */ __webpack_exports__["default"] = ((0,react_redux__WEBPACK_IMPORTED_MODULE_1__.connect)(mapStateToProps, mapDispatchToProps)(Cart));

var _c;

$RefreshReg$(_c, "Cart");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.id);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }


/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RhdGljL3dlYnBhY2svcGFnZXMvY2FydC44MTY0NzcwMGY1NjAzZjRmOTE1Yi5ob3QtdXBkYXRlLmpzIiwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBR0ksSUFBTVcsSUFBSSxHQUFHLFNBQVBBLElBQU8sQ0FBQ0MsS0FBRCxFQUFXO0FBQUE7O0FBQUEsTUFFaEJDLFFBRmdCLEdBRXlDRCxLQUZ6QyxDQUVoQkMsUUFGZ0I7QUFBQSxNQUVQVCxNQUZPLEdBRXlDUSxLQUZ6QyxDQUVQUixNQUZPO0FBQUEsTUFFQUUsU0FGQSxHQUV5Q00sS0FGekMsQ0FFQU4sU0FGQTtBQUFBLE1BRVdELFNBRlgsR0FFeUNPLEtBRnpDLENBRVdQLFNBRlg7QUFBQSxNQUVzQlMsSUFGdEIsR0FFeUNGLEtBRnpDLENBRXNCRSxJQUZ0QjtBQUFBLE1BRTRCWixRQUY1QixHQUV5Q1UsS0FGekMsQ0FFNEJWLFFBRjVCO0FBQUEsTUFHakJhLElBSGlCLEdBR0ZELElBSEUsQ0FHakJDLElBSGlCO0FBQUEsTUFHWEMsS0FIVyxHQUdGRixJQUhFLENBR1hFLEtBSFc7QUFLeEJOLEVBQUFBLGdEQUFTLENBQUMsWUFBTTtBQUNaUixJQUFBQSxRQUFRO0FBQ1gsR0FGUSxFQUVOLEVBRk0sQ0FBVDtBQUtBLE1BQU1lLEVBQUUsR0FBSUYsSUFBSSxDQUFDRyxHQUFMLENBQVMsVUFBQUMsQ0FBQztBQUFBLFdBQUVDLElBQUksQ0FBQ0MsU0FBTCxDQUFlRixDQUFmLENBQUY7QUFBQSxHQUFWLENBQUQsQ0FBaUNHLFFBQWpDLEVBQVg7O0FBRUEsTUFBR1AsSUFBSSxDQUFDUSxNQUFMLEtBQWdCLENBQW5CLEVBQXFCO0FBQ2Isd0JBQU87QUFBSSxXQUFLLEVBQUU7QUFBQ0MsUUFBQUEsU0FBUyxFQUFDO0FBQVgsT0FBWDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxhQUFQO0FBQ0gsR0FGTCxNQUVTO0FBQ0Qsd0JBQ0k7QUFBQSxpQkFFUVQsSUFBSSxDQUFDRyxHQUFMLENBQVMsVUFBQ08sSUFBRCxFQUFPQyxLQUFQO0FBQUEsNEJBQ0wsOERBQUMsOENBQUQ7QUFBc0IsY0FBSSxFQUFFRCxJQUE1QjtBQUFrQyxrQkFBUSxFQUFFWixRQUE1QztBQUFzRCxnQkFBTSxFQUFFVCxNQUE5RDtBQUFzRSxtQkFBUyxFQUFFRTtBQUFqRixXQUFlb0IsS0FBZjtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQURLO0FBQUEsT0FBVCxDQUZSLGVBTUk7QUFBUSxlQUFPLEVBQUVyQixTQUFqQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxlQU5KLGVBT0k7QUFBSyxpQkFBUyxFQUFDLE9BQWY7QUFBQSxnQ0FDQTtBQUFJLGNBQUksa0VBQTJEWSxFQUEzRCxDQUFSO0FBQUEsdUNBQTBFO0FBQUEsb0RBQW9CLDhEQUFDLHNEQUFEO0FBQUE7QUFBQTtBQUFBO0FBQUEscUJBQXBCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkFBMUU7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQURBLGVBRUk7QUFBQSxnQ0FBWUQsS0FBWjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBRko7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGVBUEosZUFZSSw4REFBQyw0Q0FBRDtBQUFBO0FBQUE7QUFBQTtBQUFBLGVBWko7QUFBQSxvQkFESjtBQWdCQztBQUNKLENBaENMOztHQUFNTDs7S0FBQUE7O0FBb0NWLElBQU1nQixlQUFlLEdBQUcsU0FBbEJBLGVBQWtCLENBQUNDLEtBQUQsRUFBVztBQUMvQixTQUFPQSxLQUFLLENBQUNDLGVBQWI7QUFDRSxDQUZOOztBQUlFLElBQU1DLGtCQUFrQixHQUFHLFNBQXJCQSxrQkFBcUIsQ0FBQ0MsUUFBRCxFQUFjO0FBQ3ZDLFNBQU87QUFDTEMsSUFBQUEsV0FBVyxFQUFFLHFCQUFDQyxPQUFEO0FBQUEsYUFBYUYsUUFBUSxDQUFDRyxNQUFNLENBQUNELE9BQUQsQ0FBUCxDQUFyQjtBQUFBLEtBRFI7QUFFTDNCLElBQUFBLFNBQVMsRUFBRyxtQkFBQzJCLE9BQUQ7QUFBQSxhQUFhRixRQUFRLENBQUN6Qix3REFBUyxDQUFDMkIsT0FBRCxDQUFWLENBQXJCO0FBQUEsS0FGUDtBQUdMNUIsSUFBQUEsU0FBUyxFQUFHLG1CQUFDNEIsT0FBRDtBQUFBLGFBQWFGLFFBQVEsQ0FBQzFCLHdEQUFTLENBQUM0QixPQUFELENBQVYsQ0FBckI7QUFBQSxLQUhQO0FBSUxwQixJQUFBQSxRQUFRLEVBQUcsa0JBQUNvQixPQUFEO0FBQUEsYUFBYUYsUUFBUSxDQUFDNUIsc0RBQU8sQ0FBQzhCLE9BQUQsQ0FBUixDQUFyQjtBQUFBLEtBSk47QUFLTDdCLElBQUFBLE1BQU0sRUFBRyxnQkFBQzZCLE9BQUQ7QUFBQSxhQUFhRixRQUFRLENBQUMzQixxREFBTSxDQUFDNkIsT0FBRCxDQUFQLENBQXJCO0FBQUEsS0FMSjtBQU1ML0IsSUFBQUEsUUFBUSxFQUFFO0FBQUEsYUFBTTZCLFFBQVEsQ0FBQzdCLHVEQUFRLEVBQVQsQ0FBZDtBQUFBO0FBTkwsR0FBUDtBQVFELENBVEQ7O0FBV0YsK0RBQWVGLG9EQUFPLENBQUMyQixlQUFELEVBQWtCRyxrQkFBbEIsQ0FBUCxDQUE2Q25CLElBQTdDLENBQWYiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vY29tcG9uZW50cy9DYXJ0L2luZGV4LmpzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge2Nvbm5lY3R9IGZyb20gJ3JlYWN0LXJlZHV4J1xyXG5pbXBvcnQgTGluayBmcm9tICduZXh0L2xpbmsnXHJcbmltcG9ydCB7IGdldFRvdGFsLCBpbmNyZXNlLCByZWR1Y2UsIHJlbW92ZUFsbCwgcmVtb3ZlT25lIH0gZnJvbSAnc3RvcmUvYWN0aW9ucydcclxuaW1wb3J0IHtGYVdoYXRzYXBwfSBmcm9tICdyZWFjdC1pY29ucy9mYSdcclxuaW1wb3J0IFN0eWxlcyBmcm9tICcuL3N0eWxlcyc7XHJcbmltcG9ydCBDYXJ0SXRlbSBmcm9tICcuL0NhcnRJdGVtJztcclxuaW1wb3J0IHsgdXNlRWZmZWN0IH0gZnJvbSAncmVhY3QnO1xyXG5cclxuXHJcbiAgICBjb25zdCBDYXJ0ID0gKHByb3BzKSA9PiB7XHJcbiAgICAgICBcclxuICAgIGNvbnN0IHsgaW5jcmVhc2UscmVkdWNlLHJlbW92ZU9uZSwgcmVtb3ZlQWxsLCBkYXRhLCBnZXRUb3RhbCB9ID0gcHJvcHM7XHJcbiAgICBjb25zdCB7Y2FydCwgdG90YWx9ID0gZGF0YTtcclxuICAgIFxyXG4gICAgdXNlRWZmZWN0KCgpID0+IHtcclxuICAgICAgICBnZXRUb3RhbDtcclxuICAgIH0sIFtdKVxyXG5cclxuXHJcbiAgICBjb25zdCB3aCA9IChjYXJ0Lm1hcChlPT5KU09OLnN0cmluZ2lmeShlKSkpLnRvU3RyaW5nKCkgXHJcbiAgIFxyXG4gICAgaWYoY2FydC5sZW5ndGggPT09IDApe1xyXG4gICAgICAgICAgICByZXR1cm4gPGgyIHN0eWxlPXt7dGV4dEFsaWduOlwiY2VudGVyXCJ9fT5Ob3RoaW5ncyBQcm9kdWN0PC9oMj5cclxuICAgICAgICB9ZWxzZXtcclxuICAgICAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgICAgIDw+XHJcbiAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjYXJ0Lm1hcCgoaXRlbSwgaW5kZXgpID0+KFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPENhcnRJdGVtIGtleT17aW5kZXh9IGRhdGE9e2l0ZW19IGluY3JlYXNlPXtpbmNyZWFzZX0gcmVkdWNlPXtyZWR1Y2V9IHJlbW92ZU9uZT17cmVtb3ZlT25lfS8+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICkpXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIDxidXR0b24gb25DbGljaz17cmVtb3ZlQWxsfT5SRU1PVkVSIEFMTDwvYnV0dG9uPlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwidG90YWxcIj5cclxuICAgICAgICAgICAgICAgICAgICA8YSAgaHJlZj17YGh0dHBzOi8vYXBpLndoYXRzYXBwLmNvbS9zZW5kP3Bob25lPTU3MzIxODEyMjE4MCZ0ZXh0PSR7d2h9YH0+IDxidXR0b24+RW52aWFyIG9yZGVuPEZhV2hhdHNhcHAvPjwvYnV0dG9uPiA8L2E+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxoMz5Ub3RhbDoke3RvdGFsfS4wMDwvaDM+IFxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG5cclxuICAgICAgICAgICAgICAgICAgICA8U3R5bGVzLz5cclxuICAgICAgICAgICAgICAgIDwvPlxyXG4gICAgICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgXHJcblxyXG5cclxuY29uc3QgbWFwU3RhdGVUb1Byb3BzID0gKHN0YXRlKSA9PiB7XHJcbiAgICByZXR1cm4gc3RhdGUucHJvZHVjdHNSZWR1Y2VyXHJcbiAgICAgfVxyXG4gIFxyXG4gIGNvbnN0IG1hcERpc3BhdGNoVG9Qcm9wcyA9IChkaXNwYXRjaCkgPT4ge1xyXG4gICAgcmV0dXJuIHtcclxuICAgICAgZ2V0UHJvZHVjdHM6IChwYXlsb2FkKSA9PiBkaXNwYXRjaChnZXRBbGwocGF5bG9hZCkpLFxyXG4gICAgICByZW1vdmVPbmU6ICAocGF5bG9hZCkgPT4gZGlzcGF0Y2gocmVtb3ZlT25lKHBheWxvYWQpKSxcclxuICAgICAgcmVtb3ZlQWxsOiAgKHBheWxvYWQpID0+IGRpc3BhdGNoKHJlbW92ZUFsbChwYXlsb2FkKSksXHJcbiAgICAgIGluY3JlYXNlOiAgKHBheWxvYWQpID0+IGRpc3BhdGNoKGluY3Jlc2UocGF5bG9hZCkpLFxyXG4gICAgICByZWR1Y2U6ICAocGF5bG9hZCkgPT4gZGlzcGF0Y2gocmVkdWNlKHBheWxvYWQpKSxcclxuICAgICAgZ2V0VG90YWw6ICgpID0+IGRpc3BhdGNoKGdldFRvdGFsKCkpXHJcbiAgICB9XHJcbiAgfVxyXG4gIFxyXG5leHBvcnQgZGVmYXVsdCBjb25uZWN0KG1hcFN0YXRlVG9Qcm9wcywgbWFwRGlzcGF0Y2hUb1Byb3BzKShDYXJ0KVxyXG5cclxuIl0sIm5hbWVzIjpbImNvbm5lY3QiLCJMaW5rIiwiZ2V0VG90YWwiLCJpbmNyZXNlIiwicmVkdWNlIiwicmVtb3ZlQWxsIiwicmVtb3ZlT25lIiwiRmFXaGF0c2FwcCIsIlN0eWxlcyIsIkNhcnRJdGVtIiwidXNlRWZmZWN0IiwiQ2FydCIsInByb3BzIiwiaW5jcmVhc2UiLCJkYXRhIiwiY2FydCIsInRvdGFsIiwid2giLCJtYXAiLCJlIiwiSlNPTiIsInN0cmluZ2lmeSIsInRvU3RyaW5nIiwibGVuZ3RoIiwidGV4dEFsaWduIiwiaXRlbSIsImluZGV4IiwibWFwU3RhdGVUb1Byb3BzIiwic3RhdGUiLCJwcm9kdWN0c1JlZHVjZXIiLCJtYXBEaXNwYXRjaFRvUHJvcHMiLCJkaXNwYXRjaCIsImdldFByb2R1Y3RzIiwicGF5bG9hZCIsImdldEFsbCJdLCJzb3VyY2VSb290IjoiIn0=