"use strict";
self["webpackHotUpdate_N_E"]("pages/cart",{

/***/ "./components/Cart/index.jsx":
/*!***********************************!*\
  !*** ./components/Cart/index.jsx ***!
  \***********************************/
/***/ (function(module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/link */ "./node_modules/next/link.js");
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_link__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var store_actions__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! store/actions */ "./store/actions/index.js");
/* harmony import */ var react_icons_fa__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react-icons/fa */ "./node_modules/react-icons/fa/index.esm.js");
/* harmony import */ var _styles__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./styles */ "./components/Cart/styles.jsx");
/* harmony import */ var _CartItem__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./CartItem */ "./components/Cart/CartItem.jsx");
/* module decorator */ module = __webpack_require__.hmd(module);



var _jsxFileName = "/mnt/d/Developer/Front-End/Next/app/components/Cart/index.jsx",
    _this = undefined,
    _s = $RefreshSig$();








var Cart = function Cart(props) {
  _s();

  var increase = props.increase,
      reduce = props.reduce,
      removeOne = props.removeOne,
      removeAll = props.removeAll,
      data = props.data;
  var cart = data.cart,
      total = data.total;
  useEffect(function () {
    (0,store_actions__WEBPACK_IMPORTED_MODULE_3__.getTotal)();
  }, []);
  var wh = cart.map(function (e) {
    return JSON.stringify(e);
  }).toString();

  if (cart.length === 0) {
    return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("h2", {
      style: {
        textAlign: "center"
      },
      children: "Nothings Product"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 22,
      columnNumber: 20
    }, _this);
  } else {
    return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
      children: [cart.map(function (item, index) {
        return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_CartItem__WEBPACK_IMPORTED_MODULE_5__.default, {
          data: item,
          increase: increase,
          reduce: reduce,
          removeOne: removeOne
        }, index, false, {
          fileName: _jsxFileName,
          lineNumber: 28,
          columnNumber: 29
        }, _this);
      }), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("button", {
        onClick: removeAll,
        children: "REMOVER ALL"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 31,
        columnNumber: 21
      }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
        className: "total",
        children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("a", {
          href: "https://api.whatsapp.com/send?phone=573218122180&text=".concat(wh),
          children: [" ", /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("button", {
            children: ["Enviar orden", /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_icons_fa__WEBPACK_IMPORTED_MODULE_6__.FaWhatsapp, {}, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 33,
              columnNumber: 115
            }, _this)]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 33,
            columnNumber: 95
          }, _this), " "]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 33,
          columnNumber: 21
        }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("h3", {
          children: ["Total:$", total, ".00"]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 34,
          columnNumber: 25
        }, _this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 32,
        columnNumber: 21
      }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_styles__WEBPACK_IMPORTED_MODULE_4__.default, {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 37,
        columnNumber: 21
      }, _this)]
    }, void 0, true);
  }
};

_s(Cart, "OD7bBpZva5O2jO+Puf00hKivP7c=");

_c = Cart;

var mapStateToProps = function mapStateToProps(state) {
  return state.productsReducer;
};

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    getProducts: function getProducts(payload) {
      return dispatch(getAll(payload));
    },
    removeOne: function removeOne(payload) {
      return dispatch((0,store_actions__WEBPACK_IMPORTED_MODULE_3__.removeOne)(payload));
    },
    removeAll: function removeAll(payload) {
      return dispatch((0,store_actions__WEBPACK_IMPORTED_MODULE_3__.removeAll)(payload));
    },
    increase: function increase(payload) {
      return dispatch((0,store_actions__WEBPACK_IMPORTED_MODULE_3__.increse)(payload));
    },
    reduce: function reduce(payload) {
      return dispatch((0,store_actions__WEBPACK_IMPORTED_MODULE_3__.reduce)(payload));
    },
    getTotal: function getTotal() {
      return dispatch((0,store_actions__WEBPACK_IMPORTED_MODULE_3__.getTotal)());
    }
  };
};

/* harmony default export */ __webpack_exports__["default"] = ((0,react_redux__WEBPACK_IMPORTED_MODULE_1__.connect)(mapStateToProps, mapDispatchToProps)(Cart));

var _c;

$RefreshReg$(_c, "Cart");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.id);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }


/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RhdGljL3dlYnBhY2svcGFnZXMvY2FydC5hZTkwODYwY2RjMTdkNzk2NzAxNS5ob3QtdXBkYXRlLmpzIiwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFHSSxJQUFNVSxJQUFJLEdBQUcsU0FBUEEsSUFBTyxDQUFDQyxLQUFELEVBQVc7QUFBQTs7QUFBQSxNQUVoQkMsUUFGZ0IsR0FFK0JELEtBRi9CLENBRWhCQyxRQUZnQjtBQUFBLE1BRVBSLE1BRk8sR0FFK0JPLEtBRi9CLENBRVBQLE1BRk87QUFBQSxNQUVBRSxTQUZBLEdBRStCSyxLQUYvQixDQUVBTCxTQUZBO0FBQUEsTUFFV0QsU0FGWCxHQUUrQk0sS0FGL0IsQ0FFV04sU0FGWDtBQUFBLE1BRXNCUSxJQUZ0QixHQUUrQkYsS0FGL0IsQ0FFc0JFLElBRnRCO0FBQUEsTUFHakJDLElBSGlCLEdBR0ZELElBSEUsQ0FHakJDLElBSGlCO0FBQUEsTUFHWEMsS0FIVyxHQUdGRixJQUhFLENBR1hFLEtBSFc7QUFLeEJDLEVBQUFBLFNBQVMsQ0FBQyxZQUFNO0FBQ1pkLElBQUFBLHVEQUFRO0FBQ1gsR0FGUSxFQUVOLEVBRk0sQ0FBVDtBQUtBLE1BQU1lLEVBQUUsR0FBSUgsSUFBSSxDQUFDSSxHQUFMLENBQVMsVUFBQUMsQ0FBQztBQUFBLFdBQUVDLElBQUksQ0FBQ0MsU0FBTCxDQUFlRixDQUFmLENBQUY7QUFBQSxHQUFWLENBQUQsQ0FBaUNHLFFBQWpDLEVBQVg7O0FBRUEsTUFBR1IsSUFBSSxDQUFDUyxNQUFMLEtBQWdCLENBQW5CLEVBQXFCO0FBQ2Isd0JBQU87QUFBSSxXQUFLLEVBQUU7QUFBQ0MsUUFBQUEsU0FBUyxFQUFDO0FBQVgsT0FBWDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxhQUFQO0FBQ0gsR0FGTCxNQUVTO0FBQ0Qsd0JBQ0k7QUFBQSxpQkFFUVYsSUFBSSxDQUFDSSxHQUFMLENBQVMsVUFBQ08sSUFBRCxFQUFPQyxLQUFQO0FBQUEsNEJBQ0wsOERBQUMsOENBQUQ7QUFBc0IsY0FBSSxFQUFFRCxJQUE1QjtBQUFrQyxrQkFBUSxFQUFFYixRQUE1QztBQUFzRCxnQkFBTSxFQUFFUixNQUE5RDtBQUFzRSxtQkFBUyxFQUFFRTtBQUFqRixXQUFlb0IsS0FBZjtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQURLO0FBQUEsT0FBVCxDQUZSLGVBTUk7QUFBUSxlQUFPLEVBQUVyQixTQUFqQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxlQU5KLGVBT0k7QUFBSyxpQkFBUyxFQUFDLE9BQWY7QUFBQSxnQ0FDQTtBQUFJLGNBQUksa0VBQTJEWSxFQUEzRCxDQUFSO0FBQUEsdUNBQTBFO0FBQUEsb0RBQW9CLDhEQUFDLHNEQUFEO0FBQUE7QUFBQTtBQUFBO0FBQUEscUJBQXBCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkFBMUU7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQURBLGVBRUk7QUFBQSxnQ0FBWUYsS0FBWjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBRko7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGVBUEosZUFZSSw4REFBQyw0Q0FBRDtBQUFBO0FBQUE7QUFBQTtBQUFBLGVBWko7QUFBQSxvQkFESjtBQWdCQztBQUNKLENBaENMOztHQUFNTDs7S0FBQUE7O0FBb0NWLElBQU1pQixlQUFlLEdBQUcsU0FBbEJBLGVBQWtCLENBQUNDLEtBQUQsRUFBVztBQUMvQixTQUFPQSxLQUFLLENBQUNDLGVBQWI7QUFDRSxDQUZOOztBQUlFLElBQU1DLGtCQUFrQixHQUFHLFNBQXJCQSxrQkFBcUIsQ0FBQ0MsUUFBRCxFQUFjO0FBQ3ZDLFNBQU87QUFDTEMsSUFBQUEsV0FBVyxFQUFFLHFCQUFDQyxPQUFEO0FBQUEsYUFBYUYsUUFBUSxDQUFDRyxNQUFNLENBQUNELE9BQUQsQ0FBUCxDQUFyQjtBQUFBLEtBRFI7QUFFTDNCLElBQUFBLFNBQVMsRUFBRyxtQkFBQzJCLE9BQUQ7QUFBQSxhQUFhRixRQUFRLENBQUN6Qix3REFBUyxDQUFDMkIsT0FBRCxDQUFWLENBQXJCO0FBQUEsS0FGUDtBQUdMNUIsSUFBQUEsU0FBUyxFQUFHLG1CQUFDNEIsT0FBRDtBQUFBLGFBQWFGLFFBQVEsQ0FBQzFCLHdEQUFTLENBQUM0QixPQUFELENBQVYsQ0FBckI7QUFBQSxLQUhQO0FBSUxyQixJQUFBQSxRQUFRLEVBQUcsa0JBQUNxQixPQUFEO0FBQUEsYUFBYUYsUUFBUSxDQUFDNUIsc0RBQU8sQ0FBQzhCLE9BQUQsQ0FBUixDQUFyQjtBQUFBLEtBSk47QUFLTDdCLElBQUFBLE1BQU0sRUFBRyxnQkFBQzZCLE9BQUQ7QUFBQSxhQUFhRixRQUFRLENBQUMzQixxREFBTSxDQUFDNkIsT0FBRCxDQUFQLENBQXJCO0FBQUEsS0FMSjtBQU1ML0IsSUFBQUEsUUFBUSxFQUFFO0FBQUEsYUFBTTZCLFFBQVEsQ0FBQzdCLHVEQUFRLEVBQVQsQ0FBZDtBQUFBO0FBTkwsR0FBUDtBQVFELENBVEQ7O0FBV0YsK0RBQWVGLG9EQUFPLENBQUMyQixlQUFELEVBQWtCRyxrQkFBbEIsQ0FBUCxDQUE2Q3BCLElBQTdDLENBQWYiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vY29tcG9uZW50cy9DYXJ0L2luZGV4LmpzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge2Nvbm5lY3R9IGZyb20gJ3JlYWN0LXJlZHV4J1xyXG5pbXBvcnQgTGluayBmcm9tICduZXh0L2xpbmsnXHJcbmltcG9ydCB7IGdldFRvdGFsLCBpbmNyZXNlLCByZWR1Y2UsIHJlbW92ZUFsbCwgcmVtb3ZlT25lIH0gZnJvbSAnc3RvcmUvYWN0aW9ucydcclxuaW1wb3J0IHtGYVdoYXRzYXBwfSBmcm9tICdyZWFjdC1pY29ucy9mYSdcclxuaW1wb3J0IFN0eWxlcyBmcm9tICcuL3N0eWxlcyc7XHJcbmltcG9ydCBDYXJ0SXRlbSBmcm9tICcuL0NhcnRJdGVtJztcclxuXHJcblxyXG4gICAgY29uc3QgQ2FydCA9IChwcm9wcykgPT4ge1xyXG4gICAgICAgXHJcbiAgICBjb25zdCB7IGluY3JlYXNlLHJlZHVjZSxyZW1vdmVPbmUsIHJlbW92ZUFsbCwgZGF0YSB9ID0gcHJvcHM7XHJcbiAgICBjb25zdCB7Y2FydCwgdG90YWx9ID0gZGF0YTtcclxuICAgIFxyXG4gICAgdXNlRWZmZWN0KCgpID0+IHtcclxuICAgICAgICBnZXRUb3RhbCgpO1xyXG4gICAgfSwgW10pXHJcblxyXG5cclxuICAgIGNvbnN0IHdoID0gKGNhcnQubWFwKGU9PkpTT04uc3RyaW5naWZ5KGUpKSkudG9TdHJpbmcoKSBcclxuICAgXHJcbiAgICBpZihjYXJ0Lmxlbmd0aCA9PT0gMCl7XHJcbiAgICAgICAgICAgIHJldHVybiA8aDIgc3R5bGU9e3t0ZXh0QWxpZ246XCJjZW50ZXJcIn19Pk5vdGhpbmdzIFByb2R1Y3Q8L2gyPlxyXG4gICAgICAgIH1lbHNle1xyXG4gICAgICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICAgICAgPD5cclxuICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNhcnQubWFwKChpdGVtLCBpbmRleCkgPT4oXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8Q2FydEl0ZW0ga2V5PXtpbmRleH0gZGF0YT17aXRlbX0gaW5jcmVhc2U9e2luY3JlYXNlfSByZWR1Y2U9e3JlZHVjZX0gcmVtb3ZlT25lPXtyZW1vdmVPbmV9Lz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgKSlcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgPGJ1dHRvbiBvbkNsaWNrPXtyZW1vdmVBbGx9PlJFTU9WRVIgQUxMPC9idXR0b24+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJ0b3RhbFwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxhICBocmVmPXtgaHR0cHM6Ly9hcGkud2hhdHNhcHAuY29tL3NlbmQ/cGhvbmU9NTczMjE4MTIyMTgwJnRleHQ9JHt3aH1gfT4gPGJ1dHRvbj5FbnZpYXIgb3JkZW48RmFXaGF0c2FwcC8+PC9idXR0b24+IDwvYT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGgzPlRvdGFsOiR7dG90YWx9LjAwPC9oMz4gXHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIDxTdHlsZXMvPlxyXG4gICAgICAgICAgICAgICAgPC8+XHJcbiAgICAgICAgICAgICAgICApXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICBcclxuXHJcblxyXG5jb25zdCBtYXBTdGF0ZVRvUHJvcHMgPSAoc3RhdGUpID0+IHtcclxuICAgIHJldHVybiBzdGF0ZS5wcm9kdWN0c1JlZHVjZXJcclxuICAgICB9XHJcbiAgXHJcbiAgY29uc3QgbWFwRGlzcGF0Y2hUb1Byb3BzID0gKGRpc3BhdGNoKSA9PiB7XHJcbiAgICByZXR1cm4ge1xyXG4gICAgICBnZXRQcm9kdWN0czogKHBheWxvYWQpID0+IGRpc3BhdGNoKGdldEFsbChwYXlsb2FkKSksXHJcbiAgICAgIHJlbW92ZU9uZTogIChwYXlsb2FkKSA9PiBkaXNwYXRjaChyZW1vdmVPbmUocGF5bG9hZCkpLFxyXG4gICAgICByZW1vdmVBbGw6ICAocGF5bG9hZCkgPT4gZGlzcGF0Y2gocmVtb3ZlQWxsKHBheWxvYWQpKSxcclxuICAgICAgaW5jcmVhc2U6ICAocGF5bG9hZCkgPT4gZGlzcGF0Y2goaW5jcmVzZShwYXlsb2FkKSksXHJcbiAgICAgIHJlZHVjZTogIChwYXlsb2FkKSA9PiBkaXNwYXRjaChyZWR1Y2UocGF5bG9hZCkpLFxyXG4gICAgICBnZXRUb3RhbDogKCkgPT4gZGlzcGF0Y2goZ2V0VG90YWwoKSlcclxuICAgIH1cclxuICB9XHJcbiAgXHJcbmV4cG9ydCBkZWZhdWx0IGNvbm5lY3QobWFwU3RhdGVUb1Byb3BzLCBtYXBEaXNwYXRjaFRvUHJvcHMpKENhcnQpXHJcblxyXG4iXSwibmFtZXMiOlsiY29ubmVjdCIsIkxpbmsiLCJnZXRUb3RhbCIsImluY3Jlc2UiLCJyZWR1Y2UiLCJyZW1vdmVBbGwiLCJyZW1vdmVPbmUiLCJGYVdoYXRzYXBwIiwiU3R5bGVzIiwiQ2FydEl0ZW0iLCJDYXJ0IiwicHJvcHMiLCJpbmNyZWFzZSIsImRhdGEiLCJjYXJ0IiwidG90YWwiLCJ1c2VFZmZlY3QiLCJ3aCIsIm1hcCIsImUiLCJKU09OIiwic3RyaW5naWZ5IiwidG9TdHJpbmciLCJsZW5ndGgiLCJ0ZXh0QWxpZ24iLCJpdGVtIiwiaW5kZXgiLCJtYXBTdGF0ZVRvUHJvcHMiLCJzdGF0ZSIsInByb2R1Y3RzUmVkdWNlciIsIm1hcERpc3BhdGNoVG9Qcm9wcyIsImRpc3BhdGNoIiwiZ2V0UHJvZHVjdHMiLCJwYXlsb2FkIiwiZ2V0QWxsIl0sInNvdXJjZVJvb3QiOiIifQ==