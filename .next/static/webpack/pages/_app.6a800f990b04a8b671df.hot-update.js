"use strict";
self["webpackHotUpdate_N_E"]("pages/_app",{

/***/ "./store/reducers/productsReducer.js":
/*!*******************************************!*\
  !*** ./store/reducers/productsReducer.js ***!
  \*******************************************/
/***/ (function(module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _mnt_d_Developer_Front_End_Next_app_node_modules_next_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/next/node_modules/@babel/runtime/helpers/esm/defineProperty */ "./node_modules/next/node_modules/@babel/runtime/helpers/esm/defineProperty.js");
/* harmony import */ var _types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../types */ "./store/types.js");
/* module decorator */ module = __webpack_require__.hmd(module);


function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { (0,_mnt_d_Developer_Front_End_Next_app_node_modules_next_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__.default)(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }


var initialState = {
  products: [],
  cart: [],
  loading: false,
  error: ''
};

var productsReducer = function productsReducer() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : initialState;
  var action = arguments.length > 1 ? arguments[1] : undefined;

  switch (action.type) {
    case _types__WEBPACK_IMPORTED_MODULE_1__.GET_ALL:
      return _objectSpread(_objectSpread({}, state), {}, {
        products: action.payload,
        loading: false,
        error: '',
        global: 0
      });

    case _types__WEBPACK_IMPORTED_MODULE_1__.LOADING:
      return _objectSpread(_objectSpread({}, state), {}, {
        loading: true
      });

    case _types__WEBPACK_IMPORTED_MODULE_1__.ERROR:
      return _objectSpread(_objectSpread({}, state), {}, {
        error: action.payload,
        loading: false
      });

    default:
      return state;
  }
};

/* harmony default export */ __webpack_exports__["default"] = (productsReducer);

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.id);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }


/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RhdGljL3dlYnBhY2svcGFnZXMvX2FwcC42YTgwMGY5OTBiMDRhOGI2NzFkZi5ob3QtdXBkYXRlLmpzIiwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUdBLElBQU1DLFlBQVksR0FBRTtBQUNoQkMsRUFBQUEsUUFBUSxFQUFFLEVBRE07QUFFaEJDLEVBQUFBLElBQUksRUFBRSxFQUZVO0FBR2hCQyxFQUFBQSxPQUFPLEVBQUUsS0FITztBQUloQkMsRUFBQUEsS0FBSyxFQUFDO0FBSlUsQ0FBcEI7O0FBUUEsSUFBTUMsZUFBZSxHQUFHLFNBQWxCQSxlQUFrQixHQUFrQztBQUFBLE1BQWpDQyxLQUFpQyx1RUFBekJOLFlBQXlCO0FBQUEsTUFBWE8sTUFBVzs7QUFDdEQsVUFBUUEsTUFBTSxDQUFDQyxJQUFmO0FBQ0ksU0FBS1QsMkNBQUw7QUFDSSw2Q0FDT08sS0FEUDtBQUVJTCxRQUFBQSxRQUFRLEVBQUVNLE1BQU0sQ0FBQ0csT0FGckI7QUFHSVAsUUFBQUEsT0FBTyxFQUFFLEtBSGI7QUFJSUMsUUFBQUEsS0FBSyxFQUFFLEVBSlg7QUFLSU8sUUFBQUEsTUFBTSxFQUFDO0FBTFg7O0FBUUosU0FBS1osMkNBQUw7QUFDSSw2Q0FBV08sS0FBWDtBQUFrQkgsUUFBQUEsT0FBTyxFQUFFO0FBQTNCOztBQUVKLFNBQUtKLHlDQUFMO0FBQ1ksNkNBQVdPLEtBQVg7QUFBa0JGLFFBQUFBLEtBQUssRUFBRUcsTUFBTSxDQUFDRyxPQUFoQztBQUF5Q1AsUUFBQUEsT0FBTyxFQUFFO0FBQWxEOztBQUVaO0FBQ0ksYUFBT0csS0FBUDtBQWpCUjtBQW1CSCxDQXBCRDs7QUFzQkEsK0RBQWVELGVBQWYiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vc3RvcmUvcmVkdWNlcnMvcHJvZHVjdHNSZWR1Y2VyLmpzIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCAqIGFzIHR5cGVzIGZyb20gJy4uL3R5cGVzJ1xyXG5cclxuXHJcbmNvbnN0IGluaXRpYWxTdGF0ZT0ge1xyXG4gICAgcHJvZHVjdHM6IFtdLFxyXG4gICAgY2FydDogW10sXHJcbiAgICBsb2FkaW5nOiBmYWxzZSxcclxuICAgIGVycm9yOicnXHJcbn1cclxuXHJcblxyXG5jb25zdCBwcm9kdWN0c1JlZHVjZXIgPSAoc3RhdGUgPSBpbml0aWFsU3RhdGUsIGFjdGlvbikgPT4ge1xyXG4gICAgc3dpdGNoIChhY3Rpb24udHlwZSkge1xyXG4gICAgICAgIGNhc2UgdHlwZXMuR0VUX0FMTDpcclxuICAgICAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgICAgIC4uLnN0YXRlLFxyXG4gICAgICAgICAgICAgICAgcHJvZHVjdHM6IGFjdGlvbi5wYXlsb2FkLFxyXG4gICAgICAgICAgICAgICAgbG9hZGluZzogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICBlcnJvcjogJycsXHJcbiAgICAgICAgICAgICAgICBnbG9iYWw6MFxyXG4gICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICBjYXNlIHR5cGVzLkxPQURJTkc6XHJcbiAgICAgICAgICAgIHJldHVybiB7Li4uc3RhdGUsIGxvYWRpbmc6IHRydWV9O1xyXG5cclxuICAgICAgICBjYXNlIHR5cGVzLkVSUk9SOlxyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiB7Li4uc3RhdGUsIGVycm9yOiBhY3Rpb24ucGF5bG9hZCwgbG9hZGluZzogZmFsc2V9O1xyXG4gICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgIGRlZmF1bHQ6XHJcbiAgICAgICAgICAgIHJldHVybiBzdGF0ZVxyXG4gICAgfVxyXG59XHJcbiAgICAgICAgICAgIFxyXG5leHBvcnQgZGVmYXVsdCBwcm9kdWN0c1JlZHVjZXI7Il0sIm5hbWVzIjpbInR5cGVzIiwiaW5pdGlhbFN0YXRlIiwicHJvZHVjdHMiLCJjYXJ0IiwibG9hZGluZyIsImVycm9yIiwicHJvZHVjdHNSZWR1Y2VyIiwic3RhdGUiLCJhY3Rpb24iLCJ0eXBlIiwiR0VUX0FMTCIsInBheWxvYWQiLCJnbG9iYWwiLCJMT0FESU5HIiwiRVJST1IiXSwic291cmNlUm9vdCI6IiJ9