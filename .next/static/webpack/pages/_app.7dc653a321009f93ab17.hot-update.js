"use strict";
self["webpackHotUpdate_N_E"]("pages/_app",{

/***/ "./store/reducers/shoppingReducer.js":
/*!*******************************************!*\
  !*** ./store/reducers/shoppingReducer.js ***!
  \*******************************************/
/***/ (function(module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _mnt_d_Developer_Front_End_Next_app_node_modules_next_node_modules_babel_runtime_helpers_esm_toConsumableArray__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/next/node_modules/@babel/runtime/helpers/esm/toConsumableArray */ "./node_modules/next/node_modules/@babel/runtime/helpers/esm/toConsumableArray.js");
/* harmony import */ var _mnt_d_Developer_Front_End_Next_app_node_modules_next_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/next/node_modules/@babel/runtime/helpers/esm/defineProperty */ "./node_modules/next/node_modules/@babel/runtime/helpers/esm/defineProperty.js");
/* harmony import */ var _types__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../types */ "./store/types.js");
/* module decorator */ module = __webpack_require__.hmd(module);



function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { (0,_mnt_d_Developer_Front_End_Next_app_node_modules_next_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_1__.default)(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }


var initialState = {
  products: [],
  cart: [],
  loading: false,
  error: '',
  total: 0
};

var shoppingReducer = function shoppingReducer() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : initialState;
  var action = arguments.length > 1 ? arguments[1] : undefined;

  switch (action.type) {
    case _types__WEBPACK_IMPORTED_MODULE_2__.ADD_CART:
      console.log(action);
      var itemInCart = state.cart.find(function (item) {
        return item.id === action.payload.id;
      });
      return itemInCart ? _objectSpread(_objectSpread({}, state), {}, {
        cart: state.cart.map(function (item) {
          return item.id === action.payload.id ? _objectSpread(_objectSpread({}, item), {}, {
            quantity: item.quantity + 1
          }) : item;
        })
      }) : _objectSpread(_objectSpread({}, state), {}, {
        cart: [].concat((0,_mnt_d_Developer_Front_End_Next_app_node_modules_next_node_modules_babel_runtime_helpers_esm_toConsumableArray__WEBPACK_IMPORTED_MODULE_0__.default)(state.cart), [_objectSpread(_objectSpread({}, action.payload), {}, {
          quantity: 1
        })])
      });

    case _types__WEBPACK_IMPORTED_MODULE_2__.REMOVE_ONE_FROM_CART:
      if (window.confirm("Do you want to delete this product?")) {
        return _objectSpread(_objectSpread({}, state), {}, {
          cart: state.cart.filter(function (items) {
            return items.id !== action.payload;
          })
        });
      }

    case _types__WEBPACK_IMPORTED_MODULE_2__.REMOVE_ALL_FROM_CART:
      if (window.confirm("Do you want to delete all products?")) {
        return _objectSpread(_objectSpread({}, state), {}, {
          cart: []
        });
      }

    case _types__WEBPACK_IMPORTED_MODULE_2__.GET_TOTAL:
      var res = state.cart.reduce(function (prev, item) {
        return prev + item.price * item.count;
      }, 0);
      return _objectSpread(_objectSpread({}, state), {}, {
        total: res
      });

    default:
      return state;
  }
};

/* harmony default export */ __webpack_exports__["default"] = (shoppingReducer);

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.id);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }


/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RhdGljL3dlYnBhY2svcGFnZXMvX2FwcC43ZGM2NTNhMzIxMDA5ZjkzYWIxNy5ob3QtdXBkYXRlLmpzIiwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBR0EsSUFBTUMsWUFBWSxHQUFFO0FBQ2hCQyxFQUFBQSxRQUFRLEVBQUUsRUFETTtBQUVoQkMsRUFBQUEsSUFBSSxFQUFFLEVBRlU7QUFHaEJDLEVBQUFBLE9BQU8sRUFBRSxLQUhPO0FBSWhCQyxFQUFBQSxLQUFLLEVBQUMsRUFKVTtBQUtoQkMsRUFBQUEsS0FBSyxFQUFDO0FBTFUsQ0FBcEI7O0FBV0EsSUFBTUMsZUFBZSxHQUFHLFNBQWxCQSxlQUFrQixHQUFrQztBQUFBLE1BQWpDQyxLQUFpQyx1RUFBekJQLFlBQXlCO0FBQUEsTUFBWFEsTUFBVzs7QUFDdEQsVUFBUUEsTUFBTSxDQUFDQyxJQUFmO0FBRUksU0FBS1YsNENBQUw7QUFDSVksTUFBQUEsT0FBTyxDQUFDQyxHQUFSLENBQVlKLE1BQVo7QUFDQSxVQUFJSyxVQUFVLEdBQUdOLEtBQUssQ0FBQ0wsSUFBTixDQUFXWSxJQUFYLENBQWdCLFVBQUFDLElBQUk7QUFBQSxlQUFFQSxJQUFJLENBQUNDLEVBQUwsS0FBWVIsTUFBTSxDQUFDUyxPQUFQLENBQWVELEVBQTdCO0FBQUEsT0FBcEIsQ0FBakI7QUFDQSxhQUFPSCxVQUFVLG1DQUVWTixLQUZVO0FBR2JMLFFBQUFBLElBQUksRUFBRUssS0FBSyxDQUFDTCxJQUFOLENBQVdnQixHQUFYLENBQWUsVUFBQUgsSUFBSTtBQUFBLGlCQUFFQSxJQUFJLENBQUNDLEVBQUwsS0FBWVIsTUFBTSxDQUFDUyxPQUFQLENBQWVELEVBQTNCLG1DQUNqQkQsSUFEaUI7QUFDWEksWUFBQUEsUUFBUSxFQUFFSixJQUFJLENBQUNJLFFBQUwsR0FBYztBQURiLGVBRXJCSixJQUZtQjtBQUFBLFNBQW5CO0FBSE8sMkNBUVZSLEtBUlU7QUFTYkwsUUFBQUEsSUFBSSxvS0FBS0ssS0FBSyxDQUFDTCxJQUFYLG9DQUFxQk0sTUFBTSxDQUFDUyxPQUE1QjtBQUFxQ0UsVUFBQUEsUUFBUSxFQUFFO0FBQS9DO0FBVFMsUUFBakI7O0FBYUosU0FBS3BCLHdEQUFMO0FBQ0ksVUFBR3NCLE1BQU0sQ0FBQ0MsT0FBUCxDQUFlLHFDQUFmLENBQUgsRUFBeUQ7QUFDckQsK0NBQ09mLEtBRFA7QUFFSUwsVUFBQUEsSUFBSSxFQUFFSyxLQUFLLENBQUNMLElBQU4sQ0FBV3FCLE1BQVgsQ0FBa0IsVUFBQUMsS0FBSztBQUFBLG1CQUFJQSxLQUFLLENBQUNSLEVBQU4sS0FBYVIsTUFBTSxDQUFDUyxPQUF4QjtBQUFBLFdBQXZCO0FBRlY7QUFLSDs7QUFFTCxTQUFLbEIsd0RBQUw7QUFDSSxVQUFHc0IsTUFBTSxDQUFDQyxPQUFQLENBQWUscUNBQWYsQ0FBSCxFQUF5RDtBQUNyRCwrQ0FDV2YsS0FEWDtBQUVRTCxVQUFBQSxJQUFJLEVBQUM7QUFGYjtBQUlIOztBQUVMLFNBQUtILDZDQUFMO0FBQ0ksVUFBTTRCLEdBQUcsR0FBR3BCLEtBQUssQ0FBQ0wsSUFBTixDQUFXMEIsTUFBWCxDQUFrQixVQUFDQyxJQUFELEVBQU9kLElBQVAsRUFBZ0I7QUFDdEMsZUFBT2MsSUFBSSxHQUFJZCxJQUFJLENBQUNlLEtBQUwsR0FBYWYsSUFBSSxDQUFDZ0IsS0FBakM7QUFDSCxPQUZPLEVBRU4sQ0FGTSxDQUFaO0FBR0EsNkNBQ094QixLQURQO0FBRUlGLFFBQUFBLEtBQUssRUFBRXNCO0FBRlg7O0FBT0o7QUFDSSxhQUFPcEIsS0FBUDtBQS9DUjtBQWlESCxDQWxERDs7QUFvREEsK0RBQWVELGVBQWYiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vc3RvcmUvcmVkdWNlcnMvc2hvcHBpbmdSZWR1Y2VyLmpzIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCAqIGFzIHR5cGVzIGZyb20gJy4uL3R5cGVzJ1xyXG5cclxuXHJcbmNvbnN0IGluaXRpYWxTdGF0ZT0ge1xyXG4gICAgcHJvZHVjdHM6IFtdLFxyXG4gICAgY2FydDogW10sXHJcbiAgICBsb2FkaW5nOiBmYWxzZSxcclxuICAgIGVycm9yOicnLFxyXG4gICAgdG90YWw6MFxyXG59XHJcblxyXG5cclxuXHJcblxyXG5jb25zdCBzaG9wcGluZ1JlZHVjZXIgPSAoc3RhdGUgPSBpbml0aWFsU3RhdGUsIGFjdGlvbikgPT4ge1xyXG4gICAgc3dpdGNoIChhY3Rpb24udHlwZSkge1xyXG4gICAgICAgIFxyXG4gICAgICAgIGNhc2UgdHlwZXMuQUREX0NBUlQ6XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKGFjdGlvbilcclxuICAgICAgICAgICAgbGV0IGl0ZW1JbkNhcnQgPSBzdGF0ZS5jYXJ0LmZpbmQoaXRlbT0+aXRlbS5pZCA9PT0gYWN0aW9uLnBheWxvYWQuaWQpXHJcbiAgICAgICAgICAgIHJldHVybiBpdGVtSW5DYXJ0IFxyXG4gICAgICAgICAgICA/e1xyXG4gICAgICAgICAgICAgICAgLi4uc3RhdGUsXHJcbiAgICAgICAgICAgICAgICBjYXJ0OiBzdGF0ZS5jYXJ0Lm1hcChpdGVtPT5pdGVtLmlkID09PSBhY3Rpb24ucGF5bG9hZC5pZCBcclxuICAgICAgICAgICAgICAgICAgICA/IHsuLi5pdGVtLCBxdWFudGl0eTogaXRlbS5xdWFudGl0eSsxfVxyXG4gICAgICAgICAgICAgICAgICAgIDogaXRlbSlcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICA6e1xyXG4gICAgICAgICAgICAgICAgLi4uc3RhdGUsXHJcbiAgICAgICAgICAgICAgICBjYXJ0OlsuLi5zdGF0ZS5jYXJ0LCB7Li4uYWN0aW9uLnBheWxvYWQsIHF1YW50aXR5OiAxfV1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIFxyXG4gICAgICAgICAgICBcclxuICAgICAgICBjYXNlIHR5cGVzLlJFTU9WRV9PTkVfRlJPTV9DQVJUOlxyXG4gICAgICAgICAgICBpZih3aW5kb3cuY29uZmlybShcIkRvIHlvdSB3YW50IHRvIGRlbGV0ZSB0aGlzIHByb2R1Y3Q/XCIpKXtcclxuICAgICAgICAgICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgICAgICAgICAgLi4uc3RhdGUsIFxyXG4gICAgICAgICAgICAgICAgICAgIGNhcnQ6IHN0YXRlLmNhcnQuZmlsdGVyKGl0ZW1zID0+IGl0ZW1zLmlkICE9PSBhY3Rpb24ucGF5bG9hZClcclxuICAgICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICBjYXNlIHR5cGVzLlJFTU9WRV9BTExfRlJPTV9DQVJUOlxyXG4gICAgICAgICAgICBpZih3aW5kb3cuY29uZmlybShcIkRvIHlvdSB3YW50IHRvIGRlbGV0ZSBhbGwgcHJvZHVjdHM/XCIpKXsgICAgXHJcbiAgICAgICAgICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAuLi5zdGF0ZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgY2FydDpbXVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgXHJcbiAgICAgICAgY2FzZSB0eXBlcy5HRVRfVE9UQUw6XHJcbiAgICAgICAgICAgIGNvbnN0IHJlcyA9IHN0YXRlLmNhcnQucmVkdWNlKChwcmV2LCBpdGVtKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHByZXYgKyAoaXRlbS5wcmljZSAqIGl0ZW0uY291bnQpO1xyXG4gICAgICAgICAgICAgICAgfSwwKVxyXG4gICAgICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICAgICAgLi4uc3RhdGUsXHJcbiAgICAgICAgICAgICAgICB0b3RhbDogcmVzXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgXHJcbiAgICAgICAgXHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgIGRlZmF1bHQ6XHJcbiAgICAgICAgICAgIHJldHVybiBzdGF0ZVxyXG4gICAgfVxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBzaG9wcGluZ1JlZHVjZXI7Il0sIm5hbWVzIjpbInR5cGVzIiwiaW5pdGlhbFN0YXRlIiwicHJvZHVjdHMiLCJjYXJ0IiwibG9hZGluZyIsImVycm9yIiwidG90YWwiLCJzaG9wcGluZ1JlZHVjZXIiLCJzdGF0ZSIsImFjdGlvbiIsInR5cGUiLCJBRERfQ0FSVCIsImNvbnNvbGUiLCJsb2ciLCJpdGVtSW5DYXJ0IiwiZmluZCIsIml0ZW0iLCJpZCIsInBheWxvYWQiLCJtYXAiLCJxdWFudGl0eSIsIlJFTU9WRV9PTkVfRlJPTV9DQVJUIiwid2luZG93IiwiY29uZmlybSIsImZpbHRlciIsIml0ZW1zIiwiUkVNT1ZFX0FMTF9GUk9NX0NBUlQiLCJHRVRfVE9UQUwiLCJyZXMiLCJyZWR1Y2UiLCJwcmV2IiwicHJpY2UiLCJjb3VudCJdLCJzb3VyY2VSb290IjoiIn0=