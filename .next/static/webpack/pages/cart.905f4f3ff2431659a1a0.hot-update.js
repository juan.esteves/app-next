"use strict";
self["webpackHotUpdate_N_E"]("pages/cart",{

/***/ "./components/Cart/index.jsx":
/*!***********************************!*\
  !*** ./components/Cart/index.jsx ***!
  \***********************************/
/***/ (function(module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/link */ "./node_modules/next/link.js");
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_link__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var store_actions__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! store/actions */ "./store/actions/index.js");
/* harmony import */ var react_icons_fa__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react-icons/fa */ "./node_modules/react-icons/fa/index.esm.js");
/* harmony import */ var _styles__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./styles */ "./components/Cart/styles.jsx");
/* harmony import */ var _CartItem__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./CartItem */ "./components/Cart/CartItem.jsx");
/* module decorator */ module = __webpack_require__.hmd(module);



var _jsxFileName = "/mnt/d/Developer/Front-End/Next/app/components/Cart/index.jsx",
    _this = undefined,
    _s = $RefreshSig$();








var Cart = function Cart(props) {
  _s();

  var increase = props.increase,
      reduce = props.reduce,
      removeOne = props.removeOne,
      removeAll = props.removeAll,
      data = props.data,
      getTotal = props.getTotal;
  var cart = data.cart,
      total = data.total;
  useEffect(function () {
    getTotal();
  }, []);
  var wh = cart.map(function (e) {
    return JSON.stringify(e);
  }).toString();

  if (cart.length === 0) {
    return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("h2", {
      style: {
        textAlign: "center"
      },
      children: "Nothings Product"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 22,
      columnNumber: 20
    }, _this);
  } else {
    return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
      children: [cart.map(function (item, index) {
        return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_CartItem__WEBPACK_IMPORTED_MODULE_5__.default, {
          data: item,
          increase: increase,
          reduce: reduce,
          removeOne: removeOne
        }, index, false, {
          fileName: _jsxFileName,
          lineNumber: 28,
          columnNumber: 29
        }, _this);
      }), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("button", {
        onClick: removeAll,
        children: "REMOVER ALL"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 31,
        columnNumber: 21
      }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
        className: "total",
        children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("a", {
          href: "https://api.whatsapp.com/send?phone=573218122180&text=".concat(wh),
          children: [" ", /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("button", {
            children: ["Enviar orden", /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_icons_fa__WEBPACK_IMPORTED_MODULE_6__.FaWhatsapp, {}, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 33,
              columnNumber: 115
            }, _this)]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 33,
            columnNumber: 95
          }, _this), " "]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 33,
          columnNumber: 21
        }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("h3", {
          children: ["Total:$", total, ".00"]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 34,
          columnNumber: 25
        }, _this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 32,
        columnNumber: 21
      }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_styles__WEBPACK_IMPORTED_MODULE_4__.default, {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 37,
        columnNumber: 21
      }, _this)]
    }, void 0, true);
  }
};

_s(Cart, "OD7bBpZva5O2jO+Puf00hKivP7c=");

_c = Cart;

var mapStateToProps = function mapStateToProps(state) {
  return state.productsReducer;
};

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    getProducts: function getProducts(payload) {
      return dispatch(getAll(payload));
    },
    removeOne: function removeOne(payload) {
      return dispatch((0,store_actions__WEBPACK_IMPORTED_MODULE_3__.removeOne)(payload));
    },
    removeAll: function removeAll(payload) {
      return dispatch((0,store_actions__WEBPACK_IMPORTED_MODULE_3__.removeAll)(payload));
    },
    increase: function increase(payload) {
      return dispatch((0,store_actions__WEBPACK_IMPORTED_MODULE_3__.increse)(payload));
    },
    reduce: function reduce(payload) {
      return dispatch((0,store_actions__WEBPACK_IMPORTED_MODULE_3__.reduce)(payload));
    },
    getTotal: function getTotal() {
      return dispatch((0,store_actions__WEBPACK_IMPORTED_MODULE_3__.getTotal)());
    }
  };
};

/* harmony default export */ __webpack_exports__["default"] = ((0,react_redux__WEBPACK_IMPORTED_MODULE_1__.connect)(mapStateToProps, mapDispatchToProps)(Cart));

var _c;

$RefreshReg$(_c, "Cart");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.id);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }


/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RhdGljL3dlYnBhY2svcGFnZXMvY2FydC45MDVmNGYzZmYyNDMxNjU5YTFhMC5ob3QtdXBkYXRlLmpzIiwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFHSSxJQUFNVSxJQUFJLEdBQUcsU0FBUEEsSUFBTyxDQUFDQyxLQUFELEVBQVc7QUFBQTs7QUFBQSxNQUVoQkMsUUFGZ0IsR0FFeUNELEtBRnpDLENBRWhCQyxRQUZnQjtBQUFBLE1BRVBSLE1BRk8sR0FFeUNPLEtBRnpDLENBRVBQLE1BRk87QUFBQSxNQUVBRSxTQUZBLEdBRXlDSyxLQUZ6QyxDQUVBTCxTQUZBO0FBQUEsTUFFV0QsU0FGWCxHQUV5Q00sS0FGekMsQ0FFV04sU0FGWDtBQUFBLE1BRXNCUSxJQUZ0QixHQUV5Q0YsS0FGekMsQ0FFc0JFLElBRnRCO0FBQUEsTUFFNEJYLFFBRjVCLEdBRXlDUyxLQUZ6QyxDQUU0QlQsUUFGNUI7QUFBQSxNQUdqQlksSUFIaUIsR0FHRkQsSUFIRSxDQUdqQkMsSUFIaUI7QUFBQSxNQUdYQyxLQUhXLEdBR0ZGLElBSEUsQ0FHWEUsS0FIVztBQUt4QkMsRUFBQUEsU0FBUyxDQUFDLFlBQU07QUFDWmQsSUFBQUEsUUFBUTtBQUNYLEdBRlEsRUFFTixFQUZNLENBQVQ7QUFLQSxNQUFNZSxFQUFFLEdBQUlILElBQUksQ0FBQ0ksR0FBTCxDQUFTLFVBQUFDLENBQUM7QUFBQSxXQUFFQyxJQUFJLENBQUNDLFNBQUwsQ0FBZUYsQ0FBZixDQUFGO0FBQUEsR0FBVixDQUFELENBQWlDRyxRQUFqQyxFQUFYOztBQUVBLE1BQUdSLElBQUksQ0FBQ1MsTUFBTCxLQUFnQixDQUFuQixFQUFxQjtBQUNiLHdCQUFPO0FBQUksV0FBSyxFQUFFO0FBQUNDLFFBQUFBLFNBQVMsRUFBQztBQUFYLE9BQVg7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsYUFBUDtBQUNILEdBRkwsTUFFUztBQUNELHdCQUNJO0FBQUEsaUJBRVFWLElBQUksQ0FBQ0ksR0FBTCxDQUFTLFVBQUNPLElBQUQsRUFBT0MsS0FBUDtBQUFBLDRCQUNMLDhEQUFDLDhDQUFEO0FBQXNCLGNBQUksRUFBRUQsSUFBNUI7QUFBa0Msa0JBQVEsRUFBRWIsUUFBNUM7QUFBc0QsZ0JBQU0sRUFBRVIsTUFBOUQ7QUFBc0UsbUJBQVMsRUFBRUU7QUFBakYsV0FBZW9CLEtBQWY7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFESztBQUFBLE9BQVQsQ0FGUixlQU1JO0FBQVEsZUFBTyxFQUFFckIsU0FBakI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsZUFOSixlQU9JO0FBQUssaUJBQVMsRUFBQyxPQUFmO0FBQUEsZ0NBQ0E7QUFBSSxjQUFJLGtFQUEyRFksRUFBM0QsQ0FBUjtBQUFBLHVDQUEwRTtBQUFBLG9EQUFvQiw4REFBQyxzREFBRDtBQUFBO0FBQUE7QUFBQTtBQUFBLHFCQUFwQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUJBQTFFO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFEQSxlQUVJO0FBQUEsZ0NBQVlGLEtBQVo7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQUZKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxlQVBKLGVBWUksOERBQUMsNENBQUQ7QUFBQTtBQUFBO0FBQUE7QUFBQSxlQVpKO0FBQUEsb0JBREo7QUFnQkM7QUFDSixDQWhDTDs7R0FBTUw7O0tBQUFBOztBQW9DVixJQUFNaUIsZUFBZSxHQUFHLFNBQWxCQSxlQUFrQixDQUFDQyxLQUFELEVBQVc7QUFDL0IsU0FBT0EsS0FBSyxDQUFDQyxlQUFiO0FBQ0UsQ0FGTjs7QUFJRSxJQUFNQyxrQkFBa0IsR0FBRyxTQUFyQkEsa0JBQXFCLENBQUNDLFFBQUQsRUFBYztBQUN2QyxTQUFPO0FBQ0xDLElBQUFBLFdBQVcsRUFBRSxxQkFBQ0MsT0FBRDtBQUFBLGFBQWFGLFFBQVEsQ0FBQ0csTUFBTSxDQUFDRCxPQUFELENBQVAsQ0FBckI7QUFBQSxLQURSO0FBRUwzQixJQUFBQSxTQUFTLEVBQUcsbUJBQUMyQixPQUFEO0FBQUEsYUFBYUYsUUFBUSxDQUFDekIsd0RBQVMsQ0FBQzJCLE9BQUQsQ0FBVixDQUFyQjtBQUFBLEtBRlA7QUFHTDVCLElBQUFBLFNBQVMsRUFBRyxtQkFBQzRCLE9BQUQ7QUFBQSxhQUFhRixRQUFRLENBQUMxQix3REFBUyxDQUFDNEIsT0FBRCxDQUFWLENBQXJCO0FBQUEsS0FIUDtBQUlMckIsSUFBQUEsUUFBUSxFQUFHLGtCQUFDcUIsT0FBRDtBQUFBLGFBQWFGLFFBQVEsQ0FBQzVCLHNEQUFPLENBQUM4QixPQUFELENBQVIsQ0FBckI7QUFBQSxLQUpOO0FBS0w3QixJQUFBQSxNQUFNLEVBQUcsZ0JBQUM2QixPQUFEO0FBQUEsYUFBYUYsUUFBUSxDQUFDM0IscURBQU0sQ0FBQzZCLE9BQUQsQ0FBUCxDQUFyQjtBQUFBLEtBTEo7QUFNTC9CLElBQUFBLFFBQVEsRUFBRTtBQUFBLGFBQU02QixRQUFRLENBQUM3Qix1REFBUSxFQUFULENBQWQ7QUFBQTtBQU5MLEdBQVA7QUFRRCxDQVREOztBQVdGLCtEQUFlRixvREFBTyxDQUFDMkIsZUFBRCxFQUFrQkcsa0JBQWxCLENBQVAsQ0FBNkNwQixJQUE3QyxDQUFmIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vX05fRS8uL2NvbXBvbmVudHMvQ2FydC9pbmRleC5qc3giXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtjb25uZWN0fSBmcm9tICdyZWFjdC1yZWR1eCdcclxuaW1wb3J0IExpbmsgZnJvbSAnbmV4dC9saW5rJ1xyXG5pbXBvcnQgeyBnZXRUb3RhbCwgaW5jcmVzZSwgcmVkdWNlLCByZW1vdmVBbGwsIHJlbW92ZU9uZSB9IGZyb20gJ3N0b3JlL2FjdGlvbnMnXHJcbmltcG9ydCB7RmFXaGF0c2FwcH0gZnJvbSAncmVhY3QtaWNvbnMvZmEnXHJcbmltcG9ydCBTdHlsZXMgZnJvbSAnLi9zdHlsZXMnO1xyXG5pbXBvcnQgQ2FydEl0ZW0gZnJvbSAnLi9DYXJ0SXRlbSc7XHJcblxyXG5cclxuICAgIGNvbnN0IENhcnQgPSAocHJvcHMpID0+IHtcclxuICAgICAgIFxyXG4gICAgY29uc3QgeyBpbmNyZWFzZSxyZWR1Y2UscmVtb3ZlT25lLCByZW1vdmVBbGwsIGRhdGEsIGdldFRvdGFsIH0gPSBwcm9wcztcclxuICAgIGNvbnN0IHtjYXJ0LCB0b3RhbH0gPSBkYXRhO1xyXG4gICAgXHJcbiAgICB1c2VFZmZlY3QoKCkgPT4ge1xyXG4gICAgICAgIGdldFRvdGFsKCk7XHJcbiAgICB9LCBbXSlcclxuXHJcblxyXG4gICAgY29uc3Qgd2ggPSAoY2FydC5tYXAoZT0+SlNPTi5zdHJpbmdpZnkoZSkpKS50b1N0cmluZygpIFxyXG4gICBcclxuICAgIGlmKGNhcnQubGVuZ3RoID09PSAwKXtcclxuICAgICAgICAgICAgcmV0dXJuIDxoMiBzdHlsZT17e3RleHRBbGlnbjpcImNlbnRlclwifX0+Tm90aGluZ3MgUHJvZHVjdDwvaDI+XHJcbiAgICAgICAgfWVsc2V7XHJcbiAgICAgICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgICAgICA8PlxyXG4gICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY2FydC5tYXAoKGl0ZW0sIGluZGV4KSA9PihcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxDYXJ0SXRlbSBrZXk9e2luZGV4fSBkYXRhPXtpdGVtfSBpbmNyZWFzZT17aW5jcmVhc2V9IHJlZHVjZT17cmVkdWNlfSByZW1vdmVPbmU9e3JlbW92ZU9uZX0vPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICApKVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICA8YnV0dG9uIG9uQ2xpY2s9e3JlbW92ZUFsbH0+UkVNT1ZFUiBBTEw8L2J1dHRvbj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInRvdGFsXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGEgIGhyZWY9e2BodHRwczovL2FwaS53aGF0c2FwcC5jb20vc2VuZD9waG9uZT01NzMyMTgxMjIxODAmdGV4dD0ke3dofWB9PiA8YnV0dG9uPkVudmlhciBvcmRlbjxGYVdoYXRzYXBwLz48L2J1dHRvbj4gPC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8aDM+VG90YWw6JHt0b3RhbH0uMDA8L2gzPiBcclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgPFN0eWxlcy8+XHJcbiAgICAgICAgICAgICAgICA8Lz5cclxuICAgICAgICAgICAgICAgIClcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIFxyXG5cclxuXHJcbmNvbnN0IG1hcFN0YXRlVG9Qcm9wcyA9IChzdGF0ZSkgPT4ge1xyXG4gICAgcmV0dXJuIHN0YXRlLnByb2R1Y3RzUmVkdWNlclxyXG4gICAgIH1cclxuICBcclxuICBjb25zdCBtYXBEaXNwYXRjaFRvUHJvcHMgPSAoZGlzcGF0Y2gpID0+IHtcclxuICAgIHJldHVybiB7XHJcbiAgICAgIGdldFByb2R1Y3RzOiAocGF5bG9hZCkgPT4gZGlzcGF0Y2goZ2V0QWxsKHBheWxvYWQpKSxcclxuICAgICAgcmVtb3ZlT25lOiAgKHBheWxvYWQpID0+IGRpc3BhdGNoKHJlbW92ZU9uZShwYXlsb2FkKSksXHJcbiAgICAgIHJlbW92ZUFsbDogIChwYXlsb2FkKSA9PiBkaXNwYXRjaChyZW1vdmVBbGwocGF5bG9hZCkpLFxyXG4gICAgICBpbmNyZWFzZTogIChwYXlsb2FkKSA9PiBkaXNwYXRjaChpbmNyZXNlKHBheWxvYWQpKSxcclxuICAgICAgcmVkdWNlOiAgKHBheWxvYWQpID0+IGRpc3BhdGNoKHJlZHVjZShwYXlsb2FkKSksXHJcbiAgICAgIGdldFRvdGFsOiAoKSA9PiBkaXNwYXRjaChnZXRUb3RhbCgpKVxyXG4gICAgfVxyXG4gIH1cclxuICBcclxuZXhwb3J0IGRlZmF1bHQgY29ubmVjdChtYXBTdGF0ZVRvUHJvcHMsIG1hcERpc3BhdGNoVG9Qcm9wcykoQ2FydClcclxuXHJcbiJdLCJuYW1lcyI6WyJjb25uZWN0IiwiTGluayIsImdldFRvdGFsIiwiaW5jcmVzZSIsInJlZHVjZSIsInJlbW92ZUFsbCIsInJlbW92ZU9uZSIsIkZhV2hhdHNhcHAiLCJTdHlsZXMiLCJDYXJ0SXRlbSIsIkNhcnQiLCJwcm9wcyIsImluY3JlYXNlIiwiZGF0YSIsImNhcnQiLCJ0b3RhbCIsInVzZUVmZmVjdCIsIndoIiwibWFwIiwiZSIsIkpTT04iLCJzdHJpbmdpZnkiLCJ0b1N0cmluZyIsImxlbmd0aCIsInRleHRBbGlnbiIsIml0ZW0iLCJpbmRleCIsIm1hcFN0YXRlVG9Qcm9wcyIsInN0YXRlIiwicHJvZHVjdHNSZWR1Y2VyIiwibWFwRGlzcGF0Y2hUb1Byb3BzIiwiZGlzcGF0Y2giLCJnZXRQcm9kdWN0cyIsInBheWxvYWQiLCJnZXRBbGwiXSwic291cmNlUm9vdCI6IiJ9