"use strict";
self["webpackHotUpdate_N_E"]("pages/_app",{

/***/ "./store/types.js":
/*!************************!*\
  !*** ./store/types.js ***!
  \************************/
/***/ (function(module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ADD_CART": function() { return /* binding */ ADD_CART; },
/* harmony export */   "INCRESE": function() { return /* binding */ INCRESE; },
/* harmony export */   "REDUCE": function() { return /* binding */ REDUCE; },
/* harmony export */   "REMOVE_ONE_FROM_CART": function() { return /* binding */ REMOVE_ONE_FROM_CART; },
/* harmony export */   "REMOVE_ALL_FROM_CART": function() { return /* binding */ REMOVE_ALL_FROM_CART; },
/* harmony export */   "CLEAR_CART": function() { return /* binding */ CLEAR_CART; },
/* harmony export */   "GET_ALL": function() { return /* binding */ GET_ALL; },
/* harmony export */   "ERROR": function() { return /* binding */ ERROR; },
/* harmony export */   "LOADING": function() { return /* binding */ LOADING; },
/* harmony export */   "GET_TOTAL": function() { return /* binding */ GET_TOTAL; }
/* harmony export */ });
/* module decorator */ module = __webpack_require__.hmd(module);
var ADD_CART = 'ADD_CART';
var INCRESE = 'INCRESE';
var REDUCE = 'REDUCE';
var REMOVE_ONE_FROM_CART = 'REMOVE_ONE_FROM_CART';
var REMOVE_ALL_FROM_CART = 'REMOVE_ALL_FROM_CART';
var CLEAR_CART = 'CLEAR_CART';
var GET_ALL = 'GET_ALL';
var ERROR = 'ERROR';
var LOADING = 'LOADING';
var GET_TOTAL = 'GET_TOTAL';

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.id);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }


/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RhdGljL3dlYnBhY2svcGFnZXMvX2FwcC40MGZmNWY1MTRkYTQ3ZmE3MzgxZS5ob3QtdXBkYXRlLmpzIiwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQU8sSUFBTUEsUUFBUSxHQUFHLFVBQWpCO0FBQ0EsSUFBTUMsT0FBTyxHQUFHLFNBQWhCO0FBQ0EsSUFBTUMsTUFBTSxHQUFHLFFBQWY7QUFDQSxJQUFNQyxvQkFBb0IsR0FBRyxzQkFBN0I7QUFDQSxJQUFNQyxvQkFBb0IsR0FBRyxzQkFBN0I7QUFDQSxJQUFNQyxVQUFVLEdBQUcsWUFBbkI7QUFDQSxJQUFNQyxPQUFPLEdBQUcsU0FBaEI7QUFDQSxJQUFNQyxLQUFLLEdBQUcsT0FBZDtBQUNBLElBQU1DLE9BQU8sR0FBRyxTQUFoQjtBQUNBLElBQU1DLFNBQVMsR0FBQyxXQUFoQiIsInNvdXJjZXMiOlsid2VicGFjazovL19OX0UvLi9zdG9yZS90eXBlcy5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgY29uc3QgQUREX0NBUlQgPSAnQUREX0NBUlQnXHJcbmV4cG9ydCBjb25zdCBJTkNSRVNFID0gJ0lOQ1JFU0UnXHJcbmV4cG9ydCBjb25zdCBSRURVQ0UgPSAnUkVEVUNFJ1xyXG5leHBvcnQgY29uc3QgUkVNT1ZFX09ORV9GUk9NX0NBUlQgPSAnUkVNT1ZFX09ORV9GUk9NX0NBUlQnXHJcbmV4cG9ydCBjb25zdCBSRU1PVkVfQUxMX0ZST01fQ0FSVCA9ICdSRU1PVkVfQUxMX0ZST01fQ0FSVCdcclxuZXhwb3J0IGNvbnN0IENMRUFSX0NBUlQgPSAnQ0xFQVJfQ0FSVCdcclxuZXhwb3J0IGNvbnN0IEdFVF9BTEwgPSAnR0VUX0FMTCdcclxuZXhwb3J0IGNvbnN0IEVSUk9SID0gJ0VSUk9SJ1xyXG5leHBvcnQgY29uc3QgTE9BRElORyA9ICdMT0FESU5HJ1xyXG5leHBvcnQgY29uc3QgR0VUX1RPVEFMPSdHRVRfVE9UQUwnXHJcblxyXG4iXSwibmFtZXMiOlsiQUREX0NBUlQiLCJJTkNSRVNFIiwiUkVEVUNFIiwiUkVNT1ZFX09ORV9GUk9NX0NBUlQiLCJSRU1PVkVfQUxMX0ZST01fQ0FSVCIsIkNMRUFSX0NBUlQiLCJHRVRfQUxMIiwiRVJST1IiLCJMT0FESU5HIiwiR0VUX1RPVEFMIl0sInNvdXJjZVJvb3QiOiIifQ==