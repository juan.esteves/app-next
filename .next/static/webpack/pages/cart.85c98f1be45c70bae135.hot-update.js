"use strict";
self["webpackHotUpdate_N_E"]("pages/cart",{

/***/ "./components/Cart/index.jsx":
/*!***********************************!*\
  !*** ./components/Cart/index.jsx ***!
  \***********************************/
/***/ (function(module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/link */ "./node_modules/next/link.js");
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_link__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var store_actions__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! store/actions */ "./store/actions/index.js");
/* harmony import */ var react_icons_fa__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react-icons/fa */ "./node_modules/react-icons/fa/index.esm.js");
/* harmony import */ var _styles__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./styles */ "./components/Cart/styles.jsx");
/* harmony import */ var _CartItem__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./CartItem */ "./components/Cart/CartItem.jsx");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_6__);
/* module decorator */ module = __webpack_require__.hmd(module);



var _jsxFileName = "/mnt/d/Developer/Front-End/Next/app/components/Cart/index.jsx",
    _this = undefined,
    _s = $RefreshSig$();









var Cart = function Cart(props) {
  _s();

  var increase = props.increase,
      reduce = props.reduce,
      removeOne = props.removeOne,
      removeAll = props.removeAll,
      data = props.data,
      getTotal = props.getTotal;
  var cart = data.cart,
      total = data.total;
  console.log(cart);
  (0,react__WEBPACK_IMPORTED_MODULE_6__.useEffect)(function () {
    getTotal();
  }, []);
  var wh = cart.map(function (e) {
    return JSON.stringify(e);
  }).toString();

  if (cart.length === 0) {
    return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("h2", {
      style: {
        textAlign: "center"
      },
      children: "Nothings Product"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 23,
      columnNumber: 20
    }, _this);
  } else {
    return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
      children: [cart.map(function (item, index) {
        return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_CartItem__WEBPACK_IMPORTED_MODULE_5__.default, {
          data: item,
          increase: increase,
          reduce: reduce,
          removeOne: removeOne
        }, index, false, {
          fileName: _jsxFileName,
          lineNumber: 29,
          columnNumber: 29
        }, _this);
      }), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("button", {
        onClick: removeAll,
        children: "REMOVER ALL"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 32,
        columnNumber: 21
      }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
        className: "total",
        children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("a", {
          href: "https://api.whatsapp.com/send?phone=573218122180&text=".concat(wh),
          children: [" ", /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("button", {
            children: ["Enviar orden", /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_icons_fa__WEBPACK_IMPORTED_MODULE_7__.FaWhatsapp, {}, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 34,
              columnNumber: 115
            }, _this)]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 34,
            columnNumber: 95
          }, _this), " "]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 34,
          columnNumber: 21
        }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("h3", {
          children: ["Total:$", total, ".00"]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 35,
          columnNumber: 25
        }, _this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 33,
        columnNumber: 21
      }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_styles__WEBPACK_IMPORTED_MODULE_4__.default, {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 38,
        columnNumber: 21
      }, _this)]
    }, void 0, true);
  }
};

_s(Cart, "OD7bBpZva5O2jO+Puf00hKivP7c=");

_c = Cart;

var mapStateToProps = function mapStateToProps(state) {
  return state.productsReducer;
};

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    getProducts: function getProducts(payload) {
      return dispatch(getAll(payload));
    },
    removeOne: function removeOne(payload) {
      return dispatch((0,store_actions__WEBPACK_IMPORTED_MODULE_3__.removeOne)(payload));
    },
    removeAll: function removeAll(payload) {
      return dispatch((0,store_actions__WEBPACK_IMPORTED_MODULE_3__.removeAll)(payload));
    },
    increase: function increase(payload) {
      return dispatch((0,store_actions__WEBPACK_IMPORTED_MODULE_3__.increse)(payload));
    },
    reduce: function reduce(payload) {
      return dispatch((0,store_actions__WEBPACK_IMPORTED_MODULE_3__.reduce)(payload));
    },
    getTotal: function getTotal() {
      return dispatch((0,store_actions__WEBPACK_IMPORTED_MODULE_3__.getTotal)());
    }
  };
};

/* harmony default export */ __webpack_exports__["default"] = ((0,react_redux__WEBPACK_IMPORTED_MODULE_1__.connect)(mapStateToProps, mapDispatchToProps)(Cart));

var _c;

$RefreshReg$(_c, "Cart");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.id);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }


/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RhdGljL3dlYnBhY2svcGFnZXMvY2FydC44NWM5OGYxYmU0NWM3MGJhZTEzNS5ob3QtdXBkYXRlLmpzIiwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBR0ksSUFBTVcsSUFBSSxHQUFHLFNBQVBBLElBQU8sQ0FBQ0MsS0FBRCxFQUFXO0FBQUE7O0FBQUEsTUFFaEJDLFFBRmdCLEdBRXlDRCxLQUZ6QyxDQUVoQkMsUUFGZ0I7QUFBQSxNQUVQVCxNQUZPLEdBRXlDUSxLQUZ6QyxDQUVQUixNQUZPO0FBQUEsTUFFQUUsU0FGQSxHQUV5Q00sS0FGekMsQ0FFQU4sU0FGQTtBQUFBLE1BRVdELFNBRlgsR0FFeUNPLEtBRnpDLENBRVdQLFNBRlg7QUFBQSxNQUVzQlMsSUFGdEIsR0FFeUNGLEtBRnpDLENBRXNCRSxJQUZ0QjtBQUFBLE1BRTRCWixRQUY1QixHQUV5Q1UsS0FGekMsQ0FFNEJWLFFBRjVCO0FBQUEsTUFHakJhLElBSGlCLEdBR0ZELElBSEUsQ0FHakJDLElBSGlCO0FBQUEsTUFHWEMsS0FIVyxHQUdGRixJQUhFLENBR1hFLEtBSFc7QUFJeEJDLEVBQUFBLE9BQU8sQ0FBQ0MsR0FBUixDQUFZSCxJQUFaO0FBQ0FMLEVBQUFBLGdEQUFTLENBQUMsWUFBTTtBQUNaUixJQUFBQSxRQUFRO0FBQ1gsR0FGUSxFQUVOLEVBRk0sQ0FBVDtBQUtBLE1BQU1pQixFQUFFLEdBQUlKLElBQUksQ0FBQ0ssR0FBTCxDQUFTLFVBQUFDLENBQUM7QUFBQSxXQUFFQyxJQUFJLENBQUNDLFNBQUwsQ0FBZUYsQ0FBZixDQUFGO0FBQUEsR0FBVixDQUFELENBQWlDRyxRQUFqQyxFQUFYOztBQUVBLE1BQUdULElBQUksQ0FBQ1UsTUFBTCxLQUFnQixDQUFuQixFQUFxQjtBQUNiLHdCQUFPO0FBQUksV0FBSyxFQUFFO0FBQUNDLFFBQUFBLFNBQVMsRUFBQztBQUFYLE9BQVg7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsYUFBUDtBQUNILEdBRkwsTUFFUztBQUNELHdCQUNJO0FBQUEsaUJBRVFYLElBQUksQ0FBQ0ssR0FBTCxDQUFTLFVBQUNPLElBQUQsRUFBT0MsS0FBUDtBQUFBLDRCQUNMLDhEQUFDLDhDQUFEO0FBQXNCLGNBQUksRUFBRUQsSUFBNUI7QUFBa0Msa0JBQVEsRUFBRWQsUUFBNUM7QUFBc0QsZ0JBQU0sRUFBRVQsTUFBOUQ7QUFBc0UsbUJBQVMsRUFBRUU7QUFBakYsV0FBZXNCLEtBQWY7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFESztBQUFBLE9BQVQsQ0FGUixlQU1JO0FBQVEsZUFBTyxFQUFFdkIsU0FBakI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsZUFOSixlQU9JO0FBQUssaUJBQVMsRUFBQyxPQUFmO0FBQUEsZ0NBQ0E7QUFBSSxjQUFJLGtFQUEyRGMsRUFBM0QsQ0FBUjtBQUFBLHVDQUEwRTtBQUFBLG9EQUFvQiw4REFBQyxzREFBRDtBQUFBO0FBQUE7QUFBQTtBQUFBLHFCQUFwQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUJBQTFFO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFEQSxlQUVJO0FBQUEsZ0NBQVlILEtBQVo7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQUZKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxlQVBKLGVBWUksOERBQUMsNENBQUQ7QUFBQTtBQUFBO0FBQUE7QUFBQSxlQVpKO0FBQUEsb0JBREo7QUFnQkM7QUFDSixDQWhDTDs7R0FBTUw7O0tBQUFBOztBQW9DVixJQUFNa0IsZUFBZSxHQUFHLFNBQWxCQSxlQUFrQixDQUFDQyxLQUFELEVBQVc7QUFDL0IsU0FBT0EsS0FBSyxDQUFDQyxlQUFiO0FBQ0UsQ0FGTjs7QUFJRSxJQUFNQyxrQkFBa0IsR0FBRyxTQUFyQkEsa0JBQXFCLENBQUNDLFFBQUQsRUFBYztBQUN2QyxTQUFPO0FBQ0xDLElBQUFBLFdBQVcsRUFBRSxxQkFBQ0MsT0FBRDtBQUFBLGFBQWFGLFFBQVEsQ0FBQ0csTUFBTSxDQUFDRCxPQUFELENBQVAsQ0FBckI7QUFBQSxLQURSO0FBRUw3QixJQUFBQSxTQUFTLEVBQUcsbUJBQUM2QixPQUFEO0FBQUEsYUFBYUYsUUFBUSxDQUFDM0Isd0RBQVMsQ0FBQzZCLE9BQUQsQ0FBVixDQUFyQjtBQUFBLEtBRlA7QUFHTDlCLElBQUFBLFNBQVMsRUFBRyxtQkFBQzhCLE9BQUQ7QUFBQSxhQUFhRixRQUFRLENBQUM1Qix3REFBUyxDQUFDOEIsT0FBRCxDQUFWLENBQXJCO0FBQUEsS0FIUDtBQUlMdEIsSUFBQUEsUUFBUSxFQUFHLGtCQUFDc0IsT0FBRDtBQUFBLGFBQWFGLFFBQVEsQ0FBQzlCLHNEQUFPLENBQUNnQyxPQUFELENBQVIsQ0FBckI7QUFBQSxLQUpOO0FBS0wvQixJQUFBQSxNQUFNLEVBQUcsZ0JBQUMrQixPQUFEO0FBQUEsYUFBYUYsUUFBUSxDQUFDN0IscURBQU0sQ0FBQytCLE9BQUQsQ0FBUCxDQUFyQjtBQUFBLEtBTEo7QUFNTGpDLElBQUFBLFFBQVEsRUFBRTtBQUFBLGFBQU0rQixRQUFRLENBQUMvQix1REFBUSxFQUFULENBQWQ7QUFBQTtBQU5MLEdBQVA7QUFRRCxDQVREOztBQVdGLCtEQUFlRixvREFBTyxDQUFDNkIsZUFBRCxFQUFrQkcsa0JBQWxCLENBQVAsQ0FBNkNyQixJQUE3QyxDQUFmIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vX05fRS8uL2NvbXBvbmVudHMvQ2FydC9pbmRleC5qc3giXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtjb25uZWN0fSBmcm9tICdyZWFjdC1yZWR1eCdcclxuaW1wb3J0IExpbmsgZnJvbSAnbmV4dC9saW5rJ1xyXG5pbXBvcnQgeyBnZXRUb3RhbCwgaW5jcmVzZSwgcmVkdWNlLCByZW1vdmVBbGwsIHJlbW92ZU9uZSB9IGZyb20gJ3N0b3JlL2FjdGlvbnMnXHJcbmltcG9ydCB7RmFXaGF0c2FwcH0gZnJvbSAncmVhY3QtaWNvbnMvZmEnXHJcbmltcG9ydCBTdHlsZXMgZnJvbSAnLi9zdHlsZXMnO1xyXG5pbXBvcnQgQ2FydEl0ZW0gZnJvbSAnLi9DYXJ0SXRlbSc7XHJcbmltcG9ydCB7IHVzZUVmZmVjdCB9IGZyb20gJ3JlYWN0JztcclxuXHJcblxyXG4gICAgY29uc3QgQ2FydCA9IChwcm9wcykgPT4ge1xyXG4gICAgICAgXHJcbiAgICBjb25zdCB7IGluY3JlYXNlLHJlZHVjZSxyZW1vdmVPbmUsIHJlbW92ZUFsbCwgZGF0YSwgZ2V0VG90YWwgfSA9IHByb3BzO1xyXG4gICAgY29uc3Qge2NhcnQsIHRvdGFsfSA9IGRhdGE7XHJcbiAgICBjb25zb2xlLmxvZyhjYXJ0KVxyXG4gICAgdXNlRWZmZWN0KCgpID0+IHtcclxuICAgICAgICBnZXRUb3RhbCgpO1xyXG4gICAgfSwgW10pXHJcblxyXG5cclxuICAgIGNvbnN0IHdoID0gKGNhcnQubWFwKGU9PkpTT04uc3RyaW5naWZ5KGUpKSkudG9TdHJpbmcoKSBcclxuICAgXHJcbiAgICBpZihjYXJ0Lmxlbmd0aCA9PT0gMCl7XHJcbiAgICAgICAgICAgIHJldHVybiA8aDIgc3R5bGU9e3t0ZXh0QWxpZ246XCJjZW50ZXJcIn19Pk5vdGhpbmdzIFByb2R1Y3Q8L2gyPlxyXG4gICAgICAgIH1lbHNle1xyXG4gICAgICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICAgICAgPD5cclxuICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNhcnQubWFwKChpdGVtLCBpbmRleCkgPT4oXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8Q2FydEl0ZW0ga2V5PXtpbmRleH0gZGF0YT17aXRlbX0gaW5jcmVhc2U9e2luY3JlYXNlfSByZWR1Y2U9e3JlZHVjZX0gcmVtb3ZlT25lPXtyZW1vdmVPbmV9Lz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgKSlcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgPGJ1dHRvbiBvbkNsaWNrPXtyZW1vdmVBbGx9PlJFTU9WRVIgQUxMPC9idXR0b24+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJ0b3RhbFwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxhICBocmVmPXtgaHR0cHM6Ly9hcGkud2hhdHNhcHAuY29tL3NlbmQ/cGhvbmU9NTczMjE4MTIyMTgwJnRleHQ9JHt3aH1gfT4gPGJ1dHRvbj5FbnZpYXIgb3JkZW48RmFXaGF0c2FwcC8+PC9idXR0b24+IDwvYT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGgzPlRvdGFsOiR7dG90YWx9LjAwPC9oMz4gXHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIDxTdHlsZXMvPlxyXG4gICAgICAgICAgICAgICAgPC8+XHJcbiAgICAgICAgICAgICAgICApXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICBcclxuXHJcblxyXG5jb25zdCBtYXBTdGF0ZVRvUHJvcHMgPSAoc3RhdGUpID0+IHtcclxuICAgIHJldHVybiBzdGF0ZS5wcm9kdWN0c1JlZHVjZXJcclxuICAgICB9XHJcbiAgXHJcbiAgY29uc3QgbWFwRGlzcGF0Y2hUb1Byb3BzID0gKGRpc3BhdGNoKSA9PiB7XHJcbiAgICByZXR1cm4ge1xyXG4gICAgICBnZXRQcm9kdWN0czogKHBheWxvYWQpID0+IGRpc3BhdGNoKGdldEFsbChwYXlsb2FkKSksXHJcbiAgICAgIHJlbW92ZU9uZTogIChwYXlsb2FkKSA9PiBkaXNwYXRjaChyZW1vdmVPbmUocGF5bG9hZCkpLFxyXG4gICAgICByZW1vdmVBbGw6ICAocGF5bG9hZCkgPT4gZGlzcGF0Y2gocmVtb3ZlQWxsKHBheWxvYWQpKSxcclxuICAgICAgaW5jcmVhc2U6ICAocGF5bG9hZCkgPT4gZGlzcGF0Y2goaW5jcmVzZShwYXlsb2FkKSksXHJcbiAgICAgIHJlZHVjZTogIChwYXlsb2FkKSA9PiBkaXNwYXRjaChyZWR1Y2UocGF5bG9hZCkpLFxyXG4gICAgICBnZXRUb3RhbDogKCkgPT4gZGlzcGF0Y2goZ2V0VG90YWwoKSlcclxuICAgIH1cclxuICB9XHJcbiAgXHJcbmV4cG9ydCBkZWZhdWx0IGNvbm5lY3QobWFwU3RhdGVUb1Byb3BzLCBtYXBEaXNwYXRjaFRvUHJvcHMpKENhcnQpXHJcblxyXG4iXSwibmFtZXMiOlsiY29ubmVjdCIsIkxpbmsiLCJnZXRUb3RhbCIsImluY3Jlc2UiLCJyZWR1Y2UiLCJyZW1vdmVBbGwiLCJyZW1vdmVPbmUiLCJGYVdoYXRzYXBwIiwiU3R5bGVzIiwiQ2FydEl0ZW0iLCJ1c2VFZmZlY3QiLCJDYXJ0IiwicHJvcHMiLCJpbmNyZWFzZSIsImRhdGEiLCJjYXJ0IiwidG90YWwiLCJjb25zb2xlIiwibG9nIiwid2giLCJtYXAiLCJlIiwiSlNPTiIsInN0cmluZ2lmeSIsInRvU3RyaW5nIiwibGVuZ3RoIiwidGV4dEFsaWduIiwiaXRlbSIsImluZGV4IiwibWFwU3RhdGVUb1Byb3BzIiwic3RhdGUiLCJwcm9kdWN0c1JlZHVjZXIiLCJtYXBEaXNwYXRjaFRvUHJvcHMiLCJkaXNwYXRjaCIsImdldFByb2R1Y3RzIiwicGF5bG9hZCIsImdldEFsbCJdLCJzb3VyY2VSb290IjoiIn0=